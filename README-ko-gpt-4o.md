[[_TOC_]]


# EC2 AWS에서 Librechat의 완전 자동화된 배포

이 프로젝트는 두 가지 방법으로 배포 및 사용할 수 있습니다:
- gitlab.com 인터페이스에서 파이프라인을 통해
- Linux에서 개인 컴퓨터를 통해

두 경우 모두 동일한 중심 파일인 `export.sh` 파일을 사용합니다.
인프라의 경우, `Terraform`(Infrastructure As Code)을 사용한 배포입니다.
EC2의 구성 요소 설치는 `bash` 스크립트를 사용한 `user-data`를 통해 수행됩니다.

배포를 위해 API 키의 추가는 필수가 아니며, Mistral AI를 사용하려는 경우에만 키를 제공해야 기능을 사용할 수 있습니다.
다른 경우에는, 키 없이도 Librechat 배포 인터페이스에서 직접 요청됩니다.  
이 프로젝트는 OpenAI, Mistral AI, Claude 및 [Google 검색](https://docs.librechat.ai/features/plugins/google_search.html)과 관련된 API 키의 자동 배포를 지원합니다.  

**기본 Librechat 설치**는 443 포트를 활성화하지 않고 80 포트를 액세스 가능하게 만듭니다. 이 구성은 인증이 필요한 애플리케이션에는 권장되지 않습니다.  
EC2의 Librechat 프로젝트의 경우, **443 포트는 기본적으로 자동 서명된 인증서를 사용하여 활성화**됩니다. 80 포트는 443 포트로 리디렉션됩니다. 사용 시, 브라우저에서 HTTPS 경고가 표시됩니다. 이 경고는 인증서가 인정된 인증 기관에 의해 서명되지 않았기 때문에 "정상"입니다. 그러나 **HTTPS 프로토콜을 사용하면 네트워크의 비밀번호 도난으로부터 보안을 제공합니다**.  

프로젝트의 Terraform 코드는 원활한 작동에 필요한 엔드포인트를 배포합니다. 이미 이러한 엔드포인트가 있는 경우 관련 코드를 주석 처리할 수 있습니다. 이를 위해 `endpoints.tf` 파일의 내용을 주석 처리하십시오.  

# 아키텍처  
![alt text](/img/architecture.drawio.png)

# 개인 컴퓨터를 통한 배포

전제 조건:
AWS 클라이언트를 구성하여 API를 통해 배포 작업을 실행할 수 있도록 하십시오(또는 환경 변수로 정보를 제공하십시오).  

`export.sh` 파일을 편집하고 `Terraform` 상태에 대한 버킷 이름을 선택합니다.  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

키를 추가하려면:  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
그런 다음 `export.sh` 스크립트를 실행합니다.  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
다음은 약간 가속화된 방식으로 얻은 결과를 보여주는 이미지입니다:  
![alt text](/img/check_deploy_status.gif)

# gitlab.com을 통한 배포

Git 프로젝트를 생성하고 이 저장소를 그 안에 전송하고 CI 변수를 추가합니다.

필수 변수:
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

선택적 변수:
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

그런 다음 Build->Pipeline으로 이동하여 관련 작업을 실행합니다(아이콘 "play"를 통해):

키 작업이 있는 경우, 그렇지 않으면 직접적으로 Terraform 검증으로 이동합니다.
![alt text](/img/pipeline.png)


# export.sh 파일의 문서화

이 Bash 스크립트는 Terraform과 EC2 인스턴스 관리를 사용하여 AWS 인프라의 관리 및 배포를 용이하게 하기 위한 명령어 인터페이스(CLI)입니다. 이 스크립트는 AWS 리소스, API 키를 관리하고 EC2 인스턴스에 연결하기 위한 여러 기능을 제공합니다.

## 초기 구성 및 전역 변수 정의

- 이 시점부터 정의된 모든 변수를 내보내기를 활성화합니다.

![alt text](/img/export.sh.gif)

## AWS 및 Terraform 환경 구성을 위한 변수 정의 - `BUCKET_STATE` : Terraform 상태를 저장하기 위한 S3 버킷의 이름.
- `AWS_DEFAULT_REGION` : 기본으로 사용되는 AWS 리전.
- `USERNAME` : SSH 연결을 위한 사용자 이름.
- `PUBLIC_KEY_FILE` : 인증을 위한 SSH 공개 키의 위치.
- `TF_VAR_region` : Terraform을 위한 환경 변수로 AWS 리전을 설정.

## awscli 확인 및 설치

- awscli가 설치되어 있는지 확인하고, 없다면 설치합니다.

## Terraform을 위한 S3 버킷 생성 및 구성

- Terraform 상태를 저장하기 위한 S3 버킷을 생성하고, 버전 관리를 활성화합니다. 이 작업은 이미 존재하지 않을 경우에만 수행합니다.

## Terraform 초기화

- S3 버킷을 백엔드로 사용하기 위한 필요한 구성으로 Terraform을 초기화합니다.

## 함수들

1. `terraform_plan()` : Terraform 모듈을 업데이트하고, 구성을 검증하며, 실행 계획을 생성하여 Terraform 변경사항을 계획합니다.
2. `terraform_apply()` : 실행 계획에 따라 Terraform 변경사항을 적용합니다.
3. `terraform_destroy()` : 설정된 모든 Terraform 리소스를 삭제하고 AWS SSM Parameter Store에서 배포 상태를 삭제하려고 시도합니다.
4. `ssh_connect()` : 공개 키를 전송한 후 SSH를 통해 EC2 인스턴스에 연결합니다.
5. `ssm_connect()` : AWS SSM을 통해 EC2 인스턴스에 연결합니다.
6. `delete_parameter()` : AWS SSM Parameter Store에서 특정 매개변수를 삭제합니다.
7. `manage_openai_key()` : AWS SSM Parameter Store에서 OpenAI API 키를 관리합니다 (추가 및 삭제).
8. `manage_mistralai_key()` : AWS SSM Parameter Store에서 Mistral AI 키를 관리합니다 (추가 및 삭제).
9. `manage_anthropic_key()` : AWS SSM Parameter Store에서 Anthropic AI 키를 관리합니다 (추가 및 삭제).
10. `manage_google_api_key()` : AWS SSM Parameter Store에서 Google API 키를 관리합니다 (추가 및 삭제).
11. `manage_google_cse_id()` : AWS SSM Parameter Store에서 Google Search Engine ID를 관리합니다 (추가 및 삭제).
12. `check_deployment_status()` : AWS SSM Parameter Store에서 특정 매개변수를 조회하여 인스턴스의 배포 상태를 확인합니다.
13. `manage_allow_registration()` : AWS SSM Parameter Store의 매개변수를 업데이트하여 LibreChat의 등록을 활성화 또는 비활성화합니다.
14. `manage_ssm_parameters()` : 지정된 작업(list, view, delete)에 따라 AWS SSM Parameter Store의 매개변수를 관리합니다.
15. `read_spot_request_type()` : EC2 인스턴스에 대한 Spot 요청 유형을 가져옵니다.
16. `manage_ec2_instance()` : 지정된 작업(stop, start, status)에 따라 EC2 인스턴스를 관리합니다.
17. `display_help()` : 사용자에게 사용 가능한 명령을 표시합니다.

## 함수 내보내기

- 함수를 내보내어 향후 사용에 사용할 수 있도록 합니다.

## 도움말 표시

- 초기화를 완료한 후 사용자를 위한 도움말을 표시합니다.

## 함수 사용법

### terraform_plan

이 함수는 Terraform 변경사항을 계획할 수 있게 합니다. Terraform 모듈을 업데이트하고, Terraform 구성을 검증하며, Terraform 실행 계획을 생성합니다.

`terraform_plan` 함수를 사용하려면, 간단히 다음과 같이 실행하세요:
```bash
terraform_plan
```
### terraform_apply

이 함수는 실행 계획에 따라 Terraform 변경사항을 적용할 수 있게 합니다.

`terraform_apply` 함수를 사용하려면, 간단히 다음과 같이 실행하세요:
```bash
terraform_apply
```
### terraform_destroy

이 함수는 설정된 모든 Terraform 리소스를 삭제하고 AWS SSM Parameter Store에서 배포 상태를 삭제하려고 시도합니다.

`terraform_destroy` 함수를 사용하려면, 간단히 다음과 같이 실행하세요:
```bash
terraform_destroy
```
### ssh_connect

이 함수는 공개 키를 전송한 후 SSH를 통해 EC2 인스턴스에 연결할 수 있게 합니다.

`ssh_connect` 함수를 사용하려면, 간단히 다음과 같이 실행하세요:
```bash
ssh_connect
```
### ssm_connect

이 함수는 AWS SSM을 통해 EC2 인스턴스에 연결할 수 있게 합니다.

`ssm_connect` 함수를 사용하려면, 간단히 다음과 같이 실행하세요:
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

이 기능은 AWS SSM Parameter Store에서 OpenAI API 키를 관리(추가 및 삭제)할 수 있습니다.

`manage_openai_key` 기능을 사용하려면:
```bash
manage_openai_key put [key]
```
키를 추가하고,
```bash
manage_openai_key delete
```
키를 삭제합니다.

### manage_mistralai_key

이 기능은 AWS SSM Parameter Store에서 Mistral AI 키를 관리(추가 및 삭제)할 수 있습니다.

`manage_mistralai_key` 기능을 사용하려면:
```bash
manage_mistralai_key put [key]
```
키를 추가하고,
```bash
manage_mistralai_key delete
```
키를 삭제합니다.

### manage_anthropic_key

이 기능은 AWS SSM Parameter Store에서 Anthropic AI 키를 관리(추가 및 삭제)할 수 있습니다.

`manage_anthropic_key` 기능을 사용하려면:
```bash
manage_anthropic_key put [key]
```
키를 추가하고,
```bash
manage_anthropic_key delete
```
키를 삭제합니다.

### manage_google_api_key

이 기능은 AWS SSM Parameter Store에서 Google API 키를 관리(추가 및 삭제)할 수 있습니다.

`manage_google_api_key` 기능을 사용하려면:
```bash
manage_google_api_key put [key]
```
키를 추가하고,
```bash
manage_google_api_key delete
```
키를 삭제합니다.

### manage_google_cse_id

이 기능은 AWS SSM Parameter Store에서 Google Search Engine ID를 관리(추가 및 삭제)할 수 있습니다.

`manage_google_cse_id` 기능을 사용하려면:
```bash
manage_google_cse_id put [key]
```
키를 추가하고,
```bash
manage_google_cse_id delete
```
키를 삭제합니다.

### check_deployment_status

이 기능은 AWS SSM Parameter Store에서 특정 매개변수를 확인하여 인스턴스의 배포 상태를 점검할 수 있습니다.

`check_deployment_status` 기능을 사용하려면:
```bash
check_deployment_status
```
### manage_allow_registration

이 기능은 AWS SSM Parameter Store에서 매개변수를 업데이트하여 LibreChat의 등록 기능을 활성화하거나 비활성화할 수 있습니다.

`manage_allow_registration` 기능을 사용하려면:
```bash
manage_allow_registration on
```
등록을 활성화하고,
```bash
manage_allow_registration off
```
등록을 비활성화합니다.

### manage_ssm_parameters

이 기능은 지정된 작업(목록, 보기, 삭제)에 따라 AWS SSM Parameter Store에서 매개변수를 관리할 수 있습니다.

`manage_ssm_parameters` 기능을 사용하려면:
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
매개변수를 나열하고,
```bash
manage_ssm_parameters view
```
매개변수 값을 표시하며, 
```bash
manage_ssm_parameters delete
```
모든 매개변수를 삭제합니다.

### manage_ec2_instance

이 기능은 지정된 작업(중지, 시작, 상태)에 따라 EC2 인스턴스를 관리할 수 있습니다.

`manage_ec2_instance` 기능을 사용하려면:
```bash
manage_ec2_instance stop
```
인스턴스를 중지하고,
```bash
manage_ec2_instance start
```
인스턴스를 시작합니다.
```bash
manage_ec2_instance status
```

인스턴스의 상태를 확인하려면:  

![alt text](/img/status.png)

### display_help

이 기능은 사용자에게 사용 가능한 명령을 표시합니다.

`display_help` 기능을 사용하려면:
```bash
display_help
```

## 참고 사항 및 모범 사례

1. 이 스크립트를 사용하기 전에 AWS 자격 증명이 올바르게 구성되었는지 확인하십시오. 자격 증명은 `aws configure` 명령을 사용하거나 환경 변수 `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, 및 `AWS_SESSION_TOKEN`(필요시)을 정의하여 구성할 수 있습니다.
2. `terraform_plan`, `terraform_apply`, `terraform_destroy` 기능을 사용하기 전에 Terraform 구성 파일(예: `main.tf`)이 올바르게 구성되었는지 확인하십시오. 이 파일에는 생성, 업데이트 또는 삭제하려는 AWS 리소스의 정의가 포함되어야 합니다.
3. `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, 및 `manage_google_cse_id` 기능은 AWS SSM Parameter Store에 API 키를 저장합니다. 이러한 키를 주의깊게 관리하고 무단 접근을 방지하기 위한 보안 모범 사례를 따르십시오. `manage_ec2_instance` 함수는 Spot 요청 유형에 따라 EC2 인스턴스를 관리합니다. 이 함수를 사용하기 전에 다양한 Spot 요청 유형의 의미를 이해해야 합니다. Spot 인스턴스에 대해 더 알고 싶다면 [AWS 문서](https://aws.amazon.com/ec2/spot/)를 참조하세요.
5. Terraform 변경사항을 적용한 후 `check_deployment_status` 함수를 사용하여 항상 인스턴스의 배포 상태를 확인하는 것이 좋습니다. 이를 통해 인스턴스가 올바르게 구성되고 사용 준비가 완료되었는지 확인할 수 있습니다.
6. `ssh_connect` 함수를 사용하여 EC2 인스턴스에 연결할 때, 해당 SSH 공개 키가 인스턴스에 올바르게 추가되었는지 확인하세요. 이는 AWS 콘솔, AWS CLI, 또는 Terraform 구성 파일을 통해 인스턴스에 SSH 공개 키를 추가할 수 있습니다.
7. 이 스크립트는 AWS 및 Terraform에서 사용하도록 설계되었습니다. 이 스크립트를 사용하기 전에 이러한 도구가 올바르게 설치 및 구성되었는지 확인하세요. AWS CLI 설치 및 구성에 대한 지침은 [AWS 문서](https://aws.amazon.com/cli/)에서, Terraform은 [Terraform 문서](https://www.terraform.io/)에서 찾을 수 있습니다.
8. 이 스크립트를 사용하여 AWS 인프라에 변경을 가하기 전에 항상 데이터와 구성을 백업하는 것이 좋습니다. 이렇게 하면 문제가 발생할 경우 데이터를 쉽게 복구할 수 있습니다.
9. 스크립트를 자신의 특정 필요에 맞게 커스터마이즈하고 확장하는 것을 주저하지 마세요. 새로운 기능을 추가하고, 기존 기능을 수정하거나, 스크립트를 다른 AWS 서비스나 인프라 코드 도구와 함께 작동하도록 조정할 수 있습니다.
10. SSM을 통해 서버에 연결하고자 한다면, 세션 매니저 플러그인을 배포해야 합니다. Ubuntu의 예:
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

이 주석과 모범 사례를 따르면 AWS 인프라 및 EC2 인스턴스를 효과적으로 관리하기 위해 이 스크립트를 최대한 활용할 수 있습니다.

## 문제 해결 및 문제 해결

1. **AWS 인증 오류** : 이 스크립트를 사용하는 동안 인증 오류가 발생하는 경우, AWS 자격 증명이 올바르게 구성되었는지 확인하세요. `aws configure` 명령을 사용하거나 환경 변수를 `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, 및 필요한 경우 `AWS_SESSION_TOKEN`을 설정하여 이를 수행할 수 있습니다. 또한 Terraform 구성 파일에 지정된 AWS 리소스에 접근할 수 있는 적절한 권한이 있는지 확인하세요.
2. **Terraform 구성 오류** : `terraform_plan`, `terraform_apply`, 또는 `terraform_destroy` 함수 사용 중 오류가 발생하는 경우, Terraform 구성 파일이 올바르게 구성되었는지 확인하세요. AWS 리소스가 올바르게 정의되었고 리소스 간의 종속성이 올바르게 관리되고 있는지 확인하십시오. `terraform validate` 명령을 사용하여 Terraform 구성 파일의 유효성을 확인할 수 있습니다.
3. **SSH 연결 오류** : `ssh_connect` 함수를 사용할 때 오류가 발생하면 해당 SSH 공개 키가 EC2 인스턴스에 올바르게 추가되었는지 확인하세요. 또한 사용자 이름과 인스턴스의 IP 주소가 정확한지 확인하십시오. 이 정보는 AWS 콘솔이나 AWS CLI를 통해 확인할 수 있습니다.
4. **AWS SSM 연결 오류** : `ssm_connect` 함수를 사용할 때 오류가 발생하는 경우, EC2 인스턴스가 AWS SSM을 사용하도록 올바르게 구성되었는지 확인하세요. 인스턴스가 적절한 IAM 역할을 가지고 있으며 보안 그룹 설정이 SSM IP 주소로부터의 들어오는 트래픽을 허용하도록 설정되었는지 확인하세요. AWS SSM 구성에 대한 더 많은 정보는 [AWS 문서](https://aws.amazon.com/ssm/)에서 확인할 수 있습니다.
5. **API 키 관리 오류**: `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, 또는 `manage_google_cse_id` 함수를 사용하는 동안 오류가 발생하면 AWS SSM Parameter Store에 액세스할 적절한 권한이 있는지 확인하세요. 또한 API 키가 올바르게 형식화되었고 이러한 함수에 올바른 인수를 제공하고 있는지도 확인하세요.
6. **EC2 인스턴스 관리 오류**: `manage_ec2_instance` 함수를 사용하는 동안 오류가 발생하면 AWS 계정에서 EC2 인스턴스를 관리할 적절한 권한이 있는지 확인하세요. 또한 인스턴스 ID가 정확하고 이 함수에 올바른 인수를 제공하고 있는지 확인하세요.  

이 스크립트와 관련한 문제가 발생하면 AWS 및 Terraform 문서와 온라인 포럼, 리소스를 참고해 도움을 받으세요. 추가 지원이 필요할 경우 AWS 지원에 문의할 수도 있습니다.

이러한 문제 해결 및 문제 해결 팁을 따르면 AWS 인프라 및 EC2 인스턴스 관리를 위한 이 스크립트를 사용할 때 발생할 수 있는 대부분의 문제를 해결할 수 있습니다.

# gitlab-ci.yml 파일 설명서

이 GitLab CI/CD 파일(gitlab-ci.yml)은 이전에 문서화된 Bash 스크립트에서 정의된 함수를 사용하여 Terraform을 통해 AWS 인프라 및 EC2 인스턴스를 관리하기 위한 배포 파이프라인을 설명합니다.

## 변수

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID`: GitLab CI/CD 인터페이스에 설정할 API 키.
- `TERRAFORM_VERSION`: 작업을 실행하는 데 사용되는 Terraform Docker 이미지 버전.
- `TF_VAR_region`: 기본 AWS 지역.

## 단계

- `Pré-requis optionels`: API 키를 추가하거나 제거하는 단계.
- `Vérifications`: Terraform 변경 사항을 확인하는 단계.
- `Déploiements`: Terraform 변경 사항을 적용하고 배포 상태를 확인하는 단계.
- `Management`: EC2 인스턴스 및 애플리케이션 매개변수를 관리하는 단계.
- `Suppressions`: Terraform 자원 및 API 키를 삭제하는 단계.

## .terraform\_template

Terraform 작업을 위한 템플릿으로, Docker 이미지, 입력, 스크립트 전에 실행할 명령을 정의합니다.

## 작업

1. **Terraform 확인**: `terraform_plan`을 실행하여 Terraform 변경 사항을 확인하고 결과를 아티팩트로 저장하는 작업.
2. **Terraform 배포**: `terraform_apply`를 실행하여 Terraform 변경 사항을 적용하고 `check_deployment_status`를 사용하여 배포 상태를 확인하는 작업.
3. **Terraform 삭제**: `terraform_destroy`를 실행하여 Terraform 자원을 삭제하는 작업.
4. **Librechat 등록 열기**: `manage_allow_registration on`을 사용하여 Librechat에서 등록을 활성화하는 작업.
5. **Librechat 등록 닫기**: `manage_allow_registration off`를 사용하여 Librechat에서 등록을 비활성화하는 작업.
6. **EC2 시작**: `manage_ec2_instance start`를 사용하여 EC2 인스턴스를 시작하는 작업.
7. **EC2 중지**: `manage_ec2_instance stop`을 사용하여 EC2 인스턴스를 중지하는 작업.
8. **EC2 상태**: `manage_ec2_instance status`를 사용하여 EC2 인스턴스 상태를 표시하는 작업.
9. **키 추가**: 해당 `manage_*_key put` 함수를 사용하여 API 키를 추가하는 작업. **키 삭제**: 이 작업은 `manage_*_key delete` 함수를 사용하여 API 키를 삭제합니다.

## 완전한 인프라 배포 시간 + EC2 OS 구성 6분 미만:
![alt text](/img/pipeline_apply_time.png)

## 주의사항 및 모범 사례

1. GitLab CI/CD 인터페이스에서 파이프라인을 실행하기 전에 `OPENAI_KEY`, `MISTRALAI_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID` 변수를 올바르게 설정하십시오.
2. 인프라에 적절한 버전의 Terraform Docker 이미지를 사용하십시오.
3. 이 GitLab CI/CD 파일에 변경 사항을 적용할 때 관련 Bash 스크립트를 업데이트하는 것을 잊지 마십시오.
4. API 키 및 AWS 자격 증명 관리를 위한 보안 모범 사례를 따르십시오.

위의 주의사항 및 모범 사례를 따름으로써 Terraform 및 Bash 스크립트 내 정의된 기능을 사용하여 AWS 인프라 및 EC2 인스턴스를 효과적으로 관리할 수 있습니다.

## 문제해결 및 해결책

1. **AWS 인증 오류**: GitLab CI/CD 파이프라인을 실행하는 동안 인증 오류가 발생하면 AWS 자격 증명이 올바르게 구성되어 있는지 확인하십시오. 환경 변수 `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, 및 (필요한 경우) `AWS_SESSION_TOKEN`이 설정되어 있고 유효한지 확인하십시오. 이는 GitLab CI/CD 인터페이스 또는 Bash 스크립트에서 구성할 수 있습니다.
2. **Terraform 구성 오류**: Terraform 작업(`terraform_plan`, `terraform_apply`, 또는 `terraform_destroy`)을 실행하는 동안 오류가 발생하면 Terraform 구성 파일이 올바르게 구성되어 있는지 확인하십시오. AWS 리소스가 올바르게 정의되고 리소스 간의 종속성이 적절하게 관리되고 있는지 확인하십시오.
3. **API 키 관리 오류**: API 키 추가 또는 삭제 시 오류가 발생하면 AWS SSM 파라미터 스토어에 액세스할 수 있는 적절한 권한이 있는지 확인하십시오. 또한 API 키가 올바르게 형식화되어 있고 `manage_*_key` 함수에 올바른 인수가 제공되고 있는지 확인하십시오.
4. **EC2 인스턴스 관리 오류**: `manage_ec2_instance` 기능 사용 시 오류가 발생하면 AWS 계정 내 EC2 인스턴스를 관리할 수 있는 적절한 권한이 있는지 확인하십시오. 또한 인스턴스 ID가 올바른지 확인하고 이러한 기능에 올바른 인수를 제시하십시오.

이 GitLab CI/CD 파일과 관련하여 문제 발생 시, AWS, Terraform 및 GitLab CI/CD 문서 및 온라인 리소스를 참조하여 도움을 받을 수 있습니다. 추가 도움이 필요하다면 AWS 또는 GitLab 지원팀에 연락할 수 있습니다.

위의 문제해결 및 해결책을 따라준다면 Terraform 및 Bash 스크립트 내 정의된 기능을 통해 AWS 인프라 및 EC2 인스턴스를 관리하는 동안 발생할 수 있는 대부분의 문제를 해결할 수 있을 것입니다.

# LibreChat EC2 사용자 데이터 스크립트

이 스크립트는 Amazon Elastic Compute Cloud (EC2) 인스턴스에서 실행되어 LibreChat 애플리케이션 구성 프로세스를 자동화하도록 설계되었습니다. 운영 체제 업데이트, 필수 패키지 설치, Docker 및 LibreChat 애플리케이션 배포 등의 여러 업무를 수행합니다.

## 스크립트 개요

1. 운영 체제 업데이트 및 필수 패키지 설치.
2. Docker 및 Docker Compose 구성.
3. LibreChat 애플리케이션 클론 및 구성.
4. 보안 통신을 위한 SSL 인증서 구성.
5. Docker Compose를 사용한 LibreChat 애플리케이션 실행.

## 스크립트 세부 사항 ### 운영 체제 업데이트 및 필수 패키지 설치

스크립트는 운영 체제를 업데이트하고 `ca-certificates`, `curl`, `awscli`와 같은 필수 패키지를 설치하는 것으로 시작합니다. 이는 시스템이 LibreChat 애플리케이션을 실행하기 위한 필요 종속성을 보유하도록 보장합니다.

### Docker 및 Docker Compose 구성

이어지는 단계에서는 Docker 및 Docker Compose가 설치되며, 이는 컨테이너화된 환경에서 LibreChat 애플리케이션을 배포하는 데 사용됩니다. 또한, 현재 사용자를 Docker 그룹에 추가하여 루트 권한 없이 Docker 명령을 실행할 수 있도록 합니다.

### LibreChat 애플리케이션 클론 및 구성

스크립트는 GitHub에서 LibreChat 저장소를 클론하고 필요한 구성 파일을 설정합니다. 또한, AWS Systems Manager (SSM) Parameter Store에서 필수 API 키와 토큰을 가져와 구성 파일을 업데이트합니다.

### 안전한 통신을 위한 SSL 인증서 구성

클라이언트와 서버 간의 안전한 통신을 보장하기 위해, 스크립트는 자체 서명된 SSL 인증서를 생성하고 Nginx 구성을 수정하여 SSL을 활성화합니다. 또한 Diffie-Hellman 매개변수를 다운로드하여 SSL 암호화를 강화합니다.

### Docker Compose를 이용한 LibreChat 애플리케이션 실행

마지막으로, 스크립트는 Docker Compose를 사용하여 LibreChat 애플리케이션을 실행합니다. 이는 컨테이너화된 애플리케이션을 시작하고 사용자에게 접근 가능하게 만듭니다.

## 오류 처리 및 로깅

스크립트는 설정 과정 중 발생할 수 있는 문제를 해결하는 데 도움을 주기 위해 오류 처리 및 로깅 메커니즘을 포함합니다. 표준 출력 및 오류 스트림을 `/var/log/user-data.log`에 위치한 로그 파일로 리디렉션합니다. 또한, 스크립트는 실행 중 발생하는 오류를 포착하고 AWS SSM Parameter Store에서 배포 상태를 업데이트하는 트랩 함수를 사용합니다.

## 배포 상태 확인

스크립트에 포함되어 있지 않지만 제공된 정보에 언급된 `check_deployment_status` 함수는 AWS SSM Parameter Store에서 특정 매개변수를 가져와 EC2 인스턴스의 배포 상태를 확인하는 데 사용됩니다. 이 함수는 `export.sh` 스크립트에서 호출되며 배포 진행 상황에 대한 업데이트를 제공합니다.

## Terraform 통합

이 사용자 데이터 스크립트는 `ec2.tf`라는 Terraform 구성 파일에서 호출되며, 이는 EC2 인스턴스를 만들고 지정된 매개변수를 적용합니다. 사용자 데이터 스크립트는 `user_data` 인수를 사용하여 인스턴스로 전달되어 인스턴스가 시작될 때 스크립트가 실행되도록 보장합니다.

## 파일 정보

이름: `user_data.sh`   
위치: `${path.module}/scripts/user_data.sh`   
사용: `ec2.tf`라는 Terraform 구성 파일에 지정된 대로 EC2 인스턴스가 시작될 때 자동으로 실행됩니다.   

# Script check_spot.py  

이 Python 스크립트는 Terraform 변수 파일 (`variables.tf`)에서 EC2 인스턴스의 스팟 가격을 확인하고 업데이트할 수 있습니다. 이 스크립트를 사용하면 비용 관리를 간소화하고 인스턴스에 사용되는 스팟 가격이 최신 상태임을 보장합니다.

이 스크립트를 사용하려면 먼저 Python 3과 AWS Python SDK인 Boto3를 설치해야 합니다. 그 후, `--update` 옵션을 사용하여 `variables.tf` 파일의 스팟 가격을 업데이트할 수 있습니다. 스크립트는 다양한 인스턴스 유형을 지원하며 지정된 인스턴스의 가용 영역(AZ)을 자동으로 검색합니다.

스크립트는 AWS 파라미터, 예를 들어 지역, 인스턴스 유형 및 `variables.tf` 파일 경로를 설정하고 인스턴스를 서비스하는 EC2 클라이언트를 초기화하는 것으로 시작합니다. `read_max_spot_prices()` 함수는 `variables.tf` 파일에서 현재 최대 스팟 가격을 읽어 인스턴스 유형을 키로 하고 최대 가격을 값으로 하는 사전을 반환합니다.

`get_instance_az()` 함수는 EC2 클라이언트를 사용하여 Name 태그로 지정된 인스턴스의 가용 영역(AZ)을 가져옵니다. 인스턴스를 찾지 못하면 기본 AZ를 반환합니다.

`check_and_update_spot_prices()` 함수는 필요에 따라 `variables.tf` 파일에 최대 스팟 가격을 확인하고 업데이트합니다. 현재 가격을 얻기 위해 `read_max_spot_prices()`를 호출한 후, 각 인스턴스 유형 및 지정된 AZ의 스팟 가격 기록을 가져옵니다. 현재 가격이 설정된 가격보다 높으면 함수는 설정된 가격을 업데이트합니다.

`update_variables_file()` 함수는 새 스팟 가격으로 `variables.tf` 파일을 업데이트합니다. 이 함수는 인스턴스 유형을 키로 하고 새 가격을 값으로 하는 사전을 입력으로 받습니다.

마지막으로, 메인 스크립트는 스팟 가격을 확인하고 업데이트하기 위해 `check_and_update_spot_prices()`를 호출합니다. 스크립트를 실행할 때 `--update` 인수가 지정되고 새로운 가격이 있으면, `update_variables_file()` 함수가 호출되어 `variables.tf` 파일을 업데이트합니다.

요약하자면, 이 Python 스크립트는 Terraform 변수 파일에서 스팟 가격을 확인하고 업데이트하는 프로세스를 자동화하여 EC2 인스턴스의 스팟 가격 관리를 단순화합니다.

# 문서 Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requirements

| 이름 | 버전 |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| 이름 | 버전 |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Modules

모듈 없음.

## Resources | 이름 | 유형 |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | 리소스 |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | 리소스 |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 리소스 |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 리소스 |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 리소스 |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | 리소스 |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | 리소스 |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | 리소스 |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | 리소스 |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | 리소스 |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | 리소스 |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | 리소스 |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | 리소스 |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 리소스 |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 리소스 |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 리소스 |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | 데이터 소스 |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | 데이터 소스 |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | 데이터 소스 |

## 입력 | Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | 등록 활성화 여부 설정 | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | EC2 인스턴스 연결 접속이 허용된 IP 주소 목록 | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | AWS AMI 필터 이름 | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | EC2 인스턴스를 자동으로 중지할 시간 | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | 서브넷을 위한 가용 영역 목록 | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | EC2 인스턴스의 유형 | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | 열어야 할 포트 목록 | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | 리소스가 배포될 AWS 지역 정의 | `string` | n/a | yes |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | 각 인스턴스 유형의 최대 가격 | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | 스팟 요청 유형 (지속적 또는 일시적) | `string` | `"persistante"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | 서브넷용 CIDR 블록 목록 | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | 사용할 서브넷의 인덱스 [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | EBS 볼륨의 크기(GB) | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC를 위한 CIDR 블록 | `string` | `"10.0.0.0/24"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | EC2 인스턴스 ID |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | EC2 인스턴스의 공용 IP 주소 |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |
<!-- END_TF_DOCS -->

# Auteur

Julien LE SAUX  
Email : contact@jls42.org

# Clause de non-responsabilité

이 스크립트를 사용함으로써, 당신은 그것을 자신의 책임으로 사용하고 있음을 인정합니다. 이 스크립트 사용으로 인해 발생하는 비용, 손해 또는 손실에 대해 저자는 책임을 지지 않습니다. 이 스크립트를 사용하기 전에 AWS와 관련된 비용을 이해해야 합니다.

# Licence

이 프로젝트는 GPL 라이선스 하에 있습니다.

**이 문서는 gpt-4o 모델을 사용하여 fr 버전에서 ko 언어로 번역되었습니다. 번역 프로세스에 대한 자세한 내용은 https://gitlab.com/jls42/ai-powered-markdown-translator를 참조하십시오.**

