# Définition de la région AWS où les ressources seront déployées.
variable "region" {
  type = string
}

# Bloc CIDR pour le VPC à créer.
variable "vpc_cidr" {
  description = "Bloc CIDR pour le VPC"
  type        = string
  default     = "10.0.0.0/24"
}

# Blocs CIDR pour les subnets à l'intérieur du VPC.
variable "subnet_cidrs" {
  description = "Blocs CIDR pour les subnets"
  type        = list(string)
  default     = ["10.0.0.0/26", "10.0.0.64/26", "10.0.0.128/26"]
}

# Zones de disponibilité AWS où les subnets seront créés.
variable "availability_zones" {
  description = "Zones de disponibilité pour les subnets"
  type        = list(string)
  default     = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

# Filtre pour choisir une AMI spécifique lors de la création d'instances EC2.
variable "ami_filter_name" {
  description = "Nom du filtre pour l'AMI AWS"
  type        = string
  default     = "*ubuntu-jammy-22.04-amd64-server*"
}

# Type d'instance EC2 à utiliser pour les instances créées.
variable "instance_type" {
  description = "Type d'instance pour l'instance EC2"
  type        = string
  default     = "t3a.medium"
}

# Heure programmée pour l'arrêt automatique de l'instance EC2.
variable "autostop_time" {
  description = "Heure pour arrêter automatiquement l'instance EC2"
  type        = string
  default     = "00:00"
}

# Taille du volume EBS attaché à l'instance EC2.
variable "volume_size" {
  description = "Taille du volume EBS en GB"
  type        = number
  default     = 20
}

# Liste des adresses IP autorisées à accéder à l'instance EC2 via Instance Connect.
variable "allowed_ips" {
  description = "Liste des adresses IP autorisées à accéder au point de terminaison EC2 Instance Connect"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

# Prix maximum pour chaque type d'instance lors de l'utilisation des instances Spot.
variable "spot_max_price" {
  description = "Prix maximum pour chaque type d'instance"
  type        = map(string)
  default = {
    "t3a.large"  = "0.0376"
    "t3a.small"  = "0.0095"
    "t3a.medium" = "0.01940"
    "t3a.micro"  = "0.006"
  }
}

# Type de demande Spot (persistent ou one-time).
variable "spot_request_type" {
  description = "Type de demande Spot (persistent ou one-time)"
  default     = "persistent"
  type        = string
}

# Index du sous-réseau à utiliser parmi ceux définis dans `subnet_cidrs`.
variable "subnet_index" {
  description = "Index du sous-réseau à utiliser [0,1,2]"
  type        = number
  default     = 0
}

# Liste des ports à ouvrir sur le groupe de sécurité de l'instance EC2 (SSH n'est pas nécessaire avec SSM).
variable "open_ports" {
  description = "Liste des ports à ouvrir"
  type        = list(number)
  default     = [80, 443]
}

# Active ou désactive la possibilité d'inscription sur l'application LibreChat.
variable "allow_registration" {
  description = "Permet d'activer ou désactiver l'inscription"
  type        = bool
  default     = true
}

variable "spot_enabled" {
  description = "Active ou désactive l'utilisation du marché Spot"
  type        = bool
  default     = false
}
