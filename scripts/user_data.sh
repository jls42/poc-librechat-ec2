#!/bin/bash

# Arrêter le script si une commande échoue
set -e
trap 'error_handler' ERR

# Rediriger la sortie standard et d'erreur vers un fichier de log pour faciliter le débogage
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Changement de répertoire vers le répertoire de l'utilisateur ubuntu
cd /home/ubuntu

# Mise à jour des paquets et installation des dépendances nécessaires
sudo apt update
sudo apt install -y ca-certificates curl awscli

# Récupération du token pour l'API metadata d'AWS EC2
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600" -s`

# Récupération de la région AWS par défaut à partir des métadonnées de l'instance
AWS_DEFAULT_REGION=`curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/placement/region`

# Fonction pour mettre à jour le statut du déploiement via AWS SSM
update_status() {
  STATUS_MESSAGE=$1
  aws ssm put-parameter --name "/librechat/deployment-status" --type "String" --value "$STATUS_MESSAGE" --overwrite --region $AWS_DEFAULT_REGION
}

# Gestionnaire d'erreur pour mettre à jour le statut en cas d'échec
error_handler() {
  update_status "Echec de l'installation"
  echo "Une erreur est survenue. Veuillez consulter le fichier de log pour plus de détails."
  exit 1
}

# Mise à jour du système d'exploitation et notification du statut
update_status "1% -  Mise à jour OS"
sudo apt upgrade -y
update_status "10% - apt pre-requis"

# Attente pour l'affichage de l'avancement dans le check de status
sleep 2

# Création du répertoire pour les clés de dépôt APT si nécessaire
sudo install -m 0755 -d /etc/apt/keyrings

# Mise à jour du statut avant l'installation de Docker
update_status "20% - Installation de Docker"

# Ajout de la clé GPG du dépôt Docker
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Ajout du dépôt Docker aux sources APT
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Mise à jour de la liste des paquets et installation de Docker et ses composants
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Ajout de l'utilisateur ubuntu au groupe Docker pour permettre l'exécution sans sudo
sudo usermod -aG docker ubuntu

# Installation de Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.24.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Installation de Git, Node.js, et NPM
sudo apt install -y git nodejs npm

# Démarrage du service Docker
sudo systemctl start docker

# Mise à jour du statut avant le clone de LibreChat
update_status "30% - Installation de Librechat"

# Clone le dépôt LibreChat avec uniquement le tag v0.7.6
git clone --branch v0.7.6 --depth 1 https://github.com/danny-avila/LibreChat.git
cd LibreChat/

# Mise à jour du statut et préparation du fichier .env
update_status "40% - Configuration du fichier .env"
cp .env.example .env

# Script de mise à jour de la configuration ALLOW_REGISTRATION pour LibreChat
cat <<'EOF' > /usr/local/bin/update_registration.sh
#!/bin/bash

# Chemin vers le fichier .env pour la configuration de LibreChat
ENV_FILE="/home/ubuntu/LibreChat/.env"

# Récupération du token et de la région AWS pour accéder aux métadonnées
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600" -s`
AWS_DEFAULT_REGION=`curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/placement/region`

# Nom du paramètre dans AWS SSM Parameter Store pour ALLOW_REGISTRATION
PARAMETER_NAME="/librechat/allow_registration"

# Récupération de la valeur du paramètre depuis AWS SSM
PARAM_VALUE=$(aws ssm get-parameter --name "$PARAMETER_NAME" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION)

# Conversion de la valeur 'on' ou 'off' en 'true' ou 'false'
if [ "$PARAM_VALUE" == "on" ]; then
    PARAM_VALUE="true"
elif [ "$PARAM_VALUE" == "off" ]; then
    PARAM_VALUE="false"
fi

# Mise à jour de la configuration ALLOW_REGISTRATION dans le fichier .env si nécessaire
if [ "$PARAM_VALUE" == "true" ] || [ "$PARAM_VALUE" == "false" ]; then
    # Extraction de la valeur actuelle de ALLOW_REGISTRATION dans .env
    CURRENT_VALUE=$(grep -m 1 "^ALLOW_REGISTRATION=" "$ENV_FILE" | cut -d'=' -f2)
    # Comparaison de la valeur actuelle avec la nouvelle valeur
    if [ "$PARAM_VALUE" != "$CURRENT_VALUE" ]; then
        # Mise à jour de ALLOW_REGISTRATION dans le fichier .env si différent
        sed -i "s/ALLOW_REGISTRATION=.*/ALLOW_REGISTRATION=$PARAM_VALUE/" "$ENV_FILE"
        # Message indiquant la mise à jour et redémarrage des services Docker
        echo "Mise à jour de ALLOW_REGISTRATION de '$CURRENT_VALUE' à '$PARAM_VALUE'. Redémarrage des services Docker..."
        (cd /home/ubuntu/LibreChat && sudo docker-compose -f ./deploy-compose.yml down && sudo docker-compose -f ./deploy-compose.yml up -d)
        echo "Services Docker redémarrés."
    else
        # Message indiquant qu'aucun changement n'est nécessaire
        echo "ALLOW_REGISTRATION est déjà défini à '$PARAM_VALUE'. Aucun redémarrage nécessaire."
    fi
else
    # Message d'erreur si la valeur du paramètre n'est pas 'true' ou 'false'
    echo "Valeur du paramètre $PARAMETER_NAME non définie correctement. Attendu 'on' ou 'off', reçu '$PARAM_VALUE'."
fi
EOF

# Rendre le script exécutable
chmod +x /usr/local/bin/update_registration.sh

# Planification du script pour s'exécuter toutes les minutes via cron
echo "* * * * * ubuntu /usr/local/bin/update_registration.sh" > /etc/cron.d/update_registration_job

# Mise à jour des clés API si elles existent dans AWS SSM Parameter Store
if OPENAI_API_KEY=$(aws ssm get-parameter --name "/librechat/openai" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null); then
    update_status "50% - .env OpenAI et DALL-E"
    sleep 2
    # Mise à jour de la clé OpenAI dans le fichier .env
    sed -i "s/OPENAI_API_KEY=user_provided/OPENAI_API_KEY=${OPENAI_API_KEY}/" .env
    # Utiliser la même clé pour DALL-E 3
    sed -i "s/# DALLE3_API_KEY=/DALLE3_API_KEY=${OPENAI_API_KEY}/" .env
    # Utiliser la même clé pour DALL-E 2
    sed -i "s/# DALLE2_API_KEY=/DALLE2_API_KEY=${OPENAI_API_KEY}/" .env  
else
    echo "La clé OpenAI n'existe pas dans AWS SSM Parameter Store."
fi

# Récupérer et mettre à jour la clé Google API et le Google Search ID, s'ils existent
if GOOGLE_SEARCH_API_KEY=$(aws ssm get-parameter --name "/librechat/googleapikey" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null); then
    update_status "60% - .env Google Search"
    sleep 2
    sed -i "s|GOOGLE_SEARCH_API_KEY=.*|GOOGLE_SEARCH_API_KEY=\"$GOOGLE_SEARCH_API_KEY\"|" .env
else
    echo "La clé Google API n'existe pas dans AWS SSM Parameter Store."
fi

if GOOGLE_CSE_ID=$(aws ssm get-parameter --name "/librechat/googlesearchid" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null); then
    sed -i "s|GOOGLE_CSE_ID=.*|GOOGLE_CSE_ID=\"$GOOGLE_CSE_ID\"|" .env
else
    echo "Le Google Search Engine ID n'existe pas dans AWS SSM Parameter Store."
fi

cp librechat.example.yaml librechat.yaml

# Récupérer et mettre à jour la clé Mistral AI, si elle existe
if MISTRAL_API_KEY=$(aws ssm get-parameter --name "/librechat/mistralai" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null); then
    update_status "70% - .env Mistral AI"
    sleep 2
    # Remplace la ligne contenant apiKey: '${MISTRAL_API_KEY}' par la clé réelle dans librechat.yaml
    sed -i "s|apiKey: '\${MISTRAL_API_KEY}'|apiKey: '${MISTRAL_API_KEY}'|" librechat.yaml
    # Vérifier ou activer les modèles par défaut pour Mistral
    sed -i '/- name:.*Mistral/,/default:/ s|default:.*|default: ['"'"'mistral-tiny'"'"', '"'"'mistral-small'"'"', '"'"'mistral-medium'"'"', '"'"'mistral-large-latest'"'"']|' librechat.yaml
else
    echo "La clé Mistral AI n'existe pas dans AWS SSM Parameter Store."
fi

# Récupérer et mettre à jour la clé Anthropic API, si elle existe
if ANTHROPIC_API_KEY=$(aws ssm get-parameter --name "/librechat/anthropic" --with-decryption --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null); then
    update_status "75% - .env Anthropic API"
    sleep 2
    sed -i "s/ANTHROPIC_API_KEY=.*/ANTHROPIC_API_KEY=${ANTHROPIC_API_KEY}/" .env
else
    echo "La clé Anthropic API n'existe pas dans AWS SSM Parameter Store."
fi

# Changement de propriétaire du répertoire LibreChat à l'utilisateur ubuntu
chown -R ubuntu:ubuntu /home/ubuntu/LibreChat

# Mise à jour du statut avant la sécurisation SSL
update_status "80% - Sécurisation du lancement web (ssl) "
sleep 2

# Création du répertoire et génération d'un certificat SSL auto-signé
sudo mkdir -p /etc/nginx/ssl
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt -subj "/CN=CertAutoGen"

cp client/nginx.conf client/nginx.conf.ori
# Modification de la configuration Nginx pour activer SSL
sed -i '/########################################  Non-SSL  ########################################/,/########################################  SSL  ########################################/ s/^/#/' client/nginx.conf
sed -i '/########################################  SSL  ########################################/,/#}/ s/^#//' client/nginx.conf
sed -i '/- .\/client\/nginx.conf:\/etc\/nginx\/conf.d\/default.conf/a \      - \/etc\/nginx\/ssl:\/etc\/nginx\/ssl' deploy-compose.yml
sed -i 's/^\(\s*\)ssl_trusted_certificate/#\1ssl_trusted_certificate/' client/nginx.conf
# Monter les certificats SSL dans le fichier deploy-compose.yml
sed -i '/- .\/client\/nginx.conf:\/etc\/nginx\/conf.d\/default.conf/a \      - \/etc\/nginx\/ssl:\/etc\/nginx\/ssl' deploy-compose.yml
# Corrige les directives dans nginx.conf pour la version récente de Nginx
sed -i '/listen 443 ssl;/!s/listen 443 ssl;/listen 443 ssl http2;/' client/nginx.conf
sed -i '/listen \[::\]:443 ssl;/!s/listen \[::\]:443 ssl;/listen [::]:443 ssl http2;/' client/nginx.conf
# Corriger les erreurs de commentaire sur les URLs
sed -i 's|https://docs.nginx.com/nginx/admin-guide/web-server/compression/|# https://docs.nginx.com/nginx/admin-guide/web-server/compression/|' client/nginx.conf
# Vérifie et corrige les commentaires mal interprétés comme des directives
sed -i 's|https://docs.nginx.com/nginx/admin-guide/web-server/compression/|# &|' client/nginx.conf


# Téléchargement des paramètres Diffie-Hellman pour renforcer la sécurité SSL
curl "https://ssl-config.mozilla.org/ffdhe2048.txt" | sudo tee /etc/nginx/ssl/dhparam > /dev/null

# Mise à jour du statut avant le premier lancement de LibreChat
update_status "90% - 1er démarrage de Librechat"

# Démarrage des services LibreChat via Docker Compose
sudo docker-compose -f ./deploy-compose.yml up -d

# Message de fin d'installation
echo -e "\e[1;32m\n########################################################\n"
echo -e "    Script user data terminé avec succès !    \n"
echo -e "########################################################\e[0m\n"

# Mise à jour finale du statut
update_status "100% - Installation terminée"
