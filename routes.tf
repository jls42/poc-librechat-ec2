# Création d'une table de routage dans le VPC spécifié.
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id # Spécifie l'ID du VPC auquel la table de routage sera attachée.

  # Définition d'une route par défaut pour diriger tout le trafic vers Internet via l'Internet Gateway.
  route {
    cidr_block = "0.0.0.0/0"                  # Cible tout le trafic Internet (adresse IP externe).
    gateway_id = aws_internet_gateway.main.id # Spécifie l'ID de l'Internet Gateway comme cible de la route.
  }

  # Tags attribués à la table de routage pour une identification facile.
  tags = {
    Name = "main-route-table" # Nom de la table de routage.
  }
}

# Association de la table de routage créée précédemment à chaque subnet.
resource "aws_route_table_association" "main" {
  for_each = aws_subnet.subnet # Utilise une boucle pour associer chaque subnet à la table de routage.

  subnet_id      = each.value.id           # L'ID du subnet courant dans la boucle.
  route_table_id = aws_route_table.main.id # L'ID de la table de routage à associer au subnet.
}
