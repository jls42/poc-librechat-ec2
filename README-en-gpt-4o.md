[[_TOC_]]


# Fully Automated Deployment of Librechat on AWS EC2

This project can be deployed and used in two ways:
- Either from the gitlab.com interface via a pipeline
- Or from your own Linux workstation

In both cases, the central file used is the same, namely the `export.sh` file. For the infrastructure, it is a deployment with `Terraform` (Infrastructure As Code). As for the installation of components in the EC2, it is carried out via `user-data` using a `bash` script.

Adding an API key is not mandatory for deployment, except for the use of Mistral AI, for which it is necessary to provide a key to function properly. In other cases, even without a key, it will be requested directly in the deployed Librechat interface.  
This project supports the automatic deployment of API keys for OpenAI, Mistral AI, Claude, and the API key associated with [Google Search](https://docs.librechat.ai/features/plugins/google_search.html).  

**The default installation of Librechat** makes port 80 accessible without enabling port 443. This configuration is not recommended for an application that requires authentication.  
In the context of this Librechat project on EC2, **port 443 is enabled by default with a self-signed certificate**. Port 80 redirects to port 443. During use, an HTTPS warning will appear in the browser. This warning is "normal" because the certificate is not signed by a recognized certification authority. However, **the use of HTTPS protocol provides security against password theft on the network**.  

The Terraform code of the project deploys the endpoints necessary for its proper functioning. If you already have these endpoints, you can comment out the associated code. To do so, comment out the content of the `endpoints.tf` file.   

# Architecture  
![alt text](/img/architecture.drawio.png)

# Deployment from your workstation

Prerequisites:
Ensure the AWS client is configured to perform deployment actions via API (or provide the information as environment variables).  

Edit the export.sh file and choose a bucket name for the `Terraform` state  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

If you want to add any key(s):  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
Then run the `export.sh` script  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
Here is the image showing the result obtained slightly accelerated:  
![alt text](/img/check_deploy_status.gif)

# Deployment via gitlab.com

Create a git project and push this repository into it, and add the CI variables.

Mandatory variables:
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

Optional variables:
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

Then go to Build->Pipeline and launch the associated jobs (via the "play" icon):

The job of key(s) if you have added any, otherwise directly on Terraform Verification.
![alt text](/img/pipeline.png)


# Documentation of the export.sh file

This Bash script is a command line interface (CLI) to facilitate the management and deployment of AWS infrastructures using Terraform and the management of EC2 instances. The script provides several functions to manage AWS resources, API keys, and connect to EC2 instances.

## Initial setup and definition of global variables

- Activates the export of all variables defined from this point onward.

![alt text](/img/export.sh.gif)

## Definition of variables to configure the AWS and Terraform environment - `BUCKET_STATE`: Name of the S3 bucket to store the Terraform state.
- `AWS_DEFAULT_REGION`: Default AWS region used.
- `USERNAME`: Username for SSH connection.
- `PUBLIC_KEY_FILE`: Location of the SSH public key for authentication.
- `TF_VAR_region`: Sets the AWS region as an environment variable for Terraform.

## Verification and installation of awscli

- Checks if awscli is installed, if not, installs it.

## Creation and configuration of the S3 bucket for Terraform

- Creates an S3 bucket to store the Terraform state, if not already done, and enables bucket versioning.

## Initialization of Terraform

- Initializes Terraform with the necessary configurations to use the S3 bucket as a backend.

## Functions

1. `terraform_plan()`: Plans Terraform changes by updating modules, validating configuration, and creating an execution plan.
2. `terraform_apply()`: Applies Terraform changes from the execution plan.
3. `terraform_destroy()`: Destroys all configured Terraform resources and attempts to delete the deployment status in AWS SSM Parameter Store.
4. `ssh_connect()`: Connects to an EC2 instance via SSH after sending the public key.
5. `ssm_connect()`: Connects to an EC2 instance via AWS SSM.
6. `delete_parameter()`: Deletes a specific parameter in AWS SSM Parameter Store.
7. `manage_openai_key()`: Manages OpenAI API keys in AWS SSM Parameter Store (adding and deleting).
8. `manage_mistralai_key()`: Manages Mistral AI keys in AWS SSM Parameter Store (adding and deleting).
9. `manage_anthropic_key()`: Manages Anthropic AI keys in AWS SSM Parameter Store (adding and deleting).
10. `manage_google_api_key()`: Manages Google API keys in AWS SSM Parameter Store (adding and deleting).
11. `manage_google_cse_id()`: Manages the Google Search Engine ID in AWS SSM Parameter Store (adding and deleting).
12. `check_deployment_status()`: Checks the deployment status of an instance by consulting a specific parameter in AWS SSM Parameter Store.
13. `manage_allow_registration()`: Enables or disables registration on LibreChat by updating a parameter in AWS SSM Parameter Store.
14. `manage_ssm_parameters()`: Manages parameters in AWS SSM Parameter Store according to the specified action (list, view, delete).
15. `read_spot_request_type()`: Retrieves the Spot request type for an EC2 instance.
16. `manage_ec2_instance()`: Manages the EC2 instance according to the specified action (stop, start, status).
17. `display_help()`: Displays the available commands for the user.

## Exporting functions

- Exports the functions so they are available for later use.

## Displaying help

- Displays help to the user after completing initialization.

## Using the functions

### terraform_plan

This function allows planning Terraform changes. It updates Terraform modules, validates the Terraform configuration, and creates a Terraform execution plan.

To use the `terraform_plan` function, simply execute:
```bash
terraform_plan
```
### terraform_apply

This function applies Terraform changes from the execution plan.

To use the `terraform_apply` function, simply execute:
```bash
terraform_apply
```
### terraform_destroy

This function destroys all configured Terraform resources and attempts to delete the deployment status in AWS SSM Parameter Store.

To use the `terraform_destroy` function, simply execute:
```bash
terraform_destroy
```
### ssh_connect

This function connects to an EC2 instance via SSH after sending the public key.

To use the `ssh_connect` function, simply execute:
```bash
ssh_connect
```
### ssm_connect

This function connects to an EC2 instance via AWS SSM.

To use the `ssm_connect` function, simply execute:
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

This function is used to manage OpenAI API keys in AWS SSM Parameter Store (adding and removing).

To use the `manage_openai_key` function, execute:
```bash
manage_openai_key put [key]
```
to add a key, and
```bash
manage_openai_key delete
```
to remove the key.

### manage_mistralai_key

This function is used to manage Mistral AI keys in AWS SSM Parameter Store (adding and removing).

To use the `manage_mistralai_key` function, execute:
```bash
manage_mistralai_key put [key]
```
to add a key, and
```bash
manage_mistralai_key delete
```
to remove the key.

### manage_anthropic_key

This function is used to manage Anthropic AI keys in AWS SSM Parameter Store (adding and removing).

To use the `manage_anthropic_key` function, execute:
```bash
manage_anthropic_key put [key]
```
to add a key, and
```bash
manage_anthropic_key delete
```
to remove the key.

### manage_google_api_key

This function is used to manage Google API keys in AWS SSM Parameter Store (adding and removing).

To use the `manage_google_api_key` function, execute:
```bash
manage_google_api_key put [key]
```
to add a key, and
```bash
manage_google_api_key delete
```
to remove the key.

### manage_google_cse_id

This function is used to manage the Google Search Engine ID in AWS SSM Parameter Store (adding and removing).

To use the `manage_google_cse_id` function, execute:
```bash
manage_google_cse_id put [key]
```
to add a key, and
```bash
manage_google_cse_id delete
```
to remove the key.

### check_deployment_status

This function checks the deployment status of an instance by consulting a specific parameter in AWS SSM Parameter Store.

To use the `check_deployment_status` function, simply execute:
```bash
check_deployment_status
```
### manage_allow_registration

This function allows enabling or disabling registration on LibreChat by updating a parameter in AWS SSM Parameter Store.

To use the `manage_allow_registration` function, execute:
```bash
manage_allow_registration on
```
to enable registration, and
```bash
manage_allow_registration off
```
to disable registration.

### manage_ssm_parameters

This function manages parameters in AWS SSM Parameter Store according to the specified action (list, view, delete).

To use the `manage_ssm_parameters` function, execute:
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
to list the parameters,
```bash
manage_ssm_parameters view
```
to display parameter values, and
```bash
manage_ssm_parameters delete
```
to delete all parameters.

### manage_ec2_instance

This function manages the EC2 instance according to the specified action (stop, start, status).

To use the `manage_ec2_instance` function, execute:
```bash
manage_ec2_instance stop
```
to stop the instance,
```bash
manage_ec2_instance start
```
to start the instance, and
```bash
manage_ec2_instance status
```

To display the instance status:  

![alt text](/img/status.png)

### display_help

This function displays available commands for the user.

To use the `display_help` function, simply execute:
```bash
display_help
```

## Remarks and Best Practices

1. Ensure you have correctly configured your AWS credentials before using this script. You can configure your credentials using the `aws configure` command or by setting the environment variables `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` (if necessary).
2. Before using the `terraform_plan`, `terraform_apply`, and `terraform_destroy` functions, make sure you have correctly configured your Terraform configuration file (e.g., `main.tf`). This file must contain the definitions of the AWS resources you wish to create, update, or delete.
3. The `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, and `manage_google_cse_id` functions store API keys in AWS SSM Parameter Store. Be sure to manage these keys carefully and follow security best practices to avoid unauthorized access.
4. The `manage_ec2_instance` function manages the EC2 instance based on the Spot request type. Make sure you understand the implications of the different Spot request types before using this function. You can learn more about Spot instances in the [AWS documentation](https://aws.amazon.com/ec2/spot/).
5. It is recommended to always check the deployment status of an instance using the `check_deployment_status` function after applying Terraform changes. This will ensure that the instance is properly configured and ready to be used.
6. When using the `ssh_connect` function to connect to an EC2 instance, ensure that the corresponding SSH public key has been correctly added to the instance. You can add the SSH public key to the instance using the AWS console, the AWS command line interface, or by specifying it in your Terraform configuration file.
7. This script is designed to be used with AWS and Terraform. Be sure to install and configure these tools correctly before using the script. You can find installation and configuration instructions for AWS CLI in the [AWS documentation](https://aws.amazon.com/cli/) and for Terraform in the [Terraform documentation](https://www.terraform.io/).
8. It is recommended to always back up your data and configurations before making changes to your AWS infrastructure using this script. This will allow you to easily recover your data and configurations in case of a problem.
9. Feel free to customize and extend this script according to your specific needs. You can add new functions, modify existing functions, or adapt the script to work with other AWS services or other infrastructure as code tools.
10. If you want to connect to the server via SSM, you need to deploy the session-manager plugin. Example for Ubuntu:
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

By following these remarks and best practices, you will be able to make the most of this script to effectively manage your AWS infrastructure and EC2 instances.

## Troubleshooting

1. **AWS Authentication Errors**: If you encounter authentication errors while using this script, check that your AWS credentials are correctly configured. You can do this by using the `aws configure` command or by setting the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` (if necessary) environment variables. Also, make sure you have the appropriate permissions to access the AWS resources specified in your Terraform configuration file.
2. **Terraform Configuration Errors**: If you encounter errors while using the `terraform_plan`, `terraform_apply`, or `terraform_destroy` functions, verify that your Terraform configuration file is correctly configured. Ensure that the AWS resources are properly defined and that dependencies between resources are correctly managed. You can use the `terraform validate` command to check the validity of your Terraform configuration file.
3. **SSH Connection Errors**: If you encounter errors while using the `ssh_connect` function, verify that the corresponding SSH public key has been correctly added to the EC2 instance. Also, make sure that the username and instance IP address are correct. You can check this information using the AWS console or the AWS command line interface.
4. **AWS SSM Connection Errors**: If you encounter errors while using the `ssm_connect` function, verify that your EC2 instance is properly configured to use AWS SSM. Ensure that the instance has an appropriate IAM role and that the security group's security settings allow inbound traffic from the SSM IP addresses. You can learn more about the AWS SSM configuration in the [AWS documentation](https://aws.amazon.com/ssm/).

5. **API Key Management Errors**: If you encounter errors when using the `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, or `manage_google_cse_id` functions, check that you have the appropriate permissions to access AWS SSM Parameter Store. Also, ensure that the API keys are correctly formatted and that you provide the correct arguments to these functions.

6. **EC2 Instance Management Errors**: If you encounter errors when using the `manage_ec2_instance` function, verify that you have the appropriate permissions to manage EC2 instances in your AWS account. Also ensure that the instance ID is correct and that you provide the correct arguments to this function.

In case of any issues with this script, feel free to consult the AWS and Terraform documentation, as well as forums and online resources for assistance. You can also contact AWS support if you need additional help.

By following these troubleshooting and problem resolution tips, you should be able to resolve most of the issues you might encounter when using this script to manage your AWS infrastructure and EC2 instances.

# Documentation for the gitlab-ci.yml File

This GitLab CI/CD file (gitlab-ci.yml) describes a deployment pipeline to manage AWS infrastructure and EC2 instances using Terraform, utilizing the functions defined in the previously documented Bash script.

## Variables

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID`: API keys to configure in the GitLab CI/CD interface.
- `TERRAFORM_VERSION`: Version of the Terraform Docker image used to execute the tasks.
- `TF_VAR_region`: Default AWS region.

## Stages

- `Optional Prerequisites`: Stage to add or remove API keys.
- `Verifications`: Stage to verify Terraform changes.
- `Deployments`: Stage to apply Terraform changes and check deployment status.
- `Management`: Stage to manage EC2 instances and application settings.
- `Deletions`: Stage to delete Terraform resources and API keys.

## .terraform_template

This is a template for Terraform tasks, defining the Docker image, entry, and pre-script commands.

## Tasks

1. **Terraform Verification**: This task checks Terraform modifications by running `terraform_plan` and saves the plan in an artifact.
2. **Terraform Deployment**: This task applies Terraform changes by running `terraform_apply` and checks the deployment status using `check_deployment_status`.
3. **Terraform Deletion**: This task deletes Terraform resources by running `terraform_destroy`.
4. **Librechat Open Registration**: This task enables registrations in the Librechat application by using `manage_allow_registration on`.
5. **Librechat Close Registration**: This task disables registrations in the Librechat application by using `manage_allow_registration off`.
6. **EC2 Start**: This task starts the EC2 instance by using `manage_ec2_instance start`.
7. **EC2 Stop**: This task stops the EC2 instance by using `manage_ec2_instance stop`.
8. **EC2 Status**: This task displays the EC2 instance status by using `manage_ec2_instance status`.
9. **Key(s) - Addition**: This task adds API keys using the corresponding `manage_*_key put` functions. **Key(s) - Deletion**: This task deletes API keys using the corresponding `manage_*_key delete` functions.

## Full infrastructure deployment time + EC2 OS configuration in less than 6 mins:
![alt text](/img/pipeline_apply_time.png)

## Remarks and Best Practices

1. Make sure to properly configure the variables `OPENAI_KEY`, `MISTRALAI_KEY`, `GOOGLE_API_KEY`, and `GOOGLE_CSE_ID` in the GitLab CI/CD interface before executing the pipeline.
2. Be sure to use an appropriate version of the Terraform Docker image for your infrastructure.
3. Remember to update the corresponding Bash script when you make changes to this GitLab CI/CD file.
4. Follow security best practices for managing API keys and AWS credentials.

By following these remarks and best practices, you can use this GitLab CI/CD file to effectively manage your AWS infrastructure and EC2 instances using Terraform and the functions defined in the Bash script.

## Troubleshooting and Problem Resolution

1. **AWS Authentication Errors**: If you encounter authentication errors during the execution of the GitLab CI/CD pipeline, check if your AWS credentials are properly configured. Ensure that the environment variables `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` (if necessary) are defined and valid. You can configure them in the GitLab CI/CD interface or in the Bash script.
2. **Terraform Configuration Errors**: If you encounter errors during the execution of Terraform tasks (`terraform_plan`, `terraform_apply`, or `terraform_destroy`), check if your Terraform configuration file is properly set up. Ensure that AWS resources are correctly defined and that dependencies between resources are properly managed.
3. **API Key Management Errors**: If you encounter errors when adding or deleting API keys, check if you have the appropriate permissions to access AWS SSM Parameter Store. Also, ensure that the API keys are properly formatted and that you provide the correct arguments to the `manage_*_key` functions.
4. **EC2 Instance Management Errors**: If you encounter errors while using the `manage_ec2_instance` functions, check if you have the appropriate permissions to manage EC2 instances in your AWS account. Also, ensure that the instance ID is correct and that you provide the correct arguments to these functions.

In case of issues with this GitLab CI/CD file, feel free to consult AWS, Terraform, and GitLab CI/CD documentation, as well as forums and online resources for assistance. You can also contact AWS or GitLab support if you need additional help.

By following these troubleshooting and problem resolution tips, you should be able to resolve most issues you might encounter while using this GitLab CI/CD file to manage your AWS infrastructure and EC2 instances using Terraform and the functions defined in the Bash script.

# LibreChat EC2 User Data Script

This script is designed to run on an Amazon Elastic Compute Cloud (EC2) instance and automate the configuration process of the LibreChat application. It performs various tasks such as updating the operating system, installing required packages, configuring Docker, and deploying the LibreChat application.

## Script Overview

1. Update the operating system and install required packages.
2. Configure Docker and Docker Compose.
3. Clone and configure the LibreChat application.
4. Configure SSL certificates for secure communication.
5. Launch the LibreChat application using Docker Compose.

## Script Details ### Update of the Operating System and Installation of Required Packages

The script begins by updating the operating system and installing required packages such as `ca-certificates`, `curl`, and `awscli`. This ensures that the system has the necessary dependencies to run the LibreChat application.

### Configuration of Docker and Docker Compose

The script then installs Docker and Docker Compose, which are used to deploy the LibreChat application in a containerized environment. It also adds the current user to the Docker group, allowing the user to execute Docker commands without requiring root privileges.

### Cloning and Configuring the LibreChat Application

The script clones the LibreChat repository from GitHub and configures the required configuration files. It also retrieves the necessary API keys and tokens from AWS Systems Manager (SSM) Parameter Store and updates the configuration files accordingly.

### SSL Certificate Configuration for Secure Communication

To secure communication between the client and server, the script generates a self-signed SSL certificate and modifies the Nginx configuration to enable SSL. It also downloads the Diffie-Hellman parameters to strengthen SSL encryption.

### Launching the LibreChat Application Using Docker Compose

Finally, the script launches the LibreChat application using Docker Compose. This starts the containerized application and makes it accessible to users.

## Error Management and Logging

The script includes error management and logging mechanisms to help troubleshoot issues that may arise during the configuration process. It redirects the standard and error output streams to a log file located at `/var/log/user-data.log`. Additionally, the script uses a trap function to capture errors that occur during execution and update the deployment status in AWS SSM Parameter Store accordingly.

## Checking Deployment Status

The function `check_deployment_status`, which is not included in this script but mentioned in the provided information, is used to check the deployment status of an EC2 instance by retrieving a specific parameter from AWS SSM Parameter Store. This function is called in the `export.sh` script and provides updates on the deployment progress.

## Terraform Integration

This user data script is called in the Terraform configuration file `ec2.tf`, which creates an EC2 instance and applies the specified parameters. The user data script is passed to the instance using the `user_data` argument, ensuring that the script is executed when the instance is launched.

## File Information

Name: `user_data.sh`   
Location: `${path.module}/scripts/user_data.sh`   
Usage: Automatically runs when the EC2 instance is launched, as specified in the Terraform configuration file `ec2.tf`.   

# Script check_spot.py  

This Python script allows you to check and update the spot prices of EC2 instances in the Terraform variables file (`variables.tf`). Using this script facilitates cost management and ensures that the spot prices used for instances are up-to-date.  

To use this script, you must first ensure you have installed Python 3 and Boto3, the AWS SDK for Python. Then, you can run the script with the `--update` option to update the spot prices in the `variables.tf` file. The script supports different types of instances and automatically retrieves the availability zone (AZ) of the specified instance.  

The script begins by configuring the argument parser and defining AWS parameters, such as the region, instance types, and the path of the `variables.tf` file. Then, it initializes an EC2 client to interact with the AWS EC2 service. The `read_max_spot_prices()` function reads the current maximum spot prices from the `variables.tf` file and returns a dictionary containing instance types as keys and maximum prices as values.

The `get_instance_az()` function obtains the availability zone (AZ) of the instance specified by the Name tag using the EC2 client. If no instance is found, it returns a default AZ.

The `check_and_update_spot_prices()` function checks and updates the maximum spot prices in the `variables.tf` file if necessary. It calls `read_max_spot_prices()` to get the current prices, then retrieves the spot price history for each instance type and the specified AZ. If the current price is higher than the configured price, the function updates the configured price.

The `update_variables_file()` function updates the `variables.tf` file with the new spot prices. It takes as input a dictionary containing instance types as keys and new prices as values.

Finally, the main script calls `check_and_update_spot_prices()` to check and update spot prices. If the `--update` argument is specified when running the script and there are new prices, the `update_variables_file()` function is called to update the `variables.tf` file.

In summary, this Python script simplifies spot price management for EC2 instances by automating the process of checking and updating prices in a Terraform variables file.

# Documentation Terraform-docs

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Modules

No modules.

## Resources | Name | Type |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs | Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | Allows enabling or disabling registration | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | List of IP addresses allowed to access the EC2 Instance Connect endpoint | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | Name of the filter for the AWS AMI | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | Time to automatically stop the EC2 instance | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | Availability zones for the subnets | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Instance type for the EC2 instance | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | List of ports to open | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | Definition of the AWS region where the resources will be deployed. | `string` | n/a | yes |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | Maximum price for each instance type | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | Spot request type (persistent or one-time) | `string` | `"persistante"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | CIDR blocks for the subnets | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | Index of the subnet to use [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | EBS volume size in GB | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | CIDR block for the VPC | `string` | `"10.0.0.0/24"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | The ID of the EC2 instance |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | The public IP address of the EC2 instance |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |

# Author

Julien LE SAUX  
Email : contact@jls42.org

# Disclaimer

BY USING THIS SCRIPT, YOU ACKNOWLEDGE THAT YOU DO SO AT YOUR OWN RISK. THE AUTHOR IS NOT RESPONSIBLE FOR ANY CHARGES INCURRED, DAMAGES OR LOSSES RESULTING FROM THE USE OF THIS SCRIPT. MAKE SURE YOU UNDERSTAND THE COSTS ASSOCIATED WITH AWS BEFORE USING THIS SCRIPT.

# License

This project is licensed under GPL.

**This document was translated from the fr version to the en language using the gpt-4o model. For more information on the translation process, visit https://gitlab.com/jls42/ai-powered-markdown-translator.**

