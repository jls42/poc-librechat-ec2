#!/bin/bash

# Configuration initiale et définition des variables globales.
set -a  # Active l'exportation de toutes les variables définies à partir de ce point.

# Définition des variables pour configurer l'environnement AWS et Terraform.
BUCKET_STATE="changer-mon-nom-de-bucket"  # Nom du bucket S3 pour stocker l'état Terraform.
AWS_DEFAULT_REGION="eu-west-1"  # Région AWS utilisée par défaut.
USERNAME="ubuntu"  # Nom d'utilisateur pour la connexion SSH, adapté à l'AMI utilisée.
PUBLIC_KEY_FILE="~/.ssh/id_rsa.pub"  # Emplacement de la clé publique SSH pour l'authentification.
echo "Region par défaut : $AWS_DEFAULT_REGION"
TF_VAR_region="${AWS_DEFAULT_REGION}"  # Définit la région AWS comme variable d'environnement pour Terraform.

# Vérifie si awscli est installé, sinon il l'installe.
if ! command -v aws &> /dev/null; then
    echo "awscli n'est pas installé. Installation en cours..."
    pip install awscli  # Utilise pip pour installer awscli.
fi

# Crée un bucket S3 pour stocker l'état de Terraform, si ce n'est pas déjà fait, et active le versioning du bucket.
echo "Vérification du bucket S3 pour l'état de Terraform..."
aws s3api head-bucket --bucket ${BUCKET_STATE} || aws s3 mb s3://${BUCKET_STATE}
aws s3api put-bucket-versioning --bucket ${BUCKET_STATE} --versioning-configuration Status=Enabled

# Initialise Terraform avec les configurations nécessaires pour utiliser le bucket S3 comme backend.
echo "Initialisation de Terraform..."
terraform init -input=false -backend-config="bucket=${BUCKET_STATE}" -backend-config="region=${AWS_DEFAULT_REGION}" -backend-config="key=terraform.tfstate"

# Fonction pour planifier les modifications Terraform.
terraform_plan() {
  export TF_VAR_region="${AWS_DEFAULT_REGION}"
  terraform get -update  # Met à jour les modules Terraform.
  terraform validate  # Valide la configuration Terraform.
  terraform plan -out=tfplan -input=false  # Crée un plan d'exécution Terraform.
}

# Fonction pour appliquer les modifications Terraform.
terraform_apply() {
  export TF_VAR_region="${AWS_DEFAULT_REGION}"
  terraform apply -input=false tfplan  # Applique le plan d'exécution Terraform.
}

# Fonction pour détruire les ressources Terraform.
terraform_destroy() {
  terraform destroy -input=false -auto-approve  # Détruit toutes les ressources Terraform configurées.
  echo "Tentative de suppression du statut de déploiement dans AWS SSM Parameter Store..."
  aws ssm delete-parameter --name "/librechat/deployment-status" --region $AWS_DEFAULT_REGION && echo "Statut de déploiement supprimé avec succès." || echo "Le statut de déploiement n'existait pas ou ne pouvait pas être supprimé."
}

# Fonction pour se connecter à une instance EC2 via SSH après avoir envoyé la clé publique.
ssh_connect() {
  INSTANCE_ID=$(terraform output -raw instance_id)  # Récupère l'ID de l'instance depuis la sortie Terraform.
  if [ -z "$INSTANCE_ID" ]; then
    echo "L'ID de l'instance EC2 n'a pas pu être récupéré. Vérifiez votre configuration Terraform."
    return 1
  fi
  # Récupère l'adresse IP publique de l'instance.
  IP_ADDRESS=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[*].Instances[*].PublicIpAddress" --output text --region $REGION)
  if [ -z "$IP_ADDRESS" ]; then
    echo "L'adresse IP de l'instance EC2 n'a pas pu être récupérée. Vérifiez que l'instance est en cours d'exécution et a une IP publique."
    return 1
  fi
  # Envoie la clé publique à l'instance pour permettre la connexion SSH.
  aws ec2-instance-connect send-ssh-public-key --region $REGION --instance-id $INSTANCE_ID --instance-os-user $USERNAME --ssh-public-key file://$PUBLIC_KEY_FILE
  if [ $? -ne 0 ]; then
    echo "Impossible d'envoyer la clé publique à l'instance."
    return 1
  fi
  echo "L'adresse IP publique de l'instance est : $IP_ADDRESS"
  echo "Tentative de connexion SSH à l'instance EC2..."
  PRIVATE_KEY_FILE="${PUBLIC_KEY_FILE%.*}"  # Extrait le chemin de la clé privée à partir de la clé publique.
  ssh -i $PRIVATE_KEY_FILE $USERNAME@$IP_ADDRESS  # Se connecte à l'instance via SSH.
}

# Fonction pour se connecter à une instance EC2 via AWS SSM.
ssm_connect() {
  INSTANCE_ID=$(terraform output -raw instance_id)  # Récupère l'ID de l'instance depuis la sortie Terraform.
  if [ -z "$INSTANCE_ID" ]; then
    echo "L'ID de l'instance EC2 n'a pas pu être récupéré. Vérifiez votre configuration Terraform."
    return 1
  fi
  # Vérifie l'état de l'instance pour s'assurer qu'elle est en cours d'exécution.
  INSTANCE_STATE=$(aws ec2 describe-instances --instance-ids "$INSTANCE_ID" --query "Reservations[*].Instances[*].State.Name" --output text --region $AWS_DEFAULT_REGION)
  if [ "$INSTANCE_STATE" != "running" ]; then
    echo "L'instance EC2 d'ID $INSTANCE_ID n'existe pas ou n'est pas en état 'running'. État actuel : $INSTANCE_STATE"
    return 1
  fi
  echo "L'instance EC2 d'ID $INSTANCE_ID est en état 'running'."
  echo "Tentative de connexion à l'instance EC2 via SSM..."
  aws ssm start-session --target "$INSTANCE_ID"  # Démarre une session SSM avec l'instance.
}

# Suppression d'un paramètre spécifique dans AWS SSM Parameter Store.
delete_parameter() {
  local parameter_path="$1"  # Chemin du paramètre à supprimer.
  echo "Suppression de la clé $parameter_path dans AWS SSM Parameter Store..."
  if aws ssm delete-parameter --name "$parameter_path" --region $AWS_DEFAULT_REGION 2>/dev/null; then
    echo "$parameter_path supprimé avec succès."
  else
    echo "$parameter_path n'existe pas ou ne pouvait pas être supprimé."
  fi
}

# Gestion des clés d'API OpenAI dans AWS SSM Parameter Store.
manage_openai_key() {
  local action=$1  # Action à effectuer ('put' pour ajouter, 'delete' pour supprimer).
  local key_value=$2  # Valeur de la clé à gérer.
  local parameter_path="/librechat/openai"  # Chemin du paramètre dans SSM Parameter Store.
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Une clé OpenAI doit être fournie pour l'action 'put'."
        return 1
      fi
      echo "Ajout de la clé OpenAI dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}

# Gestion des clés Mistral AI dans AWS SSM Parameter Store.
manage_mistralai_key() {
  local action=$1
  local key_value=$2
  local parameter_path="/librechat/mistralai"
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Une clé Mistral AI doit être fournie pour l'action 'put'."
        return 1
      fi
      echo "Ajout de la clé Mistral AI dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}

# Gestion des clés Anthropic API dans AWS SSM Parameter Store.
manage_anthropic_key() {
  local action=$1  # Action à effectuer ('put' pour ajouter, 'delete' pour supprimer).
  local key_value=$2  # Valeur de la clé à gérer.
  local parameter_path="/librechat/anthropic"  # Chemin du paramètre dans SSM Parameter Store.
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Une clé Anthropic API doit être fournie pour l'action 'put'."
        return 1
      fi
      echo "Ajout de la clé Anthropic API dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}


# Gestion des clés Google API dans AWS SSM Parameter Store.
manage_google_api_key() {
  local action=$1
  local key_value=$2
  local parameter_path="/librechat/googleapikey"
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Une clé Google API doit être fournie pour l'action 'put'."
        return 1
      fi
      echo "Ajout de la clé Google API dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}

# Gestion du Google Search Engine ID dans AWS SSM Parameter Store.
manage_google_cse_id() {
  local action=$1
  local key_value=$2
  local parameter_path="/librechat/googlesearchid"
  case "$action" in
    put)
      if [ -z "$key_value" ]; then
        echo "Erreur : Un Google Search Engine ID doit être fourni pour l'action 'put'."
        return 1
      fi
      echo "Ajout du Google Search Engine ID dans AWS SSM Parameter Store..."
      aws ssm put-parameter --name "$parameter_path" --value "$key_value" --type "SecureString" --overwrite
      ;;
    delete)
      delete_parameter "$parameter_path"
      ;;
    *)
      echo "Action non reconnue. Les actions valides sont 'put' et 'delete'."
      return 1
      ;;
  esac
}

# Vérifie l'état de déploiement d'une instance en consultant un paramètre spécifique dans AWS SSM Parameter Store.
check_deployment_status() {
  LAST_STATUS=""
  INSTANCE_ID=$(terraform output -raw instance_id)  # Récupère l'ID de l'instance Terraform.
  if [ -z "$INSTANCE_ID" ]; then
    echo "Aucune instance EC2 n'est actuellement déployée ou terraform output n'est pas configuré correctement."
    return 1
  fi
  IP_ADDRESS=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[*].Instances[*].PublicIpAddress" --output text --region $AWS_DEFAULT_REGION 2>/dev/null)
  URL="https://$IP_ADDRESS/"
  echo "Vérification de l'état de déploiement..."
  ATTENTE_STATUS=true  # Utilisée pour contrôler l'affichage du message d'attente.
  while true; do
    STATUS=$(aws ssm get-parameter --name "/librechat/deployment-status" --query "Parameter.Value" --output text --region $AWS_DEFAULT_REGION 2>/dev/null)
    if [ $? -ne 0 ]; then
      if [ "$ATTENTE_STATUS" = true ]; then
        echo -ne "\rEn attente des informations de statut de déploiement.\n"
        ATTENTE_STATUS=false  # Empêche la répétition du message.
      fi
      sleep 5
      continue
    else
      ATTENTE_STATUS=true  # Réinitialise pour le prochain cycle.
    fi
    if [[ "$STATUS" != "$LAST_STATUS" ]]; then
      if [[ "$LAST_STATUS" != "" ]]; then
        echo -e " \e[32m✓\e[0m"  # Affiche une coche verte pour le statut précédent.
      fi
      echo -ne "\rÉtat actuel du déploiement : $STATUS"
      LAST_STATUS="$STATUS"
      if [[ "$STATUS" == "100% - Installation terminée" ]]; then
        echo -e "\n\e[32m✓ Installation terminée avec succès\e[0m"
        echo -e "Accédez à l'instance Librechat via : $URL"
        break
      elif [[ "$STATUS" == "Echec de l'installation" ]]; then
        echo -e "\n\e[31m✗ Échec de l'installation\e[0m"
        exit 1
      fi
    fi
    sleep 1
  done
}

# Active ou désactive l'inscription sur LibreChat en mettant à jour un paramètre dans AWS SSM Parameter Store.
manage_allow_registration() {
  local action=$1  # 'on' pour activer, 'off' pour désactiver.
  local parameter_path="/librechat/allow_registration"
  local param_value="false"
  if [ "$action" == "on" ]; then
    param_value="true"
  elif [ "$action" == "off" ]; then
    param_value="false"
  else
    echo "Action non reconnue. Les actions valides sont 'on' et 'off'."
    return 1
  fi
  echo "Mise à jour du paramètre ALLOW_REGISTRATION à '$param_value' dans AWS SSM Parameter Store..."
  aws ssm put-parameter --name "$parameter_path" --value "$param_value" --type "String" --overwrite --region $AWS_DEFAULT_REGION
}

# Gère les paramètres dans AWS SSM Parameter Store selon l'action spécifiée (list, view, delete).
manage_ssm_parameters() {
  local action=$1
  local parameter_path="/librechat"
  echo "Action : ${action} sur les paramètres dans le chemin ${parameter_path}..."
  aws ssm get-parameters-by-path --path "${parameter_path}" --recursive --region $AWS_DEFAULT_REGION | jq -r '.Parameters[] | .Name' | while read -r parameter_name; do
    case "$action" in
      list)
        echo "Paramètre trouvé : ${parameter_name}"
        ;;
      view)
        echo "Contenu de ${parameter_name}:"
        aws ssm get-parameter --name "${parameter_name}" --with-decryption --region $AWS_DEFAULT_REGION | jq -r '.Parameter | .Value'
        ;;
      delete)
        echo "Suppression de ${parameter_name}..."
        aws ssm delete-parameter --name "${parameter_name}" --region $AWS_DEFAULT_REGION
        if [ $? -eq 0 ]; then
          echo "${parameter_name} supprimé avec succès."
        else
          echo "Échec de la suppression de ${parameter_name}."
        fi
        ;;
      *)
        echo "Action non reconnue. Les actions valides sont 'list', 'view' et 'delete'."
        return 1
        ;;
    esac
  done
}

# Récupère le type de demande Spot pour une instance EC2.
read_spot_request_type() {
  local instance_id=$1
  aws ec2 describe-spot-instance-requests --filters "Name=instance-id,Values=$instance_id" --query "SpotInstanceRequests[].Type" --output text --region $AWS_DEFAULT_REGION
}

# Gère l'instance EC2 en fonction de l'action spécifiée (stop, start, status).
manage_ec2_instance() {
  local action=$1
  local instance_id=$(terraform output -raw instance_id)
  local spot_request_type=$(read_spot_request_type)
  if [ -z "$instance_id" ]; then
    echo "L'ID de l'instance EC2 n'a pas pu être récupéré. Vérifiez votre configuration Terraform."
    return 1
  fi
  if [ "$spot_request_type" = "persistent" ]; then
    echo "L'action '$action' n'est pas disponible pour les instances Spot de type 'persistent'."
    return 1
  fi
  case $action in
    stop)
      echo "Arrêt de l'instance EC2 avec l'ID $instance_id..."
      aws ec2 stop-instances --instance-ids "$instance_id" --region $AWS_DEFAULT_REGION
      ;;
    start)
      echo "Démarrage de l'instance EC2 avec l'ID $instance_id..."
      aws ec2 start-instances --instance-ids "$instance_id" --region $AWS_DEFAULT_REGION
      echo "Attente que l'instance soit en état 'running'..."
      aws ec2 wait instance-running --instance-ids "$instance_id" --region $AWS_DEFAULT_REGION
      ;;
    status)
      echo "Récupération du statut de l'instance EC2 avec l'ID $instance_id..."
      local instance_status=$(aws ec2 describe-instances --instance-ids "$instance_id" --query "Reservations[*].Instances[*].State.Name" --output text --region $AWS_DEFAULT_REGION)
      echo "Statut de l'instance : $instance_status"
      ;;
    *)
      echo "Action '$action' non reconnue. Les actions valides sont 'stop', 'start', et 'status'."
      return 1
      ;;
  esac
  if [[ "$action" == "start" || "$action" == "status" ]]; then
    local ip_address=$(aws ec2 describe-instances --instance-ids "$instance_id" --query "Reservations[*].Instances[*].PublicIpAddress" --output text --region $AWS_DEFAULT_REGION)
    if [[ -n "$ip_address" ]]; then
      echo "Adresse IP publique de l'instance : $ip_address"
    else
      echo "L'adresse IP publique de l'instance n'est pas disponible."
    fi
  fi
}

# Affiche les commandes disponibles pour l'utilisateur.
display_help() {
  echo "Commandes disponibles :"
  echo "  terraform_plan                 - Valide et planifie les modifications Terraform."
  echo "  terraform_apply                - Applique les modifications Terraform."
  echo "  terraform_destroy              - Détruit toutes les ressources Terraform après confirmation."
  echo "  check_deployment_status        - Vérifie l'état d'avancement du déploiement."
  echo "  ssh_connect                    - Se connecte à l'instance EC2 via SSH."
  echo "  ssm_connect                    - Se connecte à l'instance EC2 via AWS SSM."
  echo "  manage_ec2_instance [action]   - Gère l'instance EC2 (actions : start, stop, status) en fonction du type de demande Spot."
  echo "  manage_openai_key [put/delete] [key]    - Ajoute ou supprime la clé OpenAI dans AWS SSM Parameter Store."
  echo "  manage_mistralai_key [put/delete] [key] - Ajoute ou supprime la clé Mistral AI dans AWS SSM Parameter Store."
  echo "  manage_anthropic_key [put/delete] [key]    - Ajoute ou supprime la clé Anthropic API dans AWS SSM Parameter Store."
  echo "  manage_google_api_key [put/delete] [key]          - Ajoute ou supprime la clé Google API dans AWS SSM Parameter Store."
  echo "  manage_google_cse_id [put/delete] [key]           - Ajoute ou supprime le Google Search Engine ID dans AWS SSM Parameter Store."
  echo "  manage_allow_registration [on/off] - Active ou désactive l'inscription sur LibreChat."
  echo "  manage_ssm_parameters [list/view/delete]     - Liste, affiche ou supprime tous les paramètres dans le chemin /librechat."
}

# Exporte les fonctions pour qu'elles soient disponibles pour une utilisation ultérieure.
export -f terraform_plan
export -f terraform_apply
export -f terraform_destroy
export -f check_deployment_status
export -f ssh_connect
export -f ssm_connect
export -f manage_ec2_instance
export -f manage_openai_key
export -f manage_mistralai_key
export -f manage_anthropic_key 
export -f manage_google_api_key
export -f manage_google_cse_id
export -f manage_allow_registration
export -f manage_ssm_parameters
export -f display_help

echo "L'initialisation est terminée."
display_help  # Affiche l'aide à l'utilisateur.
