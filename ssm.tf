
# Création d'un paramètre SSM pour gérer l'autorisation d'inscription
resource "aws_ssm_parameter" "allow_registration" {
  name  = "/librechat/allow_registration"           # Nom du paramètre
  type  = "String"                                  # Type du paramètre (chaîne de caractères)
  value = var.allow_registration ? "true" : "false" # Valeur du paramètre basée sur une variable Terraform
}

# Définition d'une politique IAM pour l'accès au Parameter Store SSM
data "aws_iam_policy_document" "ssm" {
  statement {
    actions = [
      "ssm:GetParameter",
      "ssm:PutParameter"
    ]                                                                                                              # Actions autorisées
    effect    = "Allow"                                                                                            # Effet de la politique (autorisation)
    resources = ["arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/librechat/*"] # Ressources ciblées par la politique
  }
}

# Création d'une politique IAM pour l'accès à SSM Parameter Store
resource "aws_iam_policy" "ssm" {
  name        = "ssm_parameter_store_access"                                        # Nom de la politique
  description = "Permet l'accès à SSM Parameter Store pour récupérer la clé OpenAI" # Description
  policy      = data.aws_iam_policy_document.ssm.json                               # Document de politique
}

# Association de la politique IAM à un rôle IAM pour permettre l'accès à SSM Parameter Store
resource "aws_iam_role_policy_attachment" "ssm" {
  role       = aws_iam_role.ec2_instance_role.name # Nom du rôle IAM
  policy_arn = aws_iam_policy.ssm.arn              # ARN de la politique IAM
}

# Association d'une politique IAM gérée par AWS au rôle IAM pour l'accès de base à SSM
resource "aws_iam_role_policy_attachment" "ssm_core" {
  role       = aws_iam_role.ec2_instance_role.id                      # ID du rôle IAM
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore" # ARN de la politique IAM gérée par AWS
}
