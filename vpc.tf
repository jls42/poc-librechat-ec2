# Définition d'une ressource VPC dans AWS
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr # Spécifie le bloc CIDR du VPC en utilisant la variable vpc_cidr définie ailleurs dans votre configuration

  # Configuration des tags pour le VPC
  tags = {
    Name = "main-vpc" # Attribution d'un nom au VPC pour une identification facile dans la console AWS ou via l'API
  }
}
