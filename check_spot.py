#!/usr/bin/env python3

import argparse
import boto3
import datetime
import re
import sys

# Configuration de l'analyseur d'arguments
parser = argparse.ArgumentParser(
    description="Vérifie et optionnellement met à jour les prix spot dans variables.tf selon l'AZ de l'instance spécifiée."
)
parser.add_argument(
    "--update",
    action="store_true",
    help="Mettre à jour le fichier variables.tf avec les prix spot actuels.",
)
args = parser.parse_args()

# Paramètres AWS
region = "eu-west-1"
instance_types = ["t3a.large", "t3a.medium", "t3a.small", "t3a.micro"]
variables_file_path = "variables.tf"

# Initialiser le client EC2
ec2_client = boto3.client("ec2", region_name=region)


def read_max_spot_prices():
    """
    Lit les prix spot maximaux actuels depuis le fichier variables.tf

    Returns:
        dict: Dictionnaire contenant les types d'instances comme clés et les prix maximaux comme valeurs
    """
    max_prices = {}
    try:
        with open(variables_file_path, "r") as file:
            content = file.read()
            matches = re.findall(r'"(t3a\.[a-z]+)"\s*=\s*"([0-9.]+)"', content)
            for instance_type, price in matches:
                max_prices[instance_type] = float(price)
    except FileNotFoundError:
        print(f"Le fichier {variables_file_path} n'a pas été trouvé.")
        sys.exit(1)
    return max_prices


def get_instance_az(ec2_client, instance_name_tag_value="LibreChat"):
    """
    Obtient l'AZ de l'instance spécifiée par le tag Name

    Args:
        ec2_client (boto3.client): Client EC2
        instance_name_tag_value (str): Valeur du tag Name de l'instance

    Returns:
        str: Nom de l'AZ
    """
    instances = ec2_client.describe_instances(
        Filters=[{"Name": "tag:Name", "Values": [instance_name_tag_value]}]
    )["Reservations"]
    if instances:
        az = instances[0]["Instances"][0]["Placement"]["AvailabilityZone"]
        return az
    else:
        # Retourne une AZ par défaut si aucune instance n'est trouvée
        return ec2_client.describe_availability_zones()["AvailabilityZones"][0][
            "ZoneName"
        ]


def check_and_update_spot_prices():
    """
    Vérifie et met à jour les prix spot maximaux dans variables.tf si nécessaire

    Returns:
        dict: Dictionnaire contenant les types d'instances comme clés et les nouveaux prix comme valeurs
    """
    max_spot_prices = read_max_spot_prices()
    new_prices = {}

    instance_az = get_instance_az(ec2_client)

    for instance_type in instance_types:
        # Obtenir l'historique du prix spot pour l'AZ et le type d'instance spécifiés
        response = ec2_client.describe_spot_price_history(
            InstanceTypes=[instance_type],
            ProductDescriptions=["Linux/UNIX"],
            StartTime=datetime.datetime.now() - datetime.timedelta(minutes=1),
            EndTime=datetime.datetime.now(),
            AvailabilityZone=instance_az,
        )

        # Vérifier si des données d'historique sont disponibles
        if response["SpotPriceHistory"]:
            current_price = float(response["SpotPriceHistory"][0]["SpotPrice"])
            print(
                f"Type d'instance : {instance_type}, AZ : {instance_az}, Prix courant AWS : {current_price}€, Prix configuré : {max_spot_prices.get(instance_type, 'N/A')}€"
            )
            # Si le prix courant est supérieur au prix configuré, mettre à jour le prix configuré
            if current_price > max_spot_prices.get(instance_type, 0):
                max_spot_prices[instance_type] = current_price
                new_prices[instance_type] = current_price

    return new_prices


def update_variables_file(new_prices):
    """
    Met à jour le fichier variables.tf avec les nouveaux prix spot

    Args:
        new_prices (dict): Dictionnaire contenant les types d'instances comme clés et les nouveaux prix comme valeurs
    """
    try:
        # Ouvrir le fichier en mode lecture pour récupérer son contenu
        with open(variables_file_path, "r") as file:
            lines = file.readlines()

        # Marqueurs pour identifier le début et la fin de la section à mettre à jour
        start_marker = 'variable "spot_max_price" {'
        end_marker = "}"
        in_spot_price_section = False
        updated_lines = []

        for line in lines:
            # Vérifier si la ligne courante marque le début de la section des prix spot
            if line.strip() == start_marker:
                in_spot_price_section = True
                updated_lines.append(line)
                continue

            # Vérifier si la ligne courante marque la fin de la section des prix spot
            if in_spot_price_section and line.strip() == end_marker:
                in_spot_price_section = False
                # Ajouter les prix spot mis à jour juste avant la fin de la section
                for instance_type, price in new_prices.items():
                    updated_lines.append(f'    "{instance_type}" = "{price}"\n')
                updated_lines.append(line)
                continue

            # Si on est à l'intérieur de la section des prix spot, ne pas ajouter les lignes existantes à updated_lines
            if in_spot_price_section:
                # Ignorer les lignes existantes pour les types d'instances à mettre à jour
                if any(instance_type in line for instance_type in new_prices.keys()):
                    continue
                else:
                    updated_lines.append(line)
            else:
                # En dehors de la section des prix spot, ajouter simplement la ligne au nouveau contenu
                updated_lines.append(line)

        # Réécrire le fichier avec le contenu mis à jour
        with open(variables_file_path, "w") as file:
            file.writelines(updated_lines)

        print("Le fichier 'variables.tf' a été mis à jour avec succès.")
    except Exception as e:
        print(f"Erreur lors de la mise à jour du fichier {variables_file_path}: {e}")


if __name__ == "__main__":
    print("Vérification des prix spot en cours...")
    new_prices = check_and_update_spot_prices()

    if args.update and new_prices:
        update_variables_file(new_prices)