# Récupération des informations d'identité de l'appelant AWS
data "aws_caller_identity" "current" {}

# Récupération de l'ID de l'AMI Ubuntu le plus récent selon les filtres
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"] # ID du propriétaire de l'AMI (Canonical pour Ubuntu)
  most_recent = true             # Sélectionne l'AMI le plus récent

  # Filtre pour sélectionner l'AMI par nom
  filter {
    name   = "name"
    values = [var.ami_filter_name] # Utilise une variable pour le nom de l'AMI
  }

  filter {
    name   = "description"
    values = ["*Ubuntu, 22.04 LTS, amd64 jammy*"]
  }

  # Filtre pour sélectionner l'AMI par type de virtualisation
  filter {
    name   = "virtualization-type"
    values = ["hvm"] # Type de virtualisation HVM
  }
}

# Création d'une instance EC2
resource "aws_instance" "main" {
  ami                         = data.aws_ami.ubuntu.id                         # ID de l'AMI récupéré précédemment
  instance_type               = var.instance_type                              # Type de l'instance
  associate_public_ip_address = true                                           # Associe une IP publique à l'instance
  subnet_id                   = values(aws_subnet.subnet)[var.subnet_index].id # ID du subnet dans lequel créer l'instance
  iam_instance_profile        = aws_iam_instance_profile.ec2_profile.name      # Profil IAM pour l'instance
  vpc_security_group_ids      = [aws_security_group.inbound.id]                # Groupes de sécurité associés
  user_data                   = file("${path.module}/scripts/user_data.sh")    # Script d'initialisation

  # Configuration des options de métadonnées
  metadata_options {
    http_tokens = "required" # Exige des jetons HTTP pour l'accès aux métadonnées
  }

  # Configuration du disque racine
  root_block_device {
    encrypted   = true            # Active le chiffrement
    volume_size = var.volume_size # Taille du volume
    volume_type = "gp3"           # Type de volume
  }

  # Bloc dynamique pour conditionner les options du marché Spot
  dynamic "instance_market_options" {
    for_each = var.spot_enabled ? [1] : [] # Si spot_enabled est true, le bloc est créé
    content {
      market_type = "spot"
      spot_options {
        max_price                      = var.spot_max_price[var.instance_type]                 # Prix maximum pour les instances Spot
        instance_interruption_behavior = var.spot_request_type == "persistent" ? "stop" : null # Comportement en cas d'interruption
        spot_instance_type             = var.spot_request_type                                 # Type de demande Spot
      }
    }
  }

  # Configuration du cycle de vie pour ignorer les changements d'AMI
  lifecycle {
    ignore_changes = [ami] # Ignore les changements d'AMI pour éviter la recréation
  }

  # Tags associés à l'instance
  tags = {
    Name     = "LibreChat" # Nom de l'instance
    autostop = true        # Tag personnalisé pour l'arrêt automatique
  }
}

# Création d'un rôle IAM pour EC2 Instance Connect
resource "aws_iam_role" "ec2_instance_role" {
  name = "ec2-instance-role" # Nom du rôle

  # Politique d'assomption de rôle
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com" # Service autorisé à assumer ce rôle
        }
      }
    ]
  })
}

# Création d'un profil d'instance IAM pour EC2 Instance Connect
resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2-instance-connect-profile"      # Nom du profil
  role = aws_iam_role.ec2_instance_role.name # Rôle associé au profil
}

# Création d'un groupe de sécurité pour les connexions entrantes
resource "aws_security_group" "inbound" {
  name        = "inbound"        # Nom du groupe de sécurité
  description = "Security group" # Description
  vpc_id      = aws_vpc.main.id  # ID du VPC

  # Règles dynamiques pour les connexions entrantes
  dynamic "ingress" {
    for_each = var.open_ports # Ports ouverts spécifiés dans une variable
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.allowed_ips # Adresses IP autorisées
      description = "Port ${ingress.value} ouvert en entree filtre par SG"
    }
  }

  # Règle pour autoriser toutes les connexions sortantes
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "L instance peut sortir sur tout internet"
  }

  # Tags associés au groupe de sécurité
  tags = {
    Name = "inbound" # Nom du groupe de sécurité
  }
}
