[[_TOC_]]


# Despliegue completamente automatizado de Librechat en EC2 AWS

Este proyecto puede ser desplegado y utilizado de dos maneras:
- Desde la interfaz de gitlab.com a través de una pipeline
- Desde su propio ordenador bajo Linux

En ambos casos, el archivo central utilizado es el mismo, a saber el archivo `export.sh`.
Para la infraestructura, se trata de un despliegue con `Terraform` (Infraestructura Como Código).
En cuanto a la instalación de los componentes en EC2, se realiza a través de `user-data` usando un script `bash`.

La adición de una clave API no es obligatoria para el despliegue, a excepción del uso de Mistral AI, para el cual es necesario proporcionar una clave para que funcione.
En los otros casos, incluso sin clave, esta será solicitada directamente en la interfaz de Librechat desplegada.   
Este proyecto soporta el despliegue automático de las claves API para OpenAI, Mistral AI, Claude y la clave API asociada a la [búsqueda de Google](https://docs.librechat.ai/features/plugins/google_search.html).  

**La instalación por defecto de Librechat** hace accesible el puerto 80 sin activar el puerto 443. Esta configuración no es recomendable para una aplicación que requiere autenticación.  
En el contexto de este proyecto Librechat en EC2, **el puerto 443 está activado por defecto con un certificado auto-firmado**. El puerto 80 redirige al puerto 443. Durante el uso, aparecerá una advertencia HTTPS en el navegador. Esta advertencia es "normal" ya que el certificado no está firmado por una autoridad de certificación reconocida. Sin embargo, **el uso del protocolo HTTPS ofrece seguridad contra el robo de contraseñas en la red**.  

El código Terraform del proyecto despliega puntos finales necesarios para su buen funcionamiento. Si ya dispone de estos puntos finales, puede comentar el código asociado. Para ello, comente el contenido del archivo `endpoints.tf`.   

# Arquitectura  
![alt text](/img/architecture.drawio.png)

# Despliegue desde su propio ordenador

Requisitos previos:
Tener el cliente AWS configurado para poder ejecutar acciones de despliegue vía API (o bien proporcionar la información en variable de entorno).  

Edite el archivo export.sh y elija un nombre de bucket para el estado `Terraform`  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

Si desea añadir clave(s):  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
Luego ejecute el script `export.sh`  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
Aquí la imagen mostrando el resultado obtenido de manera ligeramente acelerada:  
![alt text](/img/check_deploy_status.gif)

# Despliegue a través de gitlab.com

Crear un proyecto git y enviar este repositorio adentro y agregar las variables de CI.

Variables obligatorias:
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

Variables opcionales:
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

Luego vaya a Build->Pipeline y ejecute los trabajos asociados (a través del ícono "play"):

El trabajo de clave(s) si las ha añadido, si no, directamente en verificación Terraform.
![alt text](/img/pipeline.png)


# Documentación del archivo export.sh

Este script Bash es una interfaz de línea de comandos (CLI) para facilitar la gestión y el despliegue de infraestructuras AWS usando Terraform y la gestión de instancias EC2. El script proporciona varias funciones para gestionar los recursos AWS, las claves API y conectar las instancias EC2.

## Configuración inicial y definición de variables globales

- Activa la exportación de todas las variables definidas desde este punto.

![alt text](/img/export.sh.gif)

## Definición de variables para configurar el entorno AWS y Terraform - `BUCKET_STATE` : Nombre del bucket S3 para almacenar el estado de Terraform.
- `AWS_DEFAULT_REGION` : Región AWS utilizada por defecto.
- `USERNAME` : Nombre de usuario para la conexión SSH.
- `PUBLIC_KEY_FILE` : Ubicación de la clave pública SSH para la autenticación.
- `TF_VAR_region` : Define la región AWS como variable de entorno para Terraform.

## Verificación e instalación de awscli

- Verifica si awscli está instalado, si no, lo instala.

## Creación y configuración del bucket S3 para Terraform

- Crea un bucket S3 para almacenar el estado de Terraform, si no está ya hecho, y activa la versioning del bucket.

## Inicialización de Terraform

- Inicializa Terraform con las configuraciones necesarias para usar el bucket S3 como backend.

## Funciones

1. `terraform_plan()` : Planifica los cambios de Terraform actualizando los módulos, validando la configuración y creando un plan de ejecución.
2. `terraform_apply()` : Aplica los cambios de Terraform a partir del plan de ejecución.
3. `terraform_destroy()` : Destruye todos los recursos de Terraform configurados e intenta eliminar el estado de despliegue en AWS SSM Parameter Store.
4. `ssh_connect()` : Se conecta a una instancia EC2 vía SSH después de haber enviado la clave pública.
5. `ssm_connect()` : Se conecta a una instancia EC2 vía AWS SSM.
6. `delete_parameter()` : Elimina un parámetro específico en AWS SSM Parameter Store.
7. `manage_openai_key()` : Gestiona las claves de API de OpenAI en AWS SSM Parameter Store (añadir y eliminar).
8. `manage_mistralai_key()` : Gestiona las claves de Mistral AI en AWS SSM Parameter Store (añadir y eliminar).
9. `manage_anthropic_key()` : Gestiona las claves de Anthropic AI en AWS SSM Parameter Store (añadir y eliminar).
10. `manage_google_api_key()` : Gestiona las claves de Google API en AWS SSM Parameter Store (añadir y eliminar).
11. `manage_google_cse_id()` : Gestiona el Google Search Engine ID en AWS SSM Parameter Store (añadir y eliminar).
12. `check_deployment_status()` : Verifica el estado de despliegue de una instancia consultando un parámetro específico en AWS SSM Parameter Store.
13. `manage_allow_registration()` : Activa o desactiva el registro en LibreChat actualizando un parámetro en AWS SSM Parameter Store.
14. `manage_ssm_parameters()` : Gestiona los parámetros en AWS SSM Parameter Store según la acción especificada (listar, ver, eliminar).
15. `read_spot_request_type()` : Recupera el tipo de solicitud Spot para una instancia EC2.
16. `manage_ec2_instance()` : Gestiona la instancia EC2 según la acción especificada (detener, iniciar, estado).
17. `display_help()` : Muestra los comandos disponibles para el usuario.

## Exportación de funciones

- Exporta las funciones para que estén disponibles para un uso posterior.

## Mostrar ayuda

- Muestra la ayuda al usuario después de haber completado la inicialización.

## Uso de las funciones

### terraform_plan

Esta función permite planificar los cambios de Terraform. Actualiza los módulos de Terraform, valida la configuración de Terraform y crea un plan de ejecución de Terraform.

Para utilizar la función `terraform_plan`, simplemente ejecute:
```bash
terraform_plan
```
### terraform_apply

Esta función permite aplicar los cambios de Terraform a partir del plan de ejecución.

Para utilizar la función `terraform_apply`, simplemente ejecute:
```bash
terraform_apply
```
### terraform_destroy

Esta función permite destruir todos los recursos de Terraform configurados e intenta eliminar el estado de despliegue en AWS SSM Parameter Store.

Para utilizar la función `terraform_destroy`, simplemente ejecute:
```bash
terraform_destroy
```
### ssh_connect

Esta función permite conectarse a una instancia EC2 vía SSH después de haber enviado la clave pública.

Para utilizar la función `ssh_connect`, simplemente ejecute:
```bash
ssh_connect
```
### ssm_connect

Esta función permite conectarse a una instancia EC2 vía AWS SSM.

Para utilizar la función `ssm_connect`, simplemente ejecute:
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

Esta función permite gestionar las claves API de OpenAI en AWS SSM Parameter Store (agregar y eliminar).

Para usar la función `manage_openai_key`, ejecute:
```bash
manage_openai_key put [key]
```
para agregar una clave, y
```bash
manage_openai_key delete
```
para eliminar la clave.

### manage_mistralai_key

Esta función permite gestionar las claves Mistral AI en AWS SSM Parameter Store (agregar y eliminar).

Para usar la función `manage_mistralai_key`, ejecute:
```bash
manage_mistralai_key put [key]
```
para agregar una clave, y
```bash
manage_mistralai_key delete
```
para eliminar la clave.

### manage_anthropic_key

Esta función permite gestionar las claves Anthropic AI en AWS SSM Parameter Store (agregar y eliminar).

Para usar la función `manage_anthropic_key`, ejecute:
```bash
manage_anthropic_key put [key]
```
para agregar una clave, y
```bash
manage_anthropic_key delete
```
para eliminar la clave.

### manage_google_api_key

Esta función permite gestionar las claves Google API en AWS SSM Parameter Store (agregar y eliminar).

Para usar la función `manage_google_api_key`, ejecute:
```bash
manage_google_api_key put [key]
```
para agregar una clave, y
```bash
manage_google_api_key delete
```
para eliminar la clave.

### manage_google_cse_id

Esta función permite gestionar el Google Search Engine ID en AWS SSM Parameter Store (agregar y eliminar).

Para usar la función `manage_google_cse_id`, ejecute:
```bash
manage_google_cse_id put [key]
```
para agregar una clave, y
```bash
manage_google_cse_id delete
```
para eliminar la clave.

### check_deployment_status

Esta función permite verificar el estado de implementación de una instancia consultando un parámetro específico en AWS SSM Parameter Store.

Para usar la función `check_deployment_status`, simplemente ejecute:
```bash
check_deployment_status
```
### manage_allow_registration

Esta función permite activar o desactivar el registro en LibreChat actualizando un parámetro en AWS SSM Parameter Store.

Para usar la función `manage_allow_registration`, ejecute:
```bash
manage_allow_registration on
```
para activar el registro, y
```bash
manage_allow_registration off
```
para desactivar el registro.

### manage_ssm_parameters

Esta función permite gestionar los parámetros en AWS SSM Parameter Store según la acción especificada (listar, ver, borrar).

Para usar la función `manage_ssm_parameters`, ejecute:
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
para listar los parámetros,
```bash
manage_ssm_parameters view
```
para mostrar los valores de los parámetros, y
```bash
manage_ssm_parameters delete
```
para eliminar todos los parámetros.

### manage_ec2_instance

Esta función permite gestionar la instancia EC2 según la acción especificada (parar, iniciar, estado).

Para usar la función `manage_ec2_instance`, ejecute:
```bash
manage_ec2_instance stop
```
para detener la instancia,
```bash
manage_ec2_instance start
```
para iniciar la instancia, y
```bash
manage_ec2_instance status
```

Para mostrar el estado de la instancia:  

![alt text](/img/status.png)

### display_help

Esta función muestra los comandos disponibles para el usuario.

Para usar la función `display_help`, simplemente ejecute:
```bash
display_help
```

## Observaciones y buenas prácticas

1. Asegúrese de haber configurado correctamente sus credenciales de AWS antes de usar este script. Puede configurar sus credenciales usando el comando `aws configure` o estableciendo las variables de entorno `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, y `AWS_SESSION_TOKEN` (si es necesario).
2. Antes de usar las funciones `terraform_plan`, `terraform_apply`, y `terraform_destroy`, asegúrese de haber configurado correctamente su archivo de configuración de Terraform (por ejemplo, `main.tf`). Este archivo debe contener las definiciones de los recursos de AWS que desea crear, actualizar o eliminar.
3. Las funciones `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, y `manage_google_cse_id` almacenan las claves API en AWS SSM Parameter Store. Asegúrese de gestionar estas claves con cuidado y seguir las mejores prácticas de seguridad para evitar el acceso no autorizado.
4. La función `manage_ec2_instance` gestiona la instancia EC2 en función del tipo de solicitud Spot. Asegúrese de comprender las implicaciones de los diferentes tipos de solicitudes Spot antes de usar esta función. Puede obtener más información sobre las instancias Spot en la [documentación de AWS](https://aws.amazon.com/ec2/spot/).
5. Se recomienda verificar siempre el estado de implementación de una instancia utilizando la función `check_deployment_status` después de aplicar cambios con Terraform. Esto le permitirá asegurarse de que la instancia esté configurada correctamente y lista para usar.
6. Cuando use la función `ssh_connect` para conectarse a una instancia EC2, asegúrese de que la clave pública SSH correspondiente haya sido agregada correctamente a la instancia. Puede agregar la clave pública SSH a la instancia utilizando la consola de AWS, la interfaz de línea de comandos de AWS, o especificándola en su archivo de configuración de Terraform.
7. Este script está diseñado para usarse con AWS y Terraform. Asegúrese de haber instalado y configurado correctamente estas herramientas antes de usar el script. Puede encontrar instrucciones de instalación y configuración para AWS CLI en la [documentación de AWS](https://aws.amazon.com/cli/) y para Terraform en la [documentación de Terraform](https://www.terraform.io/).
8. Se recomienda hacer siempre copias de seguridad de sus datos y configuraciones antes de realizar cambios en su infraestructura de AWS utilizando este script. Esto le permitirá recuperar sus datos y configuraciones fácilmente en caso de un problema.
9. No dude en personalizar y extender este script según sus necesidades específicas. Puede agregar nuevas funciones, modificar las funciones existentes, o adaptar el script para trabajar con otros servicios de AWS u otras herramientas de infraestructura como código.
10. Si desea conectarse al servidor a través de SSM, debe desplegar el plugin session-manager. Ejemplo para Ubuntu: 
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

Siguiendo estas recomendaciones y buenas prácticas, podrá aprovechar al máximo este script para gestionar eficientemente su infraestructura de AWS y sus instancias EC2.  

## Solución de problemas y resolución de incidencias

1. **Errores de autenticación de AWS**: Si encuentra errores de autenticación al usar este script, verifique que sus credenciales de AWS estén configuradas correctamente. Puede hacerlo usando el comando `aws configure` o configurando las variables de entorno `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, y `AWS_SESSION_TOKEN` (si es necesario). Asegúrese también de que tenga los permisos apropiados para acceder a los recursos de AWS especificados en su archivo de configuración de Terraform.
2. **Errores de configuración de Terraform**: Si encuentra errores al usar las funciones `terraform_plan`, `terraform_apply`, o `terraform_destroy`, verifique que su archivo de configuración de Terraform esté configurado correctamente. Asegúrese de que los recursos de AWS estén definidos correctamente y que las dependencias entre recursos se manejen adecuadamente. Puede usar el comando `terraform validate` para verificar la validez de su archivo de configuración de Terraform.
3. **Errores de conexión SSH**: Si encuentra errores al usar la función `ssh_connect`, verifique que la clave pública SSH correspondiente haya sido agregada correctamente a la instancia EC2. Asegúrese también de que el nombre de usuario y la dirección IP de la instancia sean correctos. Puede verificar esta información utilizando la consola de AWS o la interfaz de línea de comandos de AWS.
4. **Errores de conexión de AWS SSM**: Si encuentra errores al usar la función `ssm_connect`, verifique que su instancia EC2 esté configurada correctamente para usar AWS SSM. Asegúrese de que la instancia tenga un rol IAM apropiado y que los parámetros de seguridad del grupo de seguridad permitan el tráfico entrante desde las direcciones IP de SSM. Puede aprender más sobre la configuración de AWS SSM en la [documentación de AWS](https://aws.amazon.com/ssm/).
5. **Errores de gestión de claves de API**: Si encuentra errores al utilizar las funciones `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, o `manage_google_cse_id`, verifique que tiene los permisos apropiados para acceder a AWS SSM Parameter Store. Asegúrese también de que las claves de API estén correctamente formateadas y que proporcione los argumentos correctos a estas funciones.
6. **Errores de gestión de instancias EC2**: Si encuentra errores al utilizar la función `manage_ec2_instance`, verifique que tiene los permisos apropiados para gestionar las instancias EC2 en su cuenta de AWS. Asegúrese también de que el ID de la instancia sea correcto y que proporcione los argumentos correctos a esta función.  

En caso de problema con este script, no dude en consultar la documentación de AWS y Terraform, así como los foros y recursos en línea para obtener ayuda. También puede contactar al soporte de AWS si necesita asistencia adicional.

Siguiendo estos consejos de solución de problemas y resolución de problemas, debería poder resolver la mayoría de los problemas que podría encontrar al utilizar este script para gestionar su infraestructura AWS y sus instancias EC2.

# Documentación del archivo gitlab-ci.yml

Este archivo de GitLab CI/CD (gitlab-ci.yml) describe un pipeline de despliegue para gestionar la infraestructura de AWS y las instancias EC2 utilizando Terraform, usando las funciones definidas en el script Bash documentado anteriormente.

## Variables

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID`: Claves de API a configurar en la interfaz de GitLab CI/CD.
- `TERRAFORM_VERSION`: Versión de la imagen Docker de Terraform utilizada para ejecutar las tareas.
- `TF_VAR_region`: Región AWS predeterminada.

## Stages

- `Requisitos opcionales`: Etapa para añadir o eliminar claves de API.
- `Verificaciones`: Etapa para verificar las modificaciones de Terraform.
- `Despliegues`: Etapa para aplicar las modificaciones de Terraform y verificar el estado del despliegue.
- `Management`: Etapa para gestionar las instancias EC2 y los parámetros de la aplicación.
- `Eliminaciones`: Etapa para eliminar los recursos de Terraform y las claves de API.

## .terraform\_template

Es un modelo para las tareas de Terraform, definiendo la imagen Docker, la entrada y los comandos antes del script.

## Tareas

1. **Verificación de Terraform**: Esta tarea verifica las modificaciones de Terraform ejecutando `terraform_plan` y guarda el plan en un artefacto.
2. **Despliegue de Terraform**: Esta tarea aplica las modificaciones de Terraform ejecutando `terraform_apply` y verifica el estado del despliegue usando `check_deployment_status`.
3. **Eliminación de Terraform**: Esta tarea elimina los recursos de Terraform ejecutando `terraform_destroy`.
4. **Librechat Abrir inscripciones**: Esta tarea activa las inscripciones en la aplicación Librechat usando `manage_allow_registration on`.
5. **Librechat Cerrar inscripciones**: Esta tarea desactiva las inscripciones en la aplicación Librechat usando `manage_allow_registration off`.
6. **EC2 Iniciar**: Esta tarea inicia la instancia EC2 usando `manage_ec2_instance start`.
7. **EC2 Detener**: Esta tarea detiene la instancia EC2 usando `manage_ec2_instance stop`.
8. **EC2 Estado**: Esta tarea muestra el estado de la instancia EC2 usando `manage_ec2_instance status`.
9. **Clave(s) - Añadir**: Esta tarea añade las claves de API usando las funciones `manage_*_key put` correspondientes. **Clave(s) - Supresión**: Esta tarea suprime las claves de API utilizando las funciones `manage_*_key delete` correspondientes.

## Tiempo de despliegue completo infraestructura + configuración OS EC2 menos de 6min:
![alt text](/img/pipeline_apply_time.png)

## Comentarios y buenas prácticas

1. Asegúrate de configurar correctamente las variables `OPENAI_KEY`, `MISTRALAI_KEY`, `GOOGLE_API_KEY`, y `GOOGLE_CSE_ID` en la interfaz de GitLab CI/CD antes de ejecutar el pipeline.
2. Asegúrate de utilizar una versión apropiada de la imagen Docker de Terraform para tu infraestructura.
3. No olvides actualizar el script Bash correspondiente cuando realices modificaciones a este archivo GitLab CI/CD.
4. Sigue las mejores prácticas de seguridad para gestionar las claves de API y las credenciales de AWS.

Siguiendo estos comentarios y buenas prácticas, podrás utilizar este archivo GitLab CI/CD para gestionar eficazmente tu infraestructura AWS y tus instancias EC2 usando Terraform y las funciones definidas en el script Bash.

## Solución de problemas y resolución de problemas

1. **Errores de autenticación de AWS**: Si encuentras errores de autenticación al ejecutar el pipeline GitLab CI/CD, verifica que tus credenciales de AWS estén correctamente configuradas. Asegúrate de que las variables de entorno `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, y `AWS_SESSION_TOKEN` (si es necesario) estén definidas y sean válidas. Puedes configurarlas en la interfaz de GitLab CI/CD o en el script Bash.
2. **Errores de configuración de Terraform**: Si encuentras errores al ejecutar las tareas de Terraform (`terraform_plan`, `terraform_apply`, o `terraform_destroy`), verifica que tu archivo de configuración de Terraform esté correctamente configurado. Asegúrate de que los recursos de AWS estén correctamente definidos y que las dependencias entre los recursos se manejen adecuadamente.
3. **Errores en la gestión de claves de API**: Si encuentras errores al añadir o eliminar las claves de API, verifica que tengas los permisos adecuados para acceder al AWS SSM Parameter Store. Asegúrate también de que las claves de API estén correctamente formateadas y que proporciones los argumentos correctos a las funciones `manage_*_key`.
4. **Errores en la gestión de instancias EC2**: Si encuentras errores al usar las funciones `manage_ec2_instance`, verifica que tengas los permisos adecuados para gestionar las instancias EC2 en tu cuenta de AWS. Asegúrate también de que el ID de la instancia sea correcto y que proporciones los argumentos correctos a estas funciones.

En caso de problemas con este archivo GitLab CI/CD, no dudes en consultar la documentación de AWS, Terraform y GitLab CI/CD, así como los foros y recursos en línea para obtener ayuda. También puedes contactar con el soporte de AWS o GitLab si necesitas asistencia adicional.

Siguiendo estos consejos de solución de problemas y resolución, deberías poder resolver la mayoría de los problemas que puedas encontrar al utilizar este archivo GitLab CI/CD para gestionar tu infraestructura AWS y tus instancias EC2 usando Terraform y las funciones definidas en el script Bash.

# Script de datos de usuario LibreChat EC2

Este script está diseñado para ejecutarse en una instancia de Amazon Elastic Compute Cloud (EC2) y automatizar el proceso de configuración de la aplicación LibreChat. Realiza diversas tareas como la actualización del sistema operativo, la instalación de los paquetes necesarios, la configuración de Docker y el despliegue de la aplicación LibreChat.

## Resumen del script

1. Actualización del sistema operativo e instalación de los paquetes necesarios.
2. Configuración de Docker y Docker Compose.
3. Clonación y configuración de la aplicación LibreChat.
4. Configuración de certificados SSL para una comunicación segura.
5. Lanzamiento de la aplicación LibreChat con Docker Compose.

## Detalles del script ### Actualización del sistema operativo e instalación de los paquetes requeridos

El script comienza actualizando el sistema operativo e instalando los paquetes requeridos como `ca-certificates`, `curl` y `awscli`. Esto garantiza que el sistema cuente con las dependencias necesarias para ejecutar la aplicación LibreChat.

### Configuración de Docker y Docker Compose

El script instala luego Docker y Docker Compose, que son utilizados para desplegar la aplicación LibreChat en un entorno contenerizado. También agrega el usuario actual al grupo Docker, permitiéndole ejecutar comandos de Docker sin necesitar privilegios de root.

### Clonación y configuración de la aplicación LibreChat

El script clona el repositorio de LibreChat desde GitHub y configura los archivos de configuración requeridos. También recupera las claves de API y los tokens necesarios desde AWS Systems Manager (SSM) Parameter Store y actualiza los archivos de configuración en consecuencia.

### Configuración de certificados SSL para una comunicación segura

Para asegurar la comunicación entre el cliente y el servidor, el script genera un certificado SSL autofirmado y modifica la configuración de Nginx para habilitar SSL. También descarga los parámetros de Diffie-Hellman para reforzar el cifrado SSL.

### Lanzamiento de la aplicación LibreChat usando Docker Compose

Finalmente, el script lanza la aplicación LibreChat usando Docker Compose. Esto inicia la aplicación contenerizada y la hace accesible a los usuarios.

## Gestión de errores y registro

El script incluye mecanismos de gestión de errores y registro para ayudar a resolver problemas que puedan surgir durante el proceso de configuración. Redirige las salidas estándar y de error a un archivo de registro ubicado en `/var/log/user-data.log`. Además, el script utiliza una función trap para capturar errores que ocurran durante la ejecución y actualizar el estado de despliegue en AWS SSM Parameter Store en consecuencia.

## Verificación del estado de despliegue

La función `check_deployment_status`, que no está incluida en este script pero es mencionada en la información proporcionada, se utiliza para verificar el estado de despliegue de una instancia EC2 recuperando un parámetro específico desde AWS SSM Parameter Store. Esta función es llamada en el script `export.sh` y proporciona actualizaciones sobre la progresión del despliegue.

## Integración Terraform

Este script de datos de usuario es llamado en el archivo de configuración Terraform `ec2.tf`, que crea una instancia EC2 y aplica los parámetros especificados. El script de datos de usuario es transmitido a la instancia utilizando el argumento `user_data`, asegurando así que el script se ejecute cuando la instancia es lanzada.

## Información sobre el archivo

Nombre: `user_data.sh`  
Ubicación: `${path.module}/scripts/user_data.sh`  
Uso: Se ejecuta automáticamente cuando la instancia EC2 es lanzada, como se especifica en el archivo de configuración Terraform `ec2.tf`.

# Script check_spot.py

Este script de Python permite verificar y actualizar los precios spot de las instancias EC2 en el archivo de variables de Terraform (`variables.tf`). El uso de este script facilita la gestión de costos y asegura que los precios spot utilizados para las instancias estén actualizados.

Para usar este script, primero debes asegurarte de haber instalado Python 3 y Boto3, el SDK de AWS para Python. Luego, puedes ejecutar el script con la opción `--update` para actualizar los precios spot en el archivo `variables.tf`. El script soporta diferentes tipos de instancias y recupera automáticamente la zona de disponibilidad (AZ) de la instancia especificada.

El script comienza configurando el analizador de argumentos y definiendo los parámetros AWS, tales como la región, los tipos de instancias y la ruta del archivo `variables.tf`. Luego, inicializa un cliente EC2 para interactuar con el servicio AWS EC2. La función `read_max_spot_prices()` lee los precios spot máximos actuales desde el archivo `variables.tf` y devuelve un diccionario que contiene los tipos de instancias como claves y los precios máximos como valores.

La función `get_instance_az()` obtiene la zona de disponibilidad (AZ) de la instancia especificada por la etiqueta Name usando el cliente EC2. Si no se encuentra ninguna instancia, devuelve una AZ por defecto.

La función `check_and_update_spot_prices()` verifica y actualiza los precios spot máximos en el archivo `variables.tf` si es necesario. Llama a `read_max_spot_prices()` para obtener los precios actuales, luego recupera el historial de precios spot para cada tipo de instancia y la AZ especificada. Si el precio corriente es superior al precio configurado, la función actualiza el precio configurado.

La función `update_variables_file()` actualiza el archivo `variables.tf` con los nuevos precios spot. Toma como entrada un diccionario que contiene los tipos de instancias como claves y los nuevos precios como valores.

Finalmente, el script principal llama a `check_and_update_spot_prices()` para verificar y actualizar los precios spot. Si el argumento `--update` se especifica durante la ejecución del script y hay nuevos precios, la función `update_variables_file()` es llamada para actualizar el archivo `variables.tf`.

En resumen, este script de Python simplifica la gestión de los precios spot para las instancias EC2 automatizando el proceso de verificación y actualización de los precios en un archivo de variables de Terraform.

# Documentación Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requisitos

| Nombre | Versión |
|--------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Proveedores

| Nombre | Versión |
|--------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Módulos

No módulos.

## Recursos | Nombre | Tipo |
|--------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | recurso |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | recurso |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | recurso |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | recurso |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | recurso |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | recurso |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | recurso |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | recurso |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | recurso |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | recurso |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | recurso |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | recurso |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | recurso |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | recurso |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | recurso |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | fuente de datos |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | fuente de datos |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | fuente de datos |

## Entradas | Nombre | Descripción | Tipo | Predeterminado | Requerido |
|--------|-------------|------|---------------|:---------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | Permite activar o desactivar el registro | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | Lista de direcciones IP permitidas para acceder al punto de terminación EC2 Instance Connect | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | Nombre del filtro para el AMI AWS | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | Hora para detener automáticamente la instancia EC2 | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | Zonas de disponibilidad para las subredes | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Tipo de instancia para la instancia EC2 | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | Lista de puertos a abrir | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | Definición de la región AWS donde se desplegarán los recursos. | `string` | n/a | sí |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | Precio máximo para cada tipo de instancia | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | Tipo de solicitud Spot (persistente o única) | `string` | `"persistente"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | Bloques CIDR para las subredes | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | Índice de la subred a usar [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | Tamaño del volumen EBS en GB | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | Bloque CIDR para el VPC | `string` | `"10.0.0.0/24"` | no |

## Salidas

| Nombre | Descripción |
|--------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | El ID de la instancia EC2 |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | La dirección IP pública de la instancia EC2 |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |
<!-- END_TF_DOCS -->

# Autor

Julien LE SAUX  
Email : contact@jls42.org

# Clausula de exención de responsabilidad

AL UTILIZAR ESTE SCRIPT, USTED RECONOCE QUE LO HACE BAJO SU PROPIO RIESGO. EL AUTOR NO ES RESPONSABLE DE LOS GASTOS INCURRIDOS, LOS DAÑOS O LAS PÉRDIDAS RESULTANTES DEL USO DE ESTE SCRIPT. ASEGÚRESE DE COMPRENDER LOS COSTOS ASOCIADOS CON AWS ANTES DE UTILIZAR ESTE SCRIPT.

# Licencia

Este proyecto está bajo la licencia GPL.

**Este documento ha sido traducido de la versión fr al idioma es utilizando el modelo gpt-4o. Para más información sobre el proceso de traducción, consulte https://gitlab.com/jls42/ai-powered-markdown-translator**

