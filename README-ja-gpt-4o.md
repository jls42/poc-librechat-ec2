[[_TOC_]]


# EC2 AWSでのLibrechatの完全自動化展開

このプロジェクトは、次の2つの方法で展開および使用できます。
- gitlab.comのインターフェースからパイプラインを通じて
- 自分のLinuxマシンから

どちらの場合でも、使用する中央のファイルは同じで、`export.sh`ファイルです。
インフラストラクチャについては、`Terraform` (Infrastructure As Code) を使用して展開します。
EC2内のコンポーネントのインストールは、`bash`スクリプトを使用して`user-data`経由で実行されます。

APIキーの追加は、デプロイメントには必須ではありませんが、Mistral AIの使用には機能させるためにキーを提供する必要があります。
その他の場合、キーなしでも、展開されたLibrechatインターフェースで直接入力を求められます。   
このプロジェクトは、OpenAI、Mistral AI、Claude、そして[Google検索](https://docs.librechat.ai/features/plugins/google_search.html)に関連するAPIキーの自動展開をサポートしています。

**Librechatのデフォルトインストール**では、ポート80が有効であり、ポート443はアクティブになっていません。この構成は、認証を必要とするアプリケーションには推奨されません。  
このプロジェクトのEC2上のLibrechatでは、**ポート443がデフォルトで自己署名証明書付きで有効化**されています。ポート80はポート443にリダイレクトされます。使用時にブラウザにHTTPS警告が表示されます。この警告は、証明書が証明機関によって署名されていないため「正常」です。ただし、**HTTPSプロトコルの使用により、ネットワーク上のパスワード盗難からのセキュリティが提供されます**。

プロジェクトのTerraformコードは、正常に機能するために必要なエンドポイントを展開します。これらのエンドポイントをすでに持っている場合は、関連するコードをコメントアウトできます。そのためには、`endpoints.tf`ファイルの内容をコメントアウトします。

# アーキテクチャ  
![alt text](/img/architecture.drawio.png)

# 自分のマシンからの展開

前提条件：
API経由でデプロイメントアクションを実行するために、AWSクライアントが設定されていること（または環境変数で情報を提供すること）。

export.shファイルを編集し、`Terraform`状態のバケット名を選択します。

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

もしキーを追加したい場合：  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
次に`export.sh`スクリプトを実行します  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
得られた結果を少し早送りにした画像はこちら：  
![alt text](/img/check_deploy_status.gif)

# gitlab.com経由の展開

Gitプロジェクトを作成し、このリポジトリを中に送信し、CI変数を追加します。

必須変数：
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

オプション変数：
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

その後、Build->Pipelineに進み、関連するジョブを（「再生」アイコンで）開始：

キーを設定した場合はそのジョブを、そうでない場合はTerraformの検証を直接開始します。
![alt text](/img/pipeline.png)


# `export.sh`ファイルのドキュメント

このBashスクリプトは、Terraformを使用してAWSインフラストラクチャの管理と展開を簡素化し、EC2インスタンスを管理するためのコマンドラインインターフェイス（CLI）です。スクリプトは、AWSリソース、APIキーの管理、およびEC2インスタンスへの接続を管理するための複数の機能を提供します。

## 初期設定とグローバル変数の定義

- ここから定義されたすべての変数のエクスポートを有効にします。

![alt text](/img/export.sh.gif)

## AWSおよびTerraform環境を設定するための変数の定義 - `BUCKET_STATE` : Terraformの状態を保存するためのS3バケット名。
- `AWS_DEFAULT_REGION` : デフォルトで使用するAWSリージョン。
- `USERNAME` : SSH接続用のユーザー名。
- `PUBLIC_KEY_FILE` : 認証用のSSH公開鍵の場所。
- `TF_VAR_region` : Terraformの環境変数としてAWSリージョンを設定。

## awscliの確認とインストール

- awscliがインストールされているか確認し、インストールされていない場合はインストールします。

## Terraform用S3バケットの作成と設定

- Terraformの状態を保存するためのS3バケットを作成し（まだ作成されていない場合）、バージョニングを有効にします。

## Terraformの初期化

- S3バケットをバックエンドとして使用するための必要な設定でTerraformを初期化します。

## 機能

1. `terraform_plan()` : Terraformのモジュールを更新し、設定を確認し、実行計画を作成して変更を計画します。
2. `terraform_apply()` : 実行計画からTerraformの変更を適用します。
3. `terraform_destroy()` : 設定済みのすべてのTerraformリソースを破棄し、AWS SSMパラメータストアでのデプロイメントステータスの削除を試みます。
4. `ssh_connect()` : 公開鍵送信後にSSHでEC2インスタンスに接続します。
5. `ssm_connect()` : AWS SSMを使用してEC2インスタンスに接続します。
6. `delete_parameter()` : AWS SSMパラメータストア内の特定のパラメータを削除します。
7. `manage_openai_key()` : AWS SSMパラメータストアでOpenAI APIキーを管理（追加と削除）します。
8. `manage_mistralai_key()` : AWS SSMパラメータストアでMistral AIキーを管理（追加と削除）します。
9. `manage_anthropic_key()` : AWS SSMパラメータストアでAnthropic AIキーを管理（追加と削除）します。
10. `manage_google_api_key()` : AWS SSMパラメータストアでGoogle APIキーを管理（追加と削除）します。
11. `manage_google_cse_id()` : AWS SSMパラメータストアでGoogle Search Engine IDを管理（追加と削除）します。
12. `check_deployment_status()` : AWS SSMパラメータストア内の特定のパラメータを確認してインスタンスのデプロイメント状態をチェックします。
13. `manage_allow_registration()` : AWS SSMパラメータストアのパラメータを更新することでLibreChatの登録を有効または無効にします。
14. `manage_ssm_parameters()` : 指定されたアクション（リスト、表示、削除）に従ってAWS SSMパラメータストア内のパラメータを管理します。
15. `read_spot_request_type()` : EC2インスタンスのスポットリクエストタイプを取得します。
16. `manage_ec2_instance()` : 指定されたアクション（停止、開始、ステータス）に基づいてEC2インスタンスを管理します。
17. `display_help()` : ユーザーに利用可能なコマンドを表示します。

## 機能のエクスポート

- 将来的な使用のために機能をエクスポートします。

## ヘルプの表示

- 初期化が終了した後、ユーザーにヘルプを表示します。

## 機能の使用法

### terraform_plan

この機能により、Terraformの変更を計画できます。Terraformのモジュールを更新し、Terraformの設定を検証し、Terraformの実行計画を作成します。

`terraform_plan`機能を使用するには、単に実行します：
```bash
terraform_plan
```
### terraform_apply

この機能により、実行計画からTerraformの変更を適用できます。

`terraform_apply`機能を使用するには、単に実行します：
```bash
terraform_apply
```
### terraform_destroy

この機能により、設定済みのすべてのTerraformリソースを破棄し、AWS SSMパラメータストアでのデプロイメントステータスの削除を試みます。

`terraform_destroy`機能を使用するには、単に実行します：
```bash
terraform_destroy
```
### ssh_connect

この機能により、公開鍵送信後にSSHでEC2インスタンスに接続できます。

`ssh_connect`機能を使用するには、単に実行します：
```bash
ssh_connect
```
### ssm_connect

この機能により、AWS SSMを使用してEC2インスタンスに接続できます。

`ssm_connect`機能を使用するには、単に実行します：
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

この機能は、AWS SSM パラメーターストアで OpenAI API キーを管理することを可能にします（追加と削除）。

`manage_openai_key` 関数を使用するには、次を実行します：
```bash
manage_openai_key put [key]
```
キーを追加するには、そして
```bash
manage_openai_key delete
```
キーを削除するには。

### manage_mistralai_key

この機能は、AWS SSM パラメーターストアで Mistral AI キーを管理することを可能にします（追加と削除）。

`manage_mistralai_key` 関数を使用するには、次を実行します：
```bash
manage_mistralai_key put [key]
```
キーを追加するには、そして
```bash
manage_mistralai_key delete
```
キーを削除するには。

### manage_anthropic_key

この機能は、AWS SSM パラメーターストアで Anthropic AI キーを管理することを可能にします（追加と削除）。

`manage_anthropic_key` 関数を使用するには、次を実行します：
```bash
manage_anthropic_key put [key]
```
キーを追加するには、そして
```bash
manage_anthropic_key delete
```
キーを削除するには。

### manage_google_api_key

この機能は、AWS SSM パラメーターストアで Google API キーを管理することを可能にします（追加と削除）。

`manage_google_api_key` 関数を使用するには、次を実行します：
```bash
manage_google_api_key put [key]
```
キーを追加するには、そして
```bash
manage_google_api_key delete
```
キーを削除するには。

### manage_google_cse_id

この機能は、AWS SSM パラメーターストアで Google Search Engine ID を管理することを可能にします（追加と削除）。

`manage_google_cse_id` 関数を使用するには、次を実行します：
```bash
manage_google_cse_id put [key]
```
キーを追加するには、そして
```bash
manage_google_cse_id delete
```
キーを削除するには。

### check_deployment_status

この機能は、AWS SSM パラメーターストア内の特定のパラメーターを確認して、インスタンスの展開ステータスを確認することができます。

`check_deployment_status` 関数を使用するには、単に次を実行します：
```bash
check_deployment_status
```
### manage_allow_registration

この機能は、AWS SSM パラメーターストア内のパラメーターを更新することにより、LibreChatへの登録を有効または無効にすることを可能にします。

`manage_allow_registration` 関数を使用するには、次を実行します：
```bash
manage_allow_registration on
```
登録を有効にするには、そして
```bash
manage_allow_registration off
```
登録を無効にするには。

### manage_ssm_parameters

この機能は、指定されたアクション（リスト、ビュー、削除）に従って AWS SSM パラメーターストアのパラメーターを管理することを可能にします。

`manage_ssm_parameters` 関数を使用するには、次を実行します：
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
パラメーターをリストするには、
```bash
manage_ssm_parameters view
```
パラメーターの値を表示するには、そして
```bash
manage_ssm_parameters delete
```
すべてのパラメーターを削除するには。

### manage_ec2_instance

この機能は、指定されたアクション（停止、開始、ステータス）に基づいて EC2 インスタンスを管理することを可能にします。

`manage_ec2_instance` 関数を使用するには、次を実行します：
```bash
manage_ec2_instance stop
```
インスタンスを停止するには、
```bash
manage_ec2_instance start
```
インスタンスを開始するには、そして
```bash
manage_ec2_instance status
```

インスタンスのステータスを表示するには：

![alt text](/img/status.png)

### display_help

この機能は、ユーザーに利用可能なコマンドを表示します。

`display_help` 関数を使用するには、単に次を実行します：
```bash
display_help
```

## 注意事項とベストプラクティス

1. このスクリプトを使用する前に、AWS 資格情報を正しく設定していることを確認してください。`aws configure` コマンドを使用するか、環境変数 `AWS_ACCESS_KEY_ID`、`AWS_SECRET_ACCESS_KEY`、および `AWS_SESSION_TOKEN`（必要な場合）を定義して、資格情報を設定できます。
2. `terraform_plan`、`terraform_apply`、および `terraform_destroy` 関数を使用する前に、Terraform 構成ファイル（例えば `main.tf`）を正しく設定していることを確認してください。このファイルには、作成、更新、または削除したい AWS リソースの定義が含まれている必要があります。
3. `manage_openai_key`、`manage_mistralai_key`、`manage_anthropic_key`、`manage_google_api_key`、および `manage_google_cse_id` 関数は、API キーを AWS SSM パラメーターストアに保存します。これらのキーを注意深く管理し、不正なアクセスを防ぐために安全性のベストプラクティスに従うことを確信してください。
4. 関数`manage_ec2_instance`は、スポットリクエストのタイプに基づいてEC2インスタンスを管理します。この関数を使用する前に、さまざまなスポットリクエストのタイプの影響を理解してください。スポットインスタンスについての詳細は[AWSドキュメント](https://aws.amazon.com/ec2/spot/)で学ぶことができます。 5. Terraformの変更を適用した後に、常に関数`check_deployment_status`を使用してインスタンスのデプロイメント状態を確認することをお勧めします。これにより、インスタンスが適切に構成され、使用可能であることを確認できます。 6. `ssh_connect`関数を使用してEC2インスタンスに接続する場合、対応するSSH公開鍵がインスタンスに正しく追加されていることを確認してください。SSH公開鍵をインスタンスに追加するには、AWSコンソール、AWS CLI、またはTerraformの設定ファイルで指定して行うことができます。 7. このスクリプトはAWSとTerraformを使用するために設計されています。このスクリプトを使用する前に、これらのツールが正しくインストールおよび構成されていることを確認してください。AWS CLIのインストールと構成の手順については[AWSドキュメント](https://aws.amazon.com/cli/)を、Terraformについては[Terraformドキュメント](https://www.terraform.io/)を参照してください。 8. このスクリプトを使用してAWSインフラストラクチャに変更を加える前に、常にデータと設定をバックアップすることをお勧めします。これにより、問題が発生した場合にデータと設定を簡単に回復できます。 9. このスクリプトを特定のニーズに応じてカスタマイズおよび拡張することをためらわないでください。新しい関数を追加したり、既存の関数を変更したり、他のAWSサービスやコードインフラストラクチャツールと連携するようにスクリプトを適応させることができます。 10. SSM経由でサーバーに接続したい場合、セッションマネージャープラグインをデプロイする必要があります。Ubuntuの例: ```bash curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

これらの備考とベストプラクティスに従うことで、AWSインフラストラクチャとEC2インスタンスを効率的に管理するためにこのスクリプトを最大限に活用できます。

## トラブルシューティングと問題解決

1. **AWS認証エラー**: このスクリプトを使用しているときに認証エラーが発生した場合、AWS資格情報が正しく構成されているか確認してください。`aws configure`コマンドを使用するか、`AWS_ACCESS_KEY_ID`、`AWS_SECRET_ACCESS_KEY`、および必要に応じて`AWS_SESSION_TOKEN`環境変数を設定できます。Terraform設定ファイルで指定されたAWSリソースにアクセスするために必要な適切な権限もあることを確認してください。 2. **Terraform構成エラー**: `terraform_plan`、`terraform_apply`、または`terraform_destroy`関数の使用中にエラーが発生する場合、Terraform設定ファイルが正しく構成されていることを確認してください。AWSリソースが正しく定義され、リソース間の依存関係が適切に管理されていることを確認してください。Terraform設定ファイルの有効性を確認するには`terraform validate`コマンドを使用できます。 3. **SSH接続エラー**: `ssh_connect`関数を使用中にエラーが発生する場合、対応するSSH公開鍵がEC2インスタンスに正しく追加されていることを確認してください。また、インスタンスのユーザー名とIPアドレスが正しいことも確認してください。これらの情報はAWSコンソールまたはAWS CLIを使用して確認できます。 4. **AWS SSM接続エラー**: `ssm_connect`関数を使用中にエラーが発生する場合、EC2インスタンスがAWS SSMを使用するように正しく構成されていることを確認してください。 インスタンスに適切なIAMロールが設定され、セキュリティグループの設定でSSMのIPアドレスからの着信トラフィックが許可されていることを確認してください。AWS SSMの設定について詳しくは[AWSのドキュメント](https://aws.amazon.com/ssm/)をご覧ください。
5. **APIキー管理エラー**: `manage_openai_key`、`manage_mistralai_key`、`manage_anthropic_key`、`manage_google_api_key`、または`manage_google_cse_id`関数を使用中にエラーが発生する場合、AWS SSMパラメーターストアにアクセスする適切な権限を持っていることを確認してください。また、APIキーが正しくフォーマットされており、これらの関数に正しい引数を提供していることを確認してください。
6. **EC2インスタンス管理エラー**: `manage_ec2_instance`関数を使用中にエラーが発生する場合、AWSアカウントのEC2インスタンスを管理する適切な権限を持っていることを確認してください。また、インスタンスIDが正しく、関数に提供する引数が正しいことを確認してください。

このスクリプトで問題が発生した場合は、AWSやTerraformのドキュメント、フォーラム、およびオンラインリソースを参照してサポートを得ることをお勧めします。さらにサポートが必要な場合は、AWSサポートにお問い合わせください。

これらのトラブルシューティングと問題解決のアドバイスに従うことで、このスクリプトを使用してAWSインフラストラクチャとEC2インスタンスを管理する際に発生しうるほとんどの問題を解決できるはずです。

# gitlab-ci.ymlファイルのドキュメント

このGitLab CI/CDファイル(gitlab-ci.yml)は、Terraformを使用してAWSインフラストラクチャとEC2インスタンス管理を行うためのデプロイパイプラインを記述しています。以前に説明されたBashスクリプトで定義された関数を使用します。

## 変数

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID` : GitLab CI/CDインターフェースで設定するAPIキー。
- `TERRAFORM_VERSION` : タスクを実行するために使用するTerraformのDockerイメージのバージョン。
- `TF_VAR_region` : デフォルトのAWS地域。

## ステージ

- `オプションの前提条件` : APIキーの追加または削除を行うステージ。
- `チェック` : Terraformの変更を確認するステージ。
- `デプロイメント` : Terraformの変更を適用し、デプロイメントの状態を確認するステージ。
- `マネジメント` : EC2インスタンスとアプリケーションの設定を管理するステージ。
- `削除` : TerraformリソースとAPIキーを削除するステージ。

## .terraform_template

これは、Dockerイメージ、エントリ、およびスクリプト前のコマンドを定義するTerraformタスクのテンプレートです。

## タスク

1. **Terraformチェック** : `terraform_plan`を実行し、変更を確認し、プランをアーティファクトに記録します。
2. **Terraformデプロイメント** : `terraform_apply`を実行して変更を適用し、`check_deployment_status`を使用してデプロイ状況を確認します。
3. **Terraform削除** : `terraform_destroy`を実行してTerraformリソースを削除します。
4. **Librechat登録を開く** : `manage_allow_registration on`を使用してLibrechatアプリケーションで登録を有効にします。
5. **Librechat登録を閉じる** : `manage_allow_registration off`を使用してLibrechatアプリケーションで登録を無効にします。
6. **EC2開始** : `manage_ec2_instance start`を使用してEC2インスタンスを開始します。
7. **EC2停止** : `manage_ec2_instance stop`を使用してEC2インスタンスを停止します。
8. **EC2ステータス** : `manage_ec2_instance status`を使用してEC2インスタンスの状態を表示します。
9. **キー - 追加** : 対応する`manage_*_key put`関数を使用してAPIキーを追加します。
10. **キーの削除**: このタスクは対応する `manage_*_key delete` 関数を使用してAPIキーを削除します。

## インフラストラクチャの完全なデプロイメント時間＋EC2 OS設定は6分未満：
![alt text](/img/pipeline_apply_time.png)

## 注意事項とベストプラクティス

1. GitLab CI/CDインターフェイスでパイプラインを実行する前に、`OPENAI_KEY`、`MISTRALAI_KEY`、`GOOGLE_API_KEY`、`GOOGLE_CSE_ID`変数を正しく設定してください。
2. インフラストラクチャに適したTerraform Dockerイメージのバージョンを使用するようにしてください。
3. GitLab CI/CDファイルを変更する際は、対応するBashスクリプトも更新することを忘れないでください。
4. APIキーとAWS認証情報を管理する際は、セキュリティのベストプラクティスに従ってください。

これらの注意事項とベストプラクティスに従うことで、GitLab CI/CDファイルを使用してTerraformとBashスクリプト内で定義された関数を使用し、AWSインフラストラクチャとEC2インスタンスを効果的に管理できます。

## トラブルシューティングと問題解決

1. **AWS認証エラー**: GitLab CI/CDパイプライン実行中に認証エラーが発生した場合、AWS認証情報が正しく設定されていることを確認してください。`AWS_ACCESS_KEY_ID`、`AWS_SECRET_ACCESS_KEY`、および必要に応じて`AWS_SESSION_TOKEN`環境変数が正しく設定され、有効であることを確認してください。これらはGitLab CI/CDインターフェイスまたはBashスクリプトで構成できます。
2. **Terraform設定エラー**: Terraformタスク(`terraform_plan`、`terraform_apply`、`terraform_destroy`)の実行中にエラーが発生した場合、Terraform設定ファイルが正しく構成されていることを確認してください。AWSリソースが適切に定義されており、リソース間の依存関係が正しく管理されていることを確認してください。
3. **APIキー管理エラー**: APIキーの追加または削除時にエラーが発生した場合、AWS SSMパラメーターストアへのアクセス権限があることを確認してください。また、APIキーが正しくフォーマットされており、`manage_*_key`関数に正しい引数を提供していることを確認してください。
4. **EC2インスタンス管理エラー**: `manage_ec2_instance`関数を使用する際にエラーが発生した場合、AWSアカウントでEC2インスタンスを管理するための適切なアクセス権限があることを確認してください。また、インスタンスIDが正しいことを確認し、これらの関数に正しい引数を提供してください。

このGitLab CI/CDファイルに問題がある場合、AWS、Terraform、およびGitLab CI/CDのドキュメント、フォーラム、オンラインリソースを参照してヘルプを得ることができます。また、追加のサポートが必要な場合は、AWSまたはGitLabのサポートにお問い合わせください。

これらのトラブルシューティングと問題解決のヒントに従うことで、TerraformとBashスクリプト内で定義された機能を使用して、AWSインフラストラクチャとEC2インスタンスを管理する際に、このGitLab CI/CDファイルを使用すると発生する可能性のあるほとんどの問題を解決できるはずです。

# LibreChat EC2ユーザーデータスクリプト

このスクリプトは、Amazon Elastic Compute Cloud (EC2) インスタンス上で実行され、LibreChatアプリケーションの設定プロセスを自動化するように設計されています。オペレーティングシステムの更新、必要なパッケージのインストール、Dockerの設定、LibreChatアプリケーションのデプロイなど、さまざまなタスクを実行します。

## スクリプトの概要

1. オペレーティングシステムの更新と必要なパッケージのインストール。
2. DockerとDocker Composeの設定。
3. LibreChatアプリケーションのクローンと設定。
4. セキュアな通信のためのSSL証明書の設定。
5. Docker Composeを使用してLibreChatアプリケーションを起動。

## スクリプトの詳細 ### オペレーティングシステムの更新と必要なパッケージのインストール

スクリプトはまず、オペレーティングシステムを更新し、`ca-certificates`、`curl`、および `awscli` などの必要なパッケージをインストールします。これにより、LibreChatアプリケーションを実行するために必要な依存関係がシステムに確保されます。

### DockerとDocker Composeの設定

次に、スクリプトはDockerとDocker Composeをインストールします。これらは、LibreChatアプリケーションをコンテナ化された環境でデプロイするために使用されます。また、現在のユーザーをDockerグループに追加し、root権限なしでDockerコマンドを実行できるようにします。

### LibreChatアプリケーションのクローンと設定

スクリプトはGitHubからLibreChatリポジトリをクローンし、必要な設定ファイルを設定します。また、AWS Systems Manager (SSM) Parameter StoreからAPIキーとトークンを取得し、それに応じて設定ファイルを更新します。

### 安全な通信のためのSSL証明書の設定

クライアントとサーバー間の通信を確保するために、スクリプトは自己署名SSL証明書を生成し、SSLを有効にするためにNginxの設定を変更します。また、SSL暗号化を強化するためにDiffie-Hellmanパラメータをダウンロードします。

### Docker Composeを使用したLibreChatアプリケーションの起動

最後に、スクリプトはDocker Composeを使用してLibreChatアプリケーションを起動します。これにより、コンテナ化されたアプリケーションが開始され、ユーザーにアクセス可能になります。

## エラー管理とログ記録

スクリプトには、設定プロセス中に発生する可能性のある問題を解決するためのエラー管理とログ記録メカニズムが含まれています。標準出力ストリームとエラーストリームを `/var/log/user-data.log` にリダイレクトします。さらに、スクリプトは実行中のエラーをキャッチするトラップ関数を使用し、AWS SSM Parameter Storeのデプロイメントステータスを適切に更新します。

## デプロイメントステータスの確認

このスクリプトには含まれていませんが、提供された情報で言及されている関数 `check_deployment_status` は、AWS SSM Parameter Storeから特定のパラメータを取得してEC2インスタンスのデプロイメントステータスを確認するために使用されます。この関数は、`export.sh` スクリプトで呼び出され、デプロイメントの進捗状況についての更新情報を提供します。

## Terraformの統合

このユーザーデータスクリプトは、Terraform設定ファイル `ec2.tf` 内で呼び出され、EC2インスタンスを作成し、指定されたパラメータを適用します。ユーザーデータスクリプトは `user_data` 引数を使用してインスタンスに送信され、インスタンスが起動されるときにスクリプトが実行されることを保証します。

## ファイル情報

名前：`user_data.sh`  
場所：`${path.module}/scripts/user_data.sh`  
使用法：Terraform設定ファイル `ec2.tf` に指定されているように、EC2インスタンスが起動されると自動的に実行されます。

# スクリプト check_spot.py

このPythonスクリプトは、Terraformの変数ファイル (`variables.tf`) に記載されているEC2スポットインスタンスの価格を確認し、更新することができます。このスクリプトを使用することで、コスト管理が容易になり、インスタンスで使用されるスポット価格が最新であることが保証されます。

このスクリプトを使用するには、まずPython 3とPython用AWS SDKであるBoto3をインストールしておく必要があります。その後、`--update` オプションを使用してスクリプトを実行し、`variables.tf` ファイル内のスポット価格を更新します。スクリプトは、異なるタイプのインスタンスをサポートし、指定されたインスタンスの可用性ゾーン（AZ）を自動的に取得します。

スクリプトは、引数解析器を設定し、リージョン、インスタンスタイプ、`variables.tf` のファイルパスなどのAWSパラメータを定義することから始まります。その後、AWS EC2サービスと連携するためにEC2クライアントを初期化します。 `read_max_spot_prices()` 関数は、`variables.tf` ファイルから現在のスポット価格の最大値を読み取り、インスタンスタイプをキー、最大価格を値とする辞書を返します。

`get_instance_az()` 関数は、EC2 クライアントを使用して、タグ名で指定されたインスタンスの可用性ゾーン（AZ）を取得します。インスタンスが見つからない場合、デフォルトのAZを返します。

`check_and_update_spot_prices()` 関数は、必要に応じて `variables.tf` ファイル内のスポット価格の最大値を確認し更新します。 `read_max_spot_prices()` を呼び出して現在の価格を取得し、各インスタンスタイプと指定されたAZのスポット価格の履歴を取得します。現在の価格が構成されている価格を超えている場合、この関数は構成された価格を更新します。

`update_variables_file()` 関数は、新しいスポット価格で `variables.tf` ファイルを更新します。この関数は、インスタンスタイプをキー、新しい価格を値とする辞書を入力として受け取ります。

最後に、メインスクリプトは `check_and_update_spot_prices()` を呼び出して、スポット価格を確認し更新します。スクリプトの実行時に `--update` 引数が指定され、新しい価格がある場合、`update_variables_file()` 関数が呼び出されて `variables.tf` ファイルを更新します。

要約すると、このPythonスクリプトは、EC2インスタンスのスポット価格の管理を簡素化し、Terraformの変数ファイルでの価格の確認と更新のプロセスを自動化します。

# ドキュメント Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## 要件

| 名前 | バージョン |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## プロバイダー

| 名前 | バージョン |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## モジュール

モジュールはありません。

## リソース | 名前 | タイプ |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | リソース |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | リソース |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | リソース |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | リソース |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | リソース |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | リソース |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | リソース |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | リソース |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | リソース |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | リソース |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | リソース |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | リソース |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | リソース |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | リソース |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | リソース |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | リソース |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | データソース |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | データソース |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | データソース |

## 入力 | Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | 登録を有効または無効にする | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | EC2インスタンス接続エンドポイントにアクセスする許可されたIPアドレスのリスト | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | AWS AMIのフィルター名 | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | EC2インスタンスを自動的に停止する時間 | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | サブネットの可用性ゾーン | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | EC2インスタンスのタイプ | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | 開放するポートのリスト | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | リソースがデプロイされるAWSリージョンを指定します。 | `string` | n/a | yes |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | 各インスタンスタイプの最大価格 | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | スポットリクエストタイプ（持続的またはスポット） | `string` | `"persistante"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | サブネット用のCIDRブロック | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | 使用するサブネットのインデックス [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | GB単位のEBSボリュームサイズ | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC用のCIDRブロック | `string` | `"10.0.0.0/24"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | EC2インスタンスのID |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | EC2インスタンスのパブリックIPアドレス |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |
<!-- END_TF_DOCS -->

# 作者

Julien LE SAUX  
メール：contact@jls42.org

# 免責事項

このスクリプトを使用することで、自分自身のリスクで行っていることを認めるものとします。作者は、このスクリプトの使用による料金、損害、損失について責任を負いません。このスクリプトを使用する前に、AWSに関連する費用を理解していることを確認してください。

# ライセンス

このプロジェクトはGPLライセンスのもとで提供されています。

**このドキュメントは、gpt-4oモデルを使用してfrバージョンからja言語に翻訳されました。翻訳プロセスの詳細については、https://gitlab.com/jls42/ai-powered-markdown-translator をご覧ください。**

