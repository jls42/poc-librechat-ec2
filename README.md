[[_TOC_]]


# Déploiement entièrement automatisé de Librechat sur EC2 AWS

Ce projet peut être déployé et utilisé de deux manières :
- Soit depuis l'interface de gitlab.com via une pipeline
- Soit depuis son poste sous Linux

Dans les deux cas, le fichier central utilisé est le même, à savoir le fichier `export.sh`.
Pour l'infrastructure, il s'agit d'un déploiement avec `Terraform` (Infrastructure As Code).
Quant à l'installation des composants dans l'EC2, elle est effectuée via `user-data` à l'aide d'un script `bash`.

L'ajout d'une clé d'API n'est pas obligatoire pour le déploiement, à l'exception de l'utilisation de Mistral AI, pour laquelle il est nécessaire de fournir une clé pour qu'il soit fonctionnel.
Dans les autres cas, même sans clé, celle-ci sera demandée directement dans l'interface du Librechat déployé.   
Ce projet prend en charge le déploiement automatique des clés d'API pour OpenAI, Mistral AI, Claude et la clé d'API associée à la [recherche Google](https://docs.librechat.ai/features/plugins/google_search.html).  

**L'installation par défaut de Librechat** rend le port 80 accessible sans activer le port 443. Cette configuration n'est pas recommandée pour une application qui nécessite une authentification.  
Dans le cadre de ce projet Librechat sur EC2, **le port 443 est activé par défaut avec un certificat auto-signé**. Le port 80 redirige vers le port 443. Lors de l'utilisation, un avertissement HTTPS s'affichera dans le navigateur. Cet avertissement est "normal" car le certificat n'est pas signé par une autorité de certification reconnue. Toutefois, **l'utilisation du protocole HTTPS offre une sécurité contre le vol de mot de passe sur le réseau**.  

Le code Terraform du projet déploie des points de terminaison nécessaires à son bon fonctionnement. Si vous disposez déjà de ces points de terminaison, vous pouvez commenter le code associé. Pour ce faire, commentez le contenu du fichier `endpoints.tf`.   

# Architecture  
![alt text](/img/architecture.drawio.png)

# Déploiement via son poste

Pré-requis :
Avoir le client AWS de configuré afin de pouvoir exécuter des actions de déploiement via API (ou alors de fournir les informations en variable d'environnement).  

On edit le fichier export.sh et on choisi un nom de bucket pour l'état `Terraform`  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

Si voulez ajouter des cle(s) :  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
Puis lancer le script `export.sh`  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
Voici l'image montrant le résultat obtenu de manière légèrement accélérée :  
![alt text](/img/check_deploy_status.gif)

# Déploiement via gitlab.com

Créer un projet git et envoyer de ce dépôt dedans et rajouter les variables de CI.

Variables obligatoires :
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

Variables optionnelles :
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

Puis aller dans Build->Pipeline et lancer les jobs associés (via l'icone "play") :

Le job de clé(s) si vous en avez mis, sinon directement sur Vérification Terraform.
![alt text](/img/pipeline.png)


# Documentation du fichier export.sh

Ce script Bash est une interface en ligne de commande (CLI) pour faciliter la gestion et le déploiement d'infrastructures AWS à l'aide de Terraform et la gestion des instances EC2. Le script fournit plusieurs fonctions pour gérer les ressources AWS, les clés d'API et se connecter aux instances EC2.

## Configuration initiale et définition des variables globales

- Active l'exportation de toutes les variables définies à partir de ce point.

![alt text](/img/export.sh.gif)

## Définition des variables pour configurer l'environnement AWS et Terraform

- `BUCKET_STATE` : Nom du bucket S3 pour stocker l'état Terraform.
- `AWS_DEFAULT_REGION` : Région AWS utilisée par défaut.
- `USERNAME` : Nom d'utilisateur pour la connexion SSH.
- `PUBLIC_KEY_FILE` : Emplacement de la clé publique SSH pour l'authentification.
- `TF_VAR_region` : Définit la région AWS comme variable d'environnement pour Terraform.

## Vérification et installation d'awscli

- Vérifie si awscli est installé, sinon il l'installe.

## Création et configuration du bucket S3 pour Terraform

- Crée un bucket S3 pour stocker l'état de Terraform, si ce n'est pas déjà fait, et active le versioning du bucket.

## Initialisation de Terraform

- Initialise Terraform avec les configurations nécessaires pour utiliser le bucket S3 comme backend.

## Fonctions

1. `terraform_plan()` : Planifie les modifications Terraform en mettant à jour les modules, validant la configuration, et créant un plan d'exécution.
2. `terraform_apply()` : Applique les modifications Terraform à partir du plan d'exécution.
3. `terraform_destroy()` : Détruit toutes les ressources Terraform configurées et tente de supprimer le statut de déploiement dans AWS SSM Parameter Store.
4. `ssh_connect()` : Se connecte à une instance EC2 via SSH après avoir envoyé la clé publique.
5. `ssm_connect()` : Se connecte à une instance EC2 via AWS SSM.
6. `delete_parameter()` : Supprime un paramètre spécifique dans AWS SSM Parameter Store.
7. `manage_openai_key()` : Gère les clés d'API OpenAI dans AWS SSM Parameter Store (ajout et suppression).
8. `manage_mistralai_key()` : Gère les clés Mistral AI dans AWS SSM Parameter Store (ajout et suppression).
9. `manage_anthropic_key()` : Gère les clés Anthropic AI dans AWS SSM Parameter Store (ajout et suppression).
10. `manage_google_api_key()` : Gère les clés Google API dans AWS SSM Parameter Store (ajout et suppression).
11. `manage_google_cse_id()` : Gère le Google Search Engine ID dans AWS SSM Parameter Store (ajout et suppression).
12. `check_deployment_status()` : Vérifie l'état de déploiement d'une instance en consultant un paramètre spécifique dans AWS SSM Parameter Store.
13. `manage_allow_registration()` : Active ou désactive l'inscription sur LibreChat en mettant à jour un paramètre dans AWS SSM Parameter Store.
14. `manage_ssm_parameters()` : Gère les paramètres dans AWS SSM Parameter Store selon l'action spécifiée (list, view, delete).
15. `read_spot_request_type()` : Récupère le type de demande Spot pour une instance EC2.
16. `manage_ec2_instance()` : Gère l'instance EC2 en fonction de l'action spécifiée (stop, start, status).
17. `display_help()` : Affiche les commandes disponibles pour l'utilisateur.

## Exportation des fonctions

- Exporte les fonctions pour qu'elles soient disponibles pour une utilisation ultérieure.

## Affichage de l'aide

- Affiche l'aide à l'utilisateur après avoir terminé l'initialisation.

## Utilisation des fonctions

### terraform_plan

Cette fonction permet de planifier les modifications Terraform. Elle met à jour les modules Terraform, valide la configuration Terraform, et crée un plan d'exécution Terraform.

Pour utiliser la fonction `terraform_plan`, exécutez simplement :
```bash
terraform_plan
```
### terraform_apply

Cette fonction permet d'appliquer les modifications Terraform à partir du plan d'exécution.

Pour utiliser la fonction `terraform_apply`, exécutez simplement :
```bash
terraform_apply
```
### terraform_destroy

Cette fonction permet de détruire toutes les ressources Terraform configurées et tente de supprimer le statut de déploiement dans AWS SSM Parameter Store.

Pour utiliser la fonction `terraform_destroy`, exécutez simplement :
```bash
terraform_destroy
```
### ssh_connect

Cette fonction permet de se connecter à une instance EC2 via SSH après avoir envoyé la clé publique.

Pour utiliser la fonction `ssh_connect`, exécutez simplement :
```bash
ssh_connect
```
### ssm_connect

Cette fonction permet de se connecter à une instance EC2 via AWS SSM.

Pour utiliser la fonction `ssm_connect`, exécutez simplement :
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png)

### manage_openai_key

Cette fonction permet de gérer les clés d'API OpenAI dans AWS SSM Parameter Store (ajout et suppression).

Pour utiliser la fonction `manage_openai_key`, exécutez :
```bash
manage_openai_key put [key]
```
pour ajouter une clé, et
```bash
manage_openai_key delete
```
pour supprimer la clé.

### manage_mistralai_key

Cette fonction permet de gérer les clés Mistral AI dans AWS SSM Parameter Store (ajout et suppression).

Pour utiliser la fonction `manage_mistralai_key`, exécutez :
```bash
manage_mistralai_key put [key]
```
pour ajouter une clé, et
```bash
manage_mistralai_key delete
```
pour supprimer la clé.

### manage_anthropic_key

Cette fonction permet de gérer les clés Anthropic AI dans AWS SSM Parameter Store (ajout et suppression).

Pour utiliser la fonction `manage_anthropic_key`, exécutez :
```bash
manage_anthropic_key put [key]
```
pour ajouter une clé, et
```bash
manage_anthropic_key delete
```
pour supprimer la clé.

### manage_google_api_key

Cette fonction permet de gérer les clés Google API dans AWS SSM Parameter Store (ajout et suppression).

Pour utiliser la fonction `manage_google_api_key`, exécutez :
```bash
manage_google_api_key put [key]
```
pour ajouter une clé, et
```bash
manage_google_api_key delete
```
pour supprimer la clé.

### manage_google_cse_id

Cette fonction permet de gérer le Google Search Engine ID dans AWS SSM Parameter Store (ajout et suppression).

Pour utiliser la fonction `manage_google_cse_id`, exécutez :
```bash
manage_google_cse_id put [key]
```
pour ajouter une clé, et
```bash
manage_google_cse_id delete
```
pour supprimer la clé.

### check_deployment_status

Cette fonction permet de vérifier l'état de déploiement d'une instance en consultant un paramètre spécifique dans AWS SSM Parameter Store.

Pour utiliser la fonction `check_deployment_status`, exécutez simplement :
```bash
check_deployment_status
```
### manage_allow_registration

Cette fonction permet d'activer ou désactiver l'inscription sur LibreChat en mettant à jour un paramètre dans AWS SSM Parameter Store.

Pour utiliser la fonction `manage_allow_registration`, exécutez :
```bash
manage_allow_registration on
```
pour activer l'inscription, et
```bash
manage_allow_registration off
```
pour désactiver l'inscription.

### manage_ssm_parameters

Cette fonction permet de gérer les paramètres dans AWS SSM Parameter Store selon l'action spécifiée (list, view, delete).

Pour utiliser la fonction `manage_ssm_parameters`, exécutez :
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
pour lister les paramètres,
```bash
manage_ssm_parameters view
```
pour afficher les valeurs des paramètres, et
```bash
manage_ssm_parameters delete
```
pour supprimer tous les paramètres.

### manage_ec2_instance

Cette fonction permet de gérer l'instance EC2 en fonction de l'action spécifiée (stop, start, status).

Pour utiliser la fonction `manage_ec2_instance`, exécutez :
```bash
manage_ec2_instance stop
```
pour arrêter l'instance,
```bash
manage_ec2_instance start
```
pour démarrer l'instance, et
```bash
manage_ec2_instance status
```

Pour afficher le statut de l'instance :  

![alt text](/img/status.png)

### display_help

Cette fonction affiche les commandes disponibles pour l'utilisateur.

Pour utiliser la fonction `display_help`, exécutez simplement :
```bash
display_help
```

## Remarques et bonnes pratiques

1. Assurez-vous d'avoir configuré correctement vos identifiants AWS avant d'utiliser ce script. Vous pouvez configurer vos identifiants en utilisant la commande `aws configure` ou en définissant les variables d'environnement `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, et `AWS_SESSION_TOKEN` (si nécessaire).
2. Avant d'utiliser les fonctions `terraform_plan`, `terraform_apply`, et `terraform_destroy`, assurez-vous d'avoir correctement configuré votre fichier de configuration Terraform (par exemple, `main.tf`). Ce fichier doit contenir les définitions des ressources AWS que vous souhaitez créer, mettre à jour ou supprimer.
3. Les fonctions `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, et `manage_google_cse_id` stockent les clés d'API dans AWS SSM Parameter Store. Assurez-vous de gérer ces clés avec soin et de suivre les meilleures pratiques de sécurité pour éviter tout accès non autorisé.
4. La fonction `manage_ec2_instance` gère l'instance EC2 en fonction du type de demande Spot. Assurez-vous de comprendre les implications des différents types de demandes Spot avant d'utiliser cette fonction. Vous pouvez en apprendre plus sur les instances Spot dans la [documentation AWS](https://aws.amazon.com/ec2/spot/).
5. Il est recommandé de toujours vérifier l'état de déploiement d'une instance en utilisant la fonction `check_deployment_status` après avoir appliqué des modifications Terraform. Cela vous permettra de vous assurer que l'instance est correctement configurée et prête à être utilisée.
6. Lorsque vous utilisez la fonction `ssh_connect` pour vous connecter à une instance EC2, assurez-vous que la clé publique SSH correspondante a été correctement ajoutée à l'instance. Vous pouvez ajouter la clé publique SSH à l'instance en utilisant la console AWS, l'interface de ligne de commande AWS, ou en la spécifiant dans votre fichier de configuration Terraform.
7. Ce script est conçu pour être utilisé avec AWS et Terraform. Assurez-vous d'avoir installé et configuré correctement ces outils avant d'utiliser le script. Vous pouvez trouver des instructions d'installation et de configuration pour AWS CLI dans la [documentation AWS](https://aws.amazon.com/cli/) et pour Terraform dans la [documentation Terraform](https://www.terraform.io/).
8. Il est recommandé de toujours sauvegarder vos données et vos configurations avant d'apporter des modifications à votre infrastructure AWS à l'aide de ce script. Cela vous permettra de récupérer facilement vos données et vos configurations en cas de problème.
9. N'hésitez pas à personnaliser et à étendre ce script en fonction de vos besoins spécifiques. Vous pouvez ajouter de nouvelles fonctions, modifier les fonctions existantes, ou adapter le script pour fonctionner avec d'autres services AWS ou d'autres outils d'infrastructure as code.
10. Si vous souhaitez vous connecter au serveur via SSM, il faut déployer le plugin session-manager. Exemple pour Ubuntu : 
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

En suivant ces remarques et bonnes pratiques, vous pourrez tirer le meilleur parti de ce script pour gérer efficacement votre infrastructure AWS et vos instances EC2.  

## Dépannage et résolution des problèmes

1. **Erreurs d'authentification AWS** : Si vous rencontrez des erreurs d'authentification lors de l'utilisation de ce script, vérifiez que vos identifiants AWS sont correctement configurés. Vous pouvez le faire en utilisant la commande `aws configure` ou en définissant les variables d'environnement `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, et `AWS_SESSION_TOKEN` (si nécessaire). Assurez-vous également que vous disposez des autorisations appropriées pour accéder aux ressources AWS spécifiées dans votre fichier de configuration Terraform.
2. **Erreurs de configuration Terraform** : Si vous rencontrez des erreurs lors de l'utilisation des fonctions `terraform_plan`, `terraform_apply`, ou `terraform_destroy`, vérifiez que votre fichier de configuration Terraform est correctement configuré. Assurez-vous que les ressources AWS sont correctement définies et que les dépendances entre les ressources sont correctement gérées. Vous pouvez utiliser la commande `terraform validate` pour vérifier la validité de votre fichier de configuration Terraform.
3. **Erreurs de connexion SSH** : Si vous rencontrez des erreurs lors de l'utilisation de la fonction `ssh_connect`, vérifiez que la clé publique SSH correspondante a été correctement ajoutée à l'instance EC2. Assurez-vous également que le nom d'utilisateur et l'adresse IP de l'instance sont corrects. Vous pouvez vérifier ces informations en utilisant la console AWS ou l'interface de ligne de commande AWS.
4. **Erreurs de connexion AWS SSM** : Si vous rencontrez des erreurs lors de l'utilisation de la fonction `ssm_connect`, vérifiez que votre instance EC2 est correctement configurée pour utiliser AWS SSM. Assurez-vous que l'instance dispose d'un rôle IAM approprié et que les paramètres de sécurité du groupe de sécurité autorisent le trafic entrant depuis les adresses IP SSM. Vous pouvez en apprendre plus sur la configuration d'AWS SSM dans la [documentation AWS](https://aws.amazon.com/ssm/).
5. **Erreurs de gestion des clés d'API** : Si vous rencontrez des erreurs lors de l'utilisation des fonctions `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key`, ou `manage_google_cse_id`, vérifiez que vous disposez des autorisations appropriées pour accéder à AWS SSM Parameter Store. Assurez-vous également que les clés d'API sont correctement formatées et que vous fournissez les bons arguments à ces fonctions.
6. **Erreurs de gestion des instances EC2** : Si vous rencontrez des erreurs lors de l'utilisation de la fonction `manage_ec2_instance`, vérifiez que vous disposez des autorisations appropriées pour gérer les instances EC2 dans votre compte AWS. Assurez-vous également que l'ID de l'instance est correct et que vous fournissez les bons arguments à cette fonction.  

En cas de problème avec ce script, n'hésitez pas à consulter la documentation AWS et Terraform, ainsi que les forums et les ressources en ligne pour obtenir de l'aide. Vous pouvez également contacter le support AWS si vous avez besoin d'une assistance supplémentaire.

En suivant ces conseils de dépannage et de résolution des problèmes, vous devriez être en mesure de résoudre la plupart des problèmes que vous pourriez rencontrer lors de l'utilisation de ce script pour gérer votre infrastructure AWS et vos instances EC2.

# Documentation du fichier gitlab-ci.yml

Ce fichier GitLab CI/CD (gitlab-ci.yml) décrit un pipeline de déploiement pour gérer l'infrastructure AWS et les instances EC2 à l'aide de Terraform, en utilisant les fonctions définies dans le script Bash précédemment documenté.

## Variables

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID` : Clés d'API à configurer dans l'interface GitLab CI/CD.
- `TERRAFORM_VERSION` : Version de l'image Docker de Terraform utilisée pour exécuter les tâches.
- `TF_VAR_region` : Région AWS par défaut.

## Stages

- `Pré-requis optionels` : Étape pour ajouter ou supprimer des clés d'API.
- `Vérifications` : Étape pour vérifier les modifications Terraform.
- `Déploiements` : Étape pour appliquer les modifications Terraform et vérifier l'état de déploiement.
- `Management` : Étape pour gérer les instances EC2 et les paramètres de l'application.
- `Suppressions` : Étape pour supprimer les ressources Terraform et les clés d'API.

## .terraform\_template

Il s'agit d'un modèle pour les tâches Terraform, définissant l'image Docker, l'entrée et les commandes avant le script.

## Tâches

1. **Vérification Terraform** : Cette tâche vérifie les modifications Terraform en exécutant `terraform_plan` et enregistre le plan dans un artefact.
2. **Déploiement Terraform** : Cette tâche applique les modifications Terraform en exécutant `terraform_apply` et vérifie l'état de déploiement en utilisant `check_deployment_status`.
3. **Suppression Terraform** : Cette tâche supprime les ressources Terraform en exécutant `terraform_destroy`.
4. **Librechat Ouvrir les inscriptions** : Cette tâche active les inscriptions dans l'application Librechat en utilisant `manage_allow_registration on`.
5. **Librechat Fermer les inscriptions** : Cette tâche désactive les inscriptions dans l'application Librechat en utilisant `manage_allow_registration off`.
6. **EC2 Démarrage** : Cette tâche démarre l'instance EC2 en utilisant `manage_ec2_instance start`.
7. **EC2 Stop** : Cette tâche arrête l'instance EC2 en utilisant `manage_ec2_instance stop`.
8. **EC2 Status** : Cette tâche affiche l'état de l'instance EC2 en utilisant `manage_ec2_instance status`.
9. **Clé(s) - Ajout** : Cette tâche ajoute les clés d'API en utilisant les fonctions `manage_*_key put` correspondantes.
10. **Clé(s) - Supression** : Cette tâche supprime les clés d'API en utilisant les fonctions `manage_*_key delete` correspondantes.

## Temps de déploiement complet infrastructure + configuration OS EC2 moins de 6mn :
![alt text](/img/pipeline_apply_time.png)

## Remarques et bonnes pratiques

1. Assurez-vous de configurer correctement les variables `OPENAI_KEY`, `MISTRALAI_KEY`, `GOOGLE_API_KEY`, et `GOOGLE_CSE_ID` dans l'interface GitLab CI/CD avant d'exécuter le pipeline.
2. Veillez à utiliser une version appropriée de l'image Docker de Terraform pour votre infrastructure.
3. N'oubliez pas de mettre à jour le script Bash correspondant lorsque vous apportez des modifications à ce fichier GitLab CI/CD.
4. Suivez les meilleures pratiques de sécurité pour gérer les clés d'API et les identifiants AWS.

En suivant ces remarques et bonnes pratiques, vous pourrez utiliser ce fichier GitLab CI/CD pour gérer efficacement votre infrastructure AWS et vos instances EC2 à l'aide de Terraform et des fonctions définies dans le script Bash.


## Dépannage et résolution des problèmes

1. **Erreurs d'authentification AWS** : Si vous rencontrez des erreurs d'authentification lors de l'exécution du pipeline GitLab CI/CD, vérifiez que vos identifiants AWS sont correctement configurés. Assurez-vous que les variables d'environnement `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, et `AWS_SESSION_TOKEN` (si nécessaire) sont définies et valides. Vous pouvez les configurer dans l'interface GitLab CI/CD ou dans le script Bash.
2. **Erreurs de configuration Terraform** : Si vous rencontrez des erreurs lors de l'exécution des tâches Terraform (`terraform_plan`, `terraform_apply`, ou `terraform_destroy`), vérifiez que votre fichier de configuration Terraform est correctement configuré. Assurez-vous que les ressources AWS sont correctement définies et que les dépendances entre les ressources sont correctement gérées.
3. **Erreurs de gestion des clés d'API** : Si vous rencontrez des erreurs lors de l'ajout ou de la suppression des clés d'API, vérifiez que vous disposez des autorisations appropriées pour accéder à AWS SSM Parameter Store. Assurez-vous également que les clés d'API sont correctement formatées et que vous fournissez les bons arguments aux fonctions `manage_*_key`.
4. **Erreurs de gestion des instances EC2** : Si vous rencontrez des erreurs lors de l'utilisation des fonctions `manage_ec2_instance`, vérifiez que vous disposez des autorisations appropriées pour gérer les instances EC2 dans votre compte AWS. Assurez-vous également que l'ID de l'instance est correct et que vous fournissez les bons arguments à ces fonctions.

En cas de problème avec ce fichier GitLab CI/CD, n'hésitez pas à consulter la documentation AWS, Terraform et GitLab CI/CD, ainsi que les forums et les ressources en ligne pour obtenir de l'aide. Vous pouvez également contacter le support AWS ou GitLab si vous avez besoin d'une assistance supplémentaire.

En suivant ces conseils de dépannage et de résolution des problèmes, vous devriez être en mesure de résoudre la plupart des problèmes que vous pourriez rencontrer lors de l'utilisation de ce fichier GitLab CI/CD pour gérer votre infrastructure AWS et vos instances EC2 à l'aide de Terraform et des fonctions définies dans le script Bash.

# Script de données utilisateur LibreChat EC2

Ce script est conçu pour s'exécuter sur une instance Amazon Elastic Compute Cloud (EC2) et automatiser le processus de configuration de l'application LibreChat. Il effectue diverses tâches telles que la mise à jour du système d'exploitation, l'installation des paquets requis, la configuration de Docker et le déploiement de l'application LibreChat.

## Aperçu du script

1. Mise à jour du système d'exploitation et installation des paquets requis.
2. Configuration de Docker et Docker Compose.
3. Clonage et configuration de l'application LibreChat.
4. Configuration des certificats SSL pour une communication sécurisée.
5. Lancement de l'application LibreChat à l'aide de Docker Compose.

## Détails du script

### Mise à jour du système d'exploitation et installation des paquets requis

Le script commence par mettre à jour le système d'exploitation et installer les paquets requis tels que `ca-certificates`, `curl` et `awscli`. Cela garantit que le système dispose des dépendances nécessaires pour exécuter l'application LibreChat.

### Configuration de Docker et Docker Compose

Le script installe ensuite Docker et Docker Compose, qui sont utilisés pour déployer l'application LibreChat dans un environnement conteneurisé. Il ajoute également l'utilisateur actuel au groupe Docker, permettant à l'utilisateur d'exécuter des commandes Docker sans nécessiter de privilèges root.

### Clonage et configuration de l'application LibreChat

Le script clone le référentiel LibreChat depuis GitHub et configure les fichiers de configuration requis. Il récupère également les clés d'API et les jetons nécessaires à partir d'AWS Systems Manager (SSM) Parameter Store et met à jour les fichiers de configuration en conséquence.

### Configuration des certificats SSL pour une communication sécurisée

Pour sécuriser la communication entre le client et le serveur, le script génère un certificat SSL auto-signé et modifie la configuration de Nginx pour activer SSL. Il télécharge également les paramètres Diffie-Hellman pour renforcer le chiffrement SSL.

### Lancement de l'application LibreChat à l'aide de Docker Compose

Enfin, le script lance l'application LibreChat à l'aide de Docker Compose. Cela démarre l'application conteneurisée et la rend accessible aux utilisateurs.

## Gestion des erreurs et journalisation

Le script comprend des mécanismes de gestion des erreurs et de journalisation pour aider à résoudre les problèmes qui peuvent survenir pendant le processus de configuration. Il redirige les flux de sortie standard et d'erreur vers un fichier journal situé à l'emplacement `/var/log/user-data.log`. De plus, le script utilise une fonction de piège pour capturer les erreurs qui se produisent pendant l'exécution et mettre à jour le statut de déploiement dans AWS SSM Parameter Store en conséquence.

## Vérification du statut de déploiement

La fonction `check_deployment_status`, qui n'est pas incluse dans ce script mais mentionnée dans les informations fournies, est utilisée pour vérifier le statut de déploiement d'une instance EC2 en récupérant un paramètre spécifique à partir d'AWS SSM Parameter Store. Cette fonction est appelée dans le script `export.sh` et fournit des mises à jour sur la progression du déploiement.

## Intégration Terraform

Ce script de données utilisateur est appelé dans le fichier de configuration Terraform `ec2.tf`, qui crée une instance EC2 et applique les paramètres spécifiés. Le script de données utilisateur est transmis à l'instance à l'aide de l'argument `user_data`, garantissant ainsi que le script est exécuté lorsque l'instance est lancée.

## Informations sur le fichier

Nom : `user_data.sh`   
Emplacement : `${path.module}/scripts/user_data.sh`   
Utilisation : S'exécute automatiquement lorsque l'instance EC2 est lancée, comme spécifié dans le fichier de configuration Terraform `ec2.tf`.   

# Script check_spot.py  

Ce script Python, permet de vérifier et mettre à jour les prix spot des instances EC2 dans le fichier de variables Terraform (`variables.tf`). L'utilisation de ce script facilite la gestion des coûts et assure que les prix spot utilisés pour les instances sont à jour.  

Pour utiliser ce script, vous devez d'abord vous assurer d'avoir installé Python 3 et Boto3, le SDK AWS pour Python. Ensuite, vous pouvez exécuter le script avec l'option `--update` pour mettre à jour les prix spot dans le fichier `variables.tf`. Le script prend en charge différents types d'instances et récupère automatiquement la zone de disponibilité (AZ) de l'instance spécifiée.  

Le script commence par configurer l'analyseur d'arguments et définir les paramètres AWS, tels que la région, les types d'instances et le chemin du fichier `variables.tf`. Ensuite, il initialise un client EC2 pour interagir avec le service AWS EC2.  

La fonction `read_max_spot_prices()` lit les prix spot maximaux actuels depuis le fichier `variables.tf` et renvoie un dictionnaire contenant les types d'instances comme clés et les prix maximaux comme valeurs.  

La fonction `get_instance_az()` obtient la zone de disponibilité (AZ) de l'instance spécifiée par le tag Name en utilisant le client EC2. Si aucune instance n'est trouvée, elle renvoie une AZ par défaut.  

La fonction `check_and_update_spot_prices()` vérifie et met à jour les prix spot maximaux dans le fichier `variables.tf` si nécessaire. Elle appelle `read_max_spot_prices()` pour obtenir les prix actuels, puis récupère l'historique des prix spot pour chaque type d'instance et l'AZ spécifiée. Si le prix courant est supérieur au prix configuré, la fonction met à jour le prix configuré.  

La fonction `update_variables_file()` met à jour le fichier `variables.tf` avec les nouveaux prix spot. Elle prend en entrée un dictionnaire contenant les types d'instances comme clés et les nouveaux prix comme valeurs.  

Enfin, le script principal appelle `check_and_update_spot_prices()` pour vérifier et mettre à jour les prix spot. Si l'argument `--update` est spécifié lors de l'exécution du script et qu'il y a de nouveaux prix, la fonction `update_variables_file()` est appelée pour mettre à jour le fichier `variables.tf`.  

En résumé, ce script Python simplifie la gestion des prix spot pour les instances EC2 en automatisant le processus de vérification et de mise à jour des prix dans un fichier de variables Terraform.  

# Documentation Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | Permet d'activer ou désactiver l'inscription | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | Liste des adresses IP autorisées à accéder au point de terminaison EC2 Instance Connect | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | Nom du filtre pour l'AMI AWS | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | Heure pour arrêter automatiquement l'instance EC2 | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | Zones de disponibilité pour les subnets | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Type d'instance pour l'instance EC2 | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | Liste des ports à ouvrir | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | Définition de la région AWS où les ressources seront déployées. | `string` | n/a | yes |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | Prix maximum pour chaque type d'instance | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | Type de demande Spot (persistante ou ponctuelle) | `string` | `"persistante"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | Blocs CIDR pour les subnets | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | Index du sous-réseau à utiliser [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | Taille du volume EBS en GB | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | Bloc CIDR pour le VPC | `string` | `"10.0.0.0/24"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | L'ID de l'instance EC2 |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | L'adresse IP publique de l'instance EC2 |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |
<!-- END_TF_DOCS -->

# Auteur

Julien LE SAUX  
Email : contact@jls42.org

# Clause de non-responsabilité

EN UTILISANT CE SCRIPT, VOUS RECONNAISSEZ QUE VOUS LE FAITES À VOS PROPRES RISQUES. L'AUTEUR N'EST PAS RESPONSABLE DES FRAIS ENGENDRÉS, DES DOMMAGES OU DES PERTES RÉSULTANT DE L'UTILISATION DE CE SCRIPT. ASSUREZ-VOUS DE COMPRENDRE LES FRAIS ASSOCIÉS À AWS AVANT D'UTILISER CE SCRIPT.

# Licence

Ce projet est sous licence GPL.