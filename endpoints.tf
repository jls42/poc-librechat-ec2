# Création d'un endpoint VPC pour AWS Systems Manager (SSM)
resource "aws_vpc_endpoint" "ssm" {
  vpc_id            = aws_vpc.main.id                   # ID du VPC où l'endpoint est créé
  service_name      = "com.amazonaws.${var.region}.ssm" # Nom du service pour SSM
  vpc_endpoint_type = "Interface"                       # Type d'endpoint (Interface dans ce cas)
}

# Création d'un endpoint VPC pour les messages SSM
resource "aws_vpc_endpoint" "ssmmessages" {
  vpc_id            = aws_vpc.main.id                           # ID du VPC où l'endpoint est créé
  service_name      = "com.amazonaws.${var.region}.ssmmessages" # Nom du service pour les messages SSM
  vpc_endpoint_type = "Interface"                               # Type d'endpoint (Interface dans ce cas)
}

# Création d'un endpoint VPC pour les messages EC2
resource "aws_vpc_endpoint" "ec2messages" {
  vpc_id            = aws_vpc.main.id                           # ID du VPC où l'endpoint est créé
  service_name      = "com.amazonaws.${var.region}.ec2messages" # Nom du service pour les messages EC2
  vpc_endpoint_type = "Interface"                               # Type d'endpoint (Interface dans ce cas)
}
