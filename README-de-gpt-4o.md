[[_TOC_]]


# Vollständig automatisierte Bereitstellung von Librechat auf EC2 AWS

Dieses Projekt kann auf zwei Arten bereitgestellt und genutzt werden:
- Entweder über die Benutzeroberfläche von gitlab.com via einer Pipeline
- Oder von seinem Linux-Rechner aus

In beiden Fällen wird dieselbe zentrale Datei verwendet, nämlich die Datei `export.sh`.
Für die Infrastruktur handelt es sich um eine Bereitstellung mit `Terraform` (Infrastructure As Code).
Die Installation der Komponenten in der EC2 erfolgt über `user-data` mithilfe eines `bash`-Scripts.

Das Hinzufügen eines API-Schlüssels ist für die Bereitstellung nicht zwingend erforderlich, außer bei der Nutzung von Mistral AI, für die ein Schlüssel erforderlich ist, damit sie funktionsfähig ist.
In anderen Fällen wird, auch wenn kein Schlüssel vorhanden ist, dieser direkt in der Oberfläche des bereitgestellten Librechat angefordert.   
Dieses Projekt unterstützt die automatische Bereitstellung von API-Schlüsseln für OpenAI, Mistral AI, Claude und den mit der [Google-Suche](https://docs.librechat.ai/features/plugins/google_search.html) verbundenen API-Schlüssel.  

**Die Standardinstallation von Librechat** macht Port 80 zugänglich, ohne Port 443 zu aktivieren. Diese Konfiguration wird für eine Anwendung, die eine Authentifizierung erfordert, nicht empfohlen.  
Im Rahmen dieses Projekts Librechat auf EC2 ist **Port 443 standardmäßig mit einem selbstsignierten Zertifikat aktiviert**. Port 80 leitet auf Port 443 um. Bei der Nutzung wird eine HTTPS-Warnung im Browser angezeigt. Diese Warnung ist "normal", da das Zertifikat nicht von einer anerkannten Zertifizierungsstelle signiert ist. Dennoch bietet die Verwendung des HTTPS-Protokolls **Schutz vor Passwortdiebstahl im Netzwerk**.  

Der Terraform-Code des Projekts stellt die für seinen ordnungsgemäßen Betrieb erforderlichen Endpunkte bereit. Wenn Sie bereits über diese Endpunkte verfügen, können Sie den zugehörigen Code auskommentieren. Dazu kommentieren Sie den Inhalt der Datei `endpoints.tf` aus.   

# Architektur  
![alt text](/img/architecture.drawio.png)

# Bereitstellung über den eigenen Rechner

Voraussetzungen:
Den AWS-Client konfiguriert haben, um Bereitstellungsaktionen über die API ausführen zu können (oder die Informationen als Umgebungsvariable bereitstellen).  

Die Datei export.sh bearbeiten und einen Bucket-Namen für den `Terraform`-Status wählen  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

Wenn Sie Schlüssel hinzufügen möchten:  
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
Dann das Script `export.sh` ausführen  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
Hier ist das Bild, das das beschleunigte erzielte Ergebnis zeigt:  
![alt text](/img/check_deploy_status.gif)

# Bereitstellung über gitlab.com

Ein Git-Projekt erstellen und dieses Repository darin senden und die CI-Variablen hinzufügen.

Obligatorische Variablen:
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

Optionale Variablen:
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

Dann zu Build->Pipeline gehen und die zugehörigen Jobs starten (über das "Play"-Symbol):

Den Schlüssel-Job, falls Sie welche eingetragen haben, ansonsten direkt zur Terraform-Überprüfung.
![alt text](/img/pipeline.png)


# Dokumentation der Datei export.sh

Dieses Bash-Skript ist eine Befehlszeilenoberfläche (CLI), um die Verwaltung und Bereitstellung von AWS-Infrastrukturen mithilfe von Terraform und die Verwaltung von EC2-Instanzen zu erleichtern. Das Script bietet mehrere Funktionen zur Verwaltung von AWS-Ressourcen, API-Schlüsseln und zur Verbindung mit den EC2-Instanzen.

## Initialkonfiguration und Definition der globalen Variablen

- Aktiviert die Exportierung aller ab diesem Punkt definierten Variablen.

![alt text](/img/export.sh.gif)

## Definition der Variablen zur Konfiguration der AWS- und Terraform-Umgebung - `BUCKET_STATE` : Name des S3-Buckets zur Speicherung des Terraform-Zustands.
- `AWS_DEFAULT_REGION` : Standardmäßig verwendete AWS-Region.
- `USERNAME` : Benutzername für die SSH-Verbindung.
- `PUBLIC_KEY_FILE` : Speicherort der öffentlichen SSH-Schlüsseldatei zur Authentifizierung.
- `TF_VAR_region` : Setzt die AWS-Region als Umgebungsvariable für Terraform.

## Überprüfung und Installation von AWSCLI

- Überprüft, ob AWSCLI installiert ist, und installiert es andernfalls.

## Erstellung und Konfiguration des S3-Buckets für Terraform

- Erstellt einen S3-Bucket zur Speicherung des Terraform-Zustands, falls noch nicht vorhanden, und aktiviert die Versionierung des Buckets.

## Initialisierung von Terraform

- Initialisiert Terraform mit den notwendigen Konfigurationen, um den S3-Bucket als Backend zu verwenden.

## Funktionen

1. `terraform_plan()` : Plant die Terraform-Änderungen, indem es die Module aktualisiert, die Konfiguration validiert und einen Ausführungsplan erstellt.
2. `terraform_apply()` : Wendet die Terraform-Änderungen aus dem Ausführungsplan an.
3. `terraform_destroy()` : Zerstört alle konfigurierten Terraform-Ressourcen und versucht, den Bereitstellungsstatus im AWS SSM Parameter Store zu löschen.
4. `ssh_connect()` : Verbindet sich über SSH mit einer EC2-Instanz, nachdem der öffentliche Schlüssel gesendet wurde.
5. `ssm_connect()` : Verbindet sich über AWS SSM mit einer EC2-Instanz.
6. `delete_parameter()` : Löscht einen spezifischen Parameter im AWS SSM Parameter Store.
7. `manage_openai_key()` : Verwalten der OpenAI-API-Schlüssel im AWS SSM Parameter Store (Hinzufügen und Löschen).
8. `manage_mistralai_key()` : Verwalten der Mistral AI-Schlüssel im AWS SSM Parameter Store (Hinzufügen und Löschen).
9. `manage_anthropic_key()` : Verwalten der Anthropic AI-Schlüssel im AWS SSM Parameter Store (Hinzufügen und Löschen).
10. `manage_google_api_key()` : Verwalten der Google-API-Schlüssel im AWS SSM Parameter Store (Hinzufügen und Löschen).
11. `manage_google_cse_id()` : Verwalten der Google Search Engine ID im AWS SSM Parameter Store (Hinzufügen und Löschen).
12. `check_deployment_status()` : Überprüft den Bereitstellungsstatus einer Instanz, indem ein spezifischer Parameter im AWS SSM Parameter Store abgerufen wird.
13. `manage_allow_registration()` : Aktiviert oder deaktiviert die Registrierung auf LibreChat, indem ein Parameter im AWS SSM Parameter Store aktualisiert wird.
14. `manage_ssm_parameters()` : Verwalten der Parameter im AWS SSM Parameter Store gemäß der angegebenen Aktion (Listen, Anzeigen, Löschen).
15. `read_spot_request_type()` : Ruft den Typ der Spot-Anforderung für eine EC2-Instanz ab.
16. `manage_ec2_instance()` : Verwalten der EC2-Instanz basierend auf der angegebenen Aktion (Stop, Start, Status).
17. `display_help()` : Zeigt die verfügbaren Befehle für den Benutzer an.

## Export der Funktionen

- Exportiert die Funktionen, damit sie später verwendet werden können.

## Anzeige der Hilfe

- Zeigt die Hilfe für den Benutzer an, nachdem die Initialisierung abgeschlossen ist.

## Verwendung der Funktionen

### terraform_plan

Diese Funktion plant die Terraform-Änderungen. Sie aktualisiert die Terraform-Module, validiert die Terraform-Konfiguration und erstellt einen Terraform-Ausführungsplan.

Um die Funktion `terraform_plan` zu verwenden, führen Sie einfach aus:
```bash
terraform_plan
```
### terraform_apply

Diese Funktion wendet die Terraform-Änderungen aus dem Ausführungsplan an.

Um die Funktion `terraform_apply` zu verwenden, führen Sie einfach aus:
```bash
terraform_apply
```
### terraform_destroy

Diese Funktion zerstört alle konfigurierten Terraform-Ressourcen und versucht, den Bereitstellungsstatus im AWS SSM Parameter Store zu löschen.

Um die Funktion `terraform_destroy` zu verwenden, führen Sie einfach aus:
```bash
terraform_destroy
```
### ssh_connect

Diese Funktion ermöglicht die SSH-Verbindung zu einer EC2-Instanz, nachdem der öffentliche Schlüssel gesendet wurde.

Um die Funktion `ssh_connect` zu verwenden, führen Sie einfach aus:
```bash
ssh_connect
```
### ssm_connect

Diese Funktion ermöglicht die Verbindung zu einer EC2-Instanz über AWS SSM.

Um die Funktion `ssm_connect` zu verwenden, führen Sie einfach aus:
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

Diese Funktion ermöglicht die Verwaltung von OpenAI-API-Schlüsseln im AWS SSM Parameter Store (Hinzufügen und Entfernen).

Um die Funktion `manage_openai_key` zu verwenden, führen Sie aus:
```bash
manage_openai_key put [key]
```
um einen Schlüssel hinzuzufügen, und
```bash
manage_openai_key delete
```
um den Schlüssel zu entfernen.

### manage_mistralai_key

Diese Funktion ermöglicht die Verwaltung von Mistral AI-Schlüsseln im AWS SSM Parameter Store (Hinzufügen und Entfernen).

Um die Funktion `manage_mistralai_key` zu verwenden, führen Sie aus:
```bash
manage_mistralai_key put [key]
```
um einen Schlüssel hinzuzufügen, und
```bash
manage_mistralai_key delete
```
um den Schlüssel zu entfernen.

### manage_anthropic_key

Diese Funktion ermöglicht die Verwaltung von Anthropic AI-Schlüsseln im AWS SSM Parameter Store (Hinzufügen und Entfernen).

Um die Funktion `manage_anthropic_key` zu verwenden, führen Sie aus:
```bash
manage_anthropic_key put [key]
```
um einen Schlüssel hinzuzufügen, und
```bash
manage_anthropic_key delete
```
um den Schlüssel zu entfernen.

### manage_google_api_key

Diese Funktion ermöglicht die Verwaltung von Google-API-Schlüsseln im AWS SSM Parameter Store (Hinzufügen und Entfernen).

Um die Funktion `manage_google_api_key` zu verwenden, führen Sie aus:
```bash
manage_google_api_key put [key]
```
um einen Schlüssel hinzuzufügen, und
```bash
manage_google_api_key delete
```
um den Schlüssel zu entfernen.

### manage_google_cse_id

Diese Funktion ermöglicht die Verwaltung der Google Search Engine ID im AWS SSM Parameter Store (Hinzufügen und Entfernen).

Um die Funktion `manage_google_cse_id` zu verwenden, führen Sie aus:
```bash
manage_google_cse_id put [key]
```
um einen Schlüssel hinzuzufügen, und
```bash
manage_google_cse_id delete
```
um den Schlüssel zu entfernen.

### check_deployment_status

Diese Funktion ermöglicht die Überprüfung des Bereitstellungsstatus einer Instanz durch Abfragen eines bestimmten Parameters im AWS SSM Parameter Store.

Um die Funktion `check_deployment_status` zu verwenden, führen Sie einfach aus:
```bash
check_deployment_status
```
### manage_allow_registration

Diese Funktion ermöglicht die Aktivierung oder Deaktivierung der Registrierung bei LibreChat durch Aktualisierung eines Parameters im AWS SSM Parameter Store.

Um die Funktion `manage_allow_registration` zu verwenden, führen Sie aus:
```bash
manage_allow_registration on
```
um die Registrierung zu aktivieren, und
```bash
manage_allow_registration off
```
um die Registrierung zu deaktivieren.

### manage_ssm_parameters

Diese Funktion ermöglicht die Verwaltung von Parametern im AWS SSM Parameter Store basierend auf der angegebenen Aktion (liste, ansicht, löschen).

Um die Funktion `manage_ssm_parameters` zu verwenden, führen Sie aus:
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
um die Parameter aufzulisten,
```bash
manage_ssm_parameters view
```
um die Parameterwerte anzuzeigen, und
```bash
manage_ssm_parameters delete
```
um alle Parameter zu löschen.

### manage_ec2_instance

Diese Funktion ermöglicht die Verwaltung der EC2-Instanz basierend auf der angegebenen Aktion (stop, start, status).

Um die Funktion `manage_ec2_instance` zu verwenden, führen Sie aus:
```bash
manage_ec2_instance stop
```
um die Instanz zu stoppen,
```bash
manage_ec2_instance start
```
um die Instanz zu starten, und
```bash
manage_ec2_instance status
```

Um den Status der Instanz anzuzeigen:  

![alt text](/img/status.png)

### display_help

Diese Funktion zeigt die verfügbaren Befehle für den Benutzer an.

Um die Funktion `display_help` zu verwenden, führen Sie einfach aus:
```bash
display_help
```

## Anmerkungen und bewährte Methoden

1. Stellen Sie sicher, dass Sie Ihre AWS-Anmeldeinformationen korrekt konfiguriert haben, bevor Sie dieses Skript verwenden. Sie können Ihre Anmeldeinformationen mit dem Befehl `aws configure` konfigurieren oder die Umgebungsvariablen `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` und `AWS_SESSION_TOKEN` (falls erforderlich) festlegen.
2. Bevor Sie die Funktionen `terraform_plan`, `terraform_apply` und `terraform_destroy` verwenden, stellen Sie sicher, dass Sie Ihre Terraform-Konfigurationsdatei (z.B. `main.tf`) korrekt konfiguriert haben. Diese Datei muss die Definitionen der AWS-Ressourcen enthalten, die Sie erstellen, aktualisieren oder löschen möchten.
3. Die Funktionen `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key` und `manage_google_cse_id` speichern die API-Schlüssel im AWS SSM Parameter Store. Stellen Sie sicher, dass Sie diese Schlüssel sorgfältig verwalten und die besten Sicherheitspraktiken anwenden, um unbefugten Zugriff zu vermeiden.
4. Die Funktion `manage_ec2_instance` verwaltet die EC2-Instanz basierend auf dem Spot-Anforderungstyp. Stellen Sie sicher, dass Sie die Auswirkungen der verschiedenen Spot-Anforderungstypen verstehen, bevor Sie diese Funktion verwenden. Sie können mehr über Spot-Instanzen in der [AWS-Dokumentation](https://aws.amazon.com/ec2/spot/) erfahren.
5. Es wird empfohlen, immer den Bereitstellungsstatus einer Instanz zu überprüfen, indem Sie die Funktion `check_deployment_status` verwenden, nachdem Terraform-Änderungen angewendet wurden. So können Sie sicherstellen, dass die Instanz korrekt konfiguriert und einsatzbereit ist.
6. Wenn Sie die Funktion `ssh_connect` verwenden, um sich mit einer EC2-Instanz zu verbinden, stellen Sie sicher, dass der entsprechende öffentliche SSH-Schlüssel korrekt zur Instanz hinzugefügt wurde. Sie können den öffentlichen SSH-Schlüssel zur Instanz hinzufügen, indem Sie die AWS-Konsole, die AWS-Befehlszeilenschnittstelle oder die Angabe in Ihrer Terraform-Konfigurationsdatei verwenden.
7. Dieses Skript ist für die Verwendung mit AWS und Terraform konzipiert. Stellen Sie sicher, dass Sie diese Tools korrekt installiert und konfiguriert haben, bevor Sie das Skript verwenden. Installations- und Konfigurationsanweisungen für die AWS CLI finden Sie in der [AWS-Dokumentation](https://aws.amazon.com/cli/) und für Terraform in der [Terraform-Dokumentation](https://www.terraform.io/).
8. Es wird empfohlen, immer Ihre Daten und Konfigurationen zu sichern, bevor Sie mit diesem Skript Änderungen an Ihrer AWS-Infrastruktur vornehmen. Dadurch können Sie Ihre Daten und Konfigurationen im Falle eines Problems leicht wiederherstellen.
9. Zögern Sie nicht, dieses Skript an Ihre spezifischen Bedürfnisse anzupassen und zu erweitern. Sie können neue Funktionen hinzufügen, bestehende Funktionen ändern oder das Skript anpassen, um mit anderen AWS-Diensten oder anderen Infrastructure-as-Code-Tools zu arbeiten.
10. Wenn Sie sich über SSM mit dem Server verbinden möchten, müssen Sie das Session-Manager-Plugin bereitstellen. Beispiel für Ubuntu: 
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

Mit diesen Anmerkungen und Best Practices können Sie das Beste aus diesem Skript herausholen, um Ihre AWS-Infrastruktur und Ihre EC2-Instanzen effektiv zu verwalten.

## Fehlerbehebung und Problemlösung

1. **AWS-Authentifizierungsfehler**: Wenn Sie bei der Verwendung dieses Skripts auf Authentifizierungsfehler stoßen, überprüfen Sie, ob Ihre AWS-Anmeldedaten korrekt konfiguriert sind. Sie können dies tun, indem Sie den Befehl `aws configure` verwenden oder die Umgebungsvariablen `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` und `AWS_SESSION_TOKEN` (falls erforderlich) festlegen. Stellen Sie außerdem sicher, dass Sie über die geeigneten Berechtigungen für den Zugriff auf die in Ihrer Terraform-Konfigurationsdatei angegebenen AWS-Ressourcen verfügen.
2. **Terraform-Konfigurationsfehler**: Wenn Sie beim Verwenden der Funktionen `terraform_plan`, `terraform_apply` oder `terraform_destroy` auf Fehler stoßen, überprüfen Sie, ob Ihre Terraform-Konfigurationsdatei korrekt konfiguriert ist. Stellen Sie sicher, dass die AWS-Ressourcen korrekt definiert sind und die Abhängigkeiten zwischen den Ressourcen korrekt verwaltet werden. Sie können den Befehl `terraform validate` verwenden, um die Gültigkeit Ihrer Terraform-Konfigurationsdatei zu überprüfen.
3. **SSH-Verbindungsfehler**: Wenn Sie bei der Verwendung der Funktion `ssh_connect` auf Fehler stoßen, überprüfen Sie, ob der entsprechende öffentliche SSH-Schlüssel korrekt zur EC2-Instanz hinzugefügt wurde. Stellen Sie außerdem sicher, dass der Benutzername und die IP-Adresse der Instanz korrekt sind. Sie können diese Informationen mithilfe der AWS-Konsole oder der AWS-Befehlszeilenschnittstelle überprüfen.
4. **AWS-SSM-Verbindungsfehler**: Wenn Sie bei der Verwendung der Funktion `ssm_connect` auf Fehler stoßen, überprüfen Sie, ob Ihre EC2-Instanz korrekt konfiguriert ist, um AWS SSM zu verwenden. Stellen Sie sicher, dass die Instanz über eine geeignete IAM-Rolle verfügt und die Sicherheitsgruppenparameter den eingehenden Datenverkehr von den SSM-IP-Adressen zulassen. Weitere Informationen zur Konfiguration von AWS SSM finden Sie in der [AWS-Dokumentation](https://aws.amazon.com/ssm/).
5. **Fehler bei der Verwaltung von API-Schlüsseln**: Wenn Sie auf Fehler bei der Verwendung der Funktionen `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key` oder `manage_google_cse_id` stoßen, überprüfen Sie, ob Sie die entsprechenden Berechtigungen zum Zugriff auf den AWS SSM Parameter Store haben. Stellen Sie außerdem sicher, dass die API-Schlüssel korrekt formatiert sind und dass Sie die richtigen Argumente an diese Funktionen übergeben.
6. **Fehler bei der Verwaltung von EC2-Instanzen**: Wenn Sie auf Fehler bei der Verwendung der Funktion `manage_ec2_instance` stoßen, überprüfen Sie, ob Sie die entsprechenden Berechtigungen zur Verwaltung der EC2-Instanzen in Ihrem AWS-Konto haben. Stellen Sie außerdem sicher, dass die Instanz-ID korrekt ist und dass Sie die richtigen Argumente an diese Funktion übergeben.  

Im Falle von Problemen mit diesem Skript zögern Sie nicht, die AWS- und Terraform-Dokumentation sowie Foren und Online-Ressourcen zu konsultieren, um Hilfe zu erhalten. Sie können sich auch an den AWS-Support wenden, wenn Sie zusätzliche Unterstützung benötigen.

Indem Sie diese Tipps zur Fehlersuche und -behebung befolgen, sollten Sie in der Lage sein, die meisten Probleme zu lösen, auf die Sie bei der Verwendung dieses Skripts zur Verwaltung Ihrer AWS-Infrastruktur und Ihrer EC2-Instanzen stoßen könnten.

# Dokumentation der Datei gitlab-ci.yml

Diese GitLab CI/CD-Datei (gitlab-ci.yml) beschreibt eine Bereitstellungspipeline zur Verwaltung der AWS-Infrastruktur und EC2-Instanzen mithilfe von Terraform, basierend auf den im zuvor dokumentierten Bash-Skript definierten Funktionen.

## Variablen

- `OPENAI_KEY`, `MISTRALAI_KEY`, `ANTHROPIC_KEY`, `GOOGLE_API_KEY`, `GOOGLE_CSE_ID`: API-Schlüssel, die in der GitLab-CI/CD-Oberfläche konfiguriert werden müssen.
- `TERRAFORM_VERSION`: Version des Docker-Images von Terraform, das zur Ausführung der Aufgaben verwendet wird.
- `TF_VAR_region`: Standardregion von AWS.

## Stufen

- `Optionale Voraussetzungen`: Stufe zum Hinzufügen oder Entfernen von API-Schlüsseln.
- `Überprüfungen`: Stufe zur Überprüfung der Terraform-Änderungen.
- `Bereitstellungen`: Stufe zur Anwendung der Terraform-Änderungen und zur Überprüfung des Bereitstellungsstatus.
- `Management`: Stufe zur Verwaltung der EC2-Instanzen und der Anwendungsparameter.
- `Löschungen`: Stufe zum Entfernen der Terraform-Ressourcen und API-Schlüssel.

## .terraform\_template

Dies ist ein Vorlage für die Terraform-Aufgaben, die das Docker-Image, den Input und die Befehle vor dem Skript definiert.

## Aufgaben

1. **Terraform-Überprüfung**: Diese Aufgabe überprüft die Terraform-Änderungen, indem `terraform_plan` ausgeführt wird, und speichert den Plan in einem Artefakt.
2. **Terraform-Bereitstellung**: Diese Aufgabe wendet die Terraform-Änderungen an, indem `terraform_apply` ausgeführt wird, und überprüft den Bereitstellungsstatus mittels `check_deployment_status`.
3. **Terraform-Löschung**: Diese Aufgabe entfernt die Terraform-Ressourcen, indem `terraform_destroy` ausgeführt wird.
4. **Librechat Registrierung öffnen**: Diese Aufgabe aktiviert die Registrierung in der Librechat-Anwendung mittels `manage_allow_registration on`.
5. **Librechat Registrierung schließen**: Diese Aufgabe deaktiviert die Registrierung in der Librechat-Anwendung mittels `manage_allow_registration off`.
6. **EC2 Starten**: Diese Aufgabe startet die EC2-Instanz mittels `manage_ec2_instance start`.
7. **EC2 Stop**: Diese Aufgabe stoppt die EC2-Instanz mittels `manage_ec2_instance stop`.
8. **EC2 Status**: Diese Aufgabe zeigt den Status der EC2-Instanz an, indem `manage_ec2_instance status` genutzt wird.
9. **Schlüssel - Hinzufügen**: Diese Aufgabe fügt die API-Schlüssel hinzu, indem die entsprechenden `manage_*_key put`-Funktionen verwendet werden.
10. **Schlüssel - Löschung**: Diese Aufgabe löscht API-Schlüssel mithilfe der entsprechenden `manage_*_key delete` Funktionen.

## Komplette Bereitstellungszeit Infrastruktur + EC2 OS-Konfiguration unter 6 Minuten:
![alt text](/img/pipeline_apply_time.png)

## Bemerkungen und bewährte Praktiken

1. Stellen Sie sicher, dass die Variablen `OPENAI_KEY`, `MISTRALAI_KEY`, `GOOGLE_API_KEY` und `GOOGLE_CSE_ID` korrekt in der GitLab CI/CD-Oberfläche konfiguriert sind, bevor Sie die Pipeline ausführen.
2. Achten Sie darauf, eine geeignete Version des Docker-Images von Terraform für Ihre Infrastruktur zu verwenden.
3. Vergessen Sie nicht, das entsprechende Bash-Skript zu aktualisieren, wenn Sie Änderungen an dieser GitLab CI/CD-Datei vornehmen.
4. Befolgen Sie die besten Sicherheitspraktiken beim Umgang mit API-Schlüsseln und AWS-Anmeldeinformationen.

Wenn Sie diese Bemerkungen und bewährten Praktiken befolgen, können Sie diese GitLab CI/CD-Datei effektiv zur Verwaltung Ihrer AWS-Infrastruktur und EC2-Instanzen mithilfe von Terraform und den im Bash-Skript definierten Funktionen verwenden.

## Fehlerbehebung und Problemlösung

1. **AWS-Authentifizierungsfehler**: Wenn Sie während der Ausführung der GitLab CI/CD-Pipeline Authentifizierungsfehler haben, überprüfen Sie, ob Ihre AWS-Berechtigungsnachweise korrekt konfiguriert sind. Stellen Sie sicher, dass die Umgebungsvariablen `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` und `AWS_SESSION_TOKEN` (falls erforderlich) definiert und gültig sind. Sie können sie in der GitLab CI/CD-Oberfläche oder im Bash-Skript konfigurieren.
2. **Terraform-Konfigurationsfehler**: Wenn Sie bei der Ausführung von Terraform-Aufgaben Fehler haben (`terraform_plan`, `terraform_apply` oder `terraform_destroy`), überprüfen Sie, ob Ihre Terraform-Konfigurationsdatei korrekt konfiguriert ist. Stellen Sie sicher, dass die AWS-Ressourcen korrekt definiert sind und dass die Abhängigkeiten zwischen den Ressourcen korrekt verwaltet werden.
3. **Fehler beim Management von API-Schlüsseln**: Wenn Sie Fehler bei der Hinzufügung oder Löschung von API-Schlüsseln haben, überprüfen Sie, ob Sie die entsprechenden Berechtigungen für den Zugriff auf AWS SSM Parameter Store haben. Stellen Sie auch sicher, dass die API-Schlüssel korrekt formatiert sind und dass Sie die richtigen Argumente für die `manage_*_key` Funktionen bereitstellen.
4. **Fehler beim Management von EC2-Instanzen**: Wenn Sie Fehler bei der Nutzung der `manage_ec2_instance` Funktionen haben, überprüfen Sie, ob Sie die entsprechenden Berechtigungen zum Verwalten der EC2-Instanzen in Ihrem AWS-Konto haben. Stellen Sie auch sicher, dass die Instanz-ID korrekt ist und dass Sie die richtigen Argumente für diese Funktionen bereitstellen.

Bei Problemen mit dieser GitLab CI/CD-Datei zögern Sie nicht, die Dokumentationen von AWS, Terraform und GitLab CI/CD sowie die Foren und Ressourcen online zu konsultieren, um Hilfe zu erhalten. Sie können sich auch an den AWS- oder GitLab-Support wenden, wenn Sie zusätzliche Unterstützung benötigen.

Durch Befolgen dieser Tipps zur Fehlerbehebung und Problemlösung sollten Sie in der Lage sein, die meisten Probleme zu lösen, die bei der Verwendung dieser GitLab CI/CD-Datei zur Verwaltung Ihrer AWS-Infrastruktur und EC2-Instanzen mithilfe von Terraform und den im Bash-Skript definierten Funktionen auftreten könnten.

# LibreChat EC2-Benutzerdaten-Skript

Dieses Skript ist so konzipiert, dass es auf einer Amazon Elastic Compute Cloud (EC2)-Instanz läuft und den Konfigurationsprozess der LibreChat-Anwendung automatisiert. Es führt verschiedene Aufgaben wie das Aktualisieren des Betriebssystems, die Installation der erforderlichen Pakete, die Konfiguration von Docker und das Deployen der LibreChat-Anwendung aus.

## Überblick über das Skript

1. Aktualisierung des Betriebssystems und Installation der erforderlichen Pakete.
2. Konfiguration von Docker und Docker Compose.
3. Klonen und Konfiguration der LibreChat-Anwendung.
4. Konfiguration der SSL-Zertifikate für eine sichere Kommunikation.
5. Start der LibreChat-Anwendung mithilfe von Docker Compose.

## Skript-Details ### Aktualisierung des Betriebssystems und Installation der erforderlichen Pakete

Das Skript beginnt mit der Aktualisierung des Betriebssystems und der Installation der erforderlichen Pakete wie `ca-certificates`, `curl` und `awscli`. Dies stellt sicher, dass das System über die notwendigen Abhängigkeiten verfügt, um die Anwendung LibreChat auszuführen.

### Konfiguration von Docker und Docker Compose

Das Skript installiert dann Docker und Docker Compose, die verwendet werden, um die Anwendung LibreChat in einer containerisierten Umgebung bereitzustellen. Es fügt auch den aktuellen Benutzer zur Docker-Gruppe hinzu, wodurch der Benutzer Docker-Befehle ausführen kann, ohne Root-Rechte zu benötigen.

### Klonen und Konfiguration der Anwendung LibreChat

Das Skript klont das LibreChat-Repository von GitHub und konfiguriert die erforderlichen Konfigurationsdateien. Es ruft auch die notwendigen API-Schlüssel und Token aus dem AWS Systems Manager (SSM) Parameter Store ab und aktualisiert die Konfigurationsdateien entsprechend.

### Konfiguration von SSL-Zertifikaten für sichere Kommunikation

Um die Kommunikation zwischen dem Client und dem Server zu sichern, generiert das Skript ein selbstsigniertes SSL-Zertifikat und ändert die Konfiguration von Nginx, um SSL zu aktivieren. Es lädt auch die Diffie-Hellman-Parameter herunter, um die SSL-Verschlüsselung zu stärken.

### Start der Anwendung LibreChat mit Docker Compose

Schließlich startet das Skript die Anwendung LibreChat mithilfe von Docker Compose. Dies startet die containerisierte Anwendung und macht sie für Benutzer zugänglich.

## Fehlerbehandlung und Protokollierung

Das Skript enthält Mechanismen zur Fehlerbehandlung und Protokollierung, um bei der Lösung von Problemen zu helfen, die während des Konfigurationsprozesses auftreten können. Es leitet die Standard- und Fehlerausgabenströme in eine Protokolldatei um, die sich unter `/var/log/user-data.log` befindet. Außerdem verwendet das Skript eine Fangfunktion, um Fehler zu erfassen, die während der Ausführung auftreten, und aktualisiert entsprechend den Bereitstellungsstatus im AWS SSM Parameter Store.

## Überprüfung des Bereitstellungsstatus

Die Funktion `check_deployment_status`, die in diesem Skript nicht enthalten, aber in den bereitgestellten Informationen erwähnt wird, wird verwendet, um den Bereitstellungsstatus einer EC2-Instanz zu überprüfen, indem ein spezifischer Parameter aus dem AWS SSM Parameter Store abgerufen wird. Diese Funktion wird im Skript `export.sh` aufgerufen und liefert Updates zum Fortschritt der Bereitstellung.

## Terraform-Integration

Dieses Benutzerdatenskript wird in der Terraform-Konfigurationsdatei `ec2.tf` aufgerufen, die eine EC2-Instanz erstellt und die angegebenen Parameter anwendet. Das Benutzerdatenskript wird an die Instanz mithilfe des Arguments `user_data` übergeben, wodurch sichergestellt wird, dass das Skript beim Starten der Instanz ausgeführt wird.

## Datei-Informationen

Name: `user_data.sh`   
Speicherort: `${path.module}/scripts/user_data.sh`   
Verwendung: Führt sich automatisch aus, wenn die EC2-Instanz gestartet wird, wie in der Terraform-Konfigurationsdatei `ec2.tf` angegeben.   

# Skript check_spot.py  

Dieses Python-Skript ermöglicht es, die Spot-Preise von EC2-Instanzen in der Terraform-Variablendatei (`variables.tf`) zu überprüfen und zu aktualisieren. Die Verwendung dieses Skripts erleichtert die Kostenverwaltung und stellt sicher, dass die für die Instanzen verwendeten Spot-Preise aktuell sind.  

Um dieses Skript zu verwenden, müssen Sie zunächst sicherstellen, dass Python 3 und Boto3, das AWS SDK für Python, installiert sind. Anschließend können Sie das Skript mit der Option `--update` ausführen, um die Spot-Preise in der Datei `variables.tf` zu aktualisieren. Das Skript unterstützt verschiedene Instanztypen und ermittelt automatisch die Verfügbarkeitszone (AZ) der angegebenen Instanz.  

Das Skript beginnt mit der Konfiguration des Argumentparsers und der Definition der AWS-Parameter, wie z. B. der Region, der Instanztypen und des Pfads der Datei `variables.tf`. Anschließend initialisiert es einen EC2-Client, um mit dem AWS EC2-Service zu interagieren. Die Funktion `read_max_spot_prices()` liest die aktuellen maximalen Spot-Preise aus der Datei `variables.tf` und gibt ein Wörterbuch zurück, das die Instanztypen als Schlüssel und die maximalen Preise als Werte enthält.

Die Funktion `get_instance_az()` ermittelt die Availability Zone (AZ) der Instanz, die durch den Tag Name mit dem EC2-Client spezifiziert ist. Wenn keine Instanz gefunden wird, gibt sie eine Standard-AZ zurück.

Die Funktion `check_and_update_spot_prices()` überprüft und aktualisiert bei Bedarf die maximalen Spot-Preise in der Datei `variables.tf`. Sie ruft `read_max_spot_prices()` auf, um die aktuellen Preise zu erhalten, und ruft dann den Spot-Preisverlauf für jeden Instanztyp und die angegebene AZ ab. Wenn der aktuelle Preis höher als der konfigurierte Preis ist, aktualisiert die Funktion den konfigurierten Preis.

Die Funktion `update_variables_file()` aktualisiert die Datei `variables.tf` mit den neuen Spot-Preisen. Sie erhält ein Wörterbuch als Eingabe, das die Instanztypen als Schlüssel und die neuen Preise als Werte enthält.

Schließlich ruft das Hauptskript `check_and_update_spot_prices()` auf, um die Spot-Preise zu überprüfen und zu aktualisieren. Wenn das Argument `--update` beim Ausführen des Skripts angegeben ist und es neue Preise gibt, wird die Funktion `update_variables_file()` aufgerufen, um die Datei `variables.tf` zu aktualisieren.

Zusammenfassend vereinfacht dieses Python-Skript die Verwaltung der Spot-Preise für EC2-Instanzen, indem es den Prozess der Überprüfung und Aktualisierung der Preise in einer Terraform-Variablendatei automatisiert.

# Terraform-docs Dokumentation
<!-- BEGIN_TF_DOCS -->
## Anforderungen

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Anbieter

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Module

Keine Module.

## Ressourcen | Name | Typ |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | Ressource |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | Ressource |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | Ressource |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | Ressource |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | Ressource |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | Ressource |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | Ressource |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | Ressource |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | Ressource |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | Ressource |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | Ressource |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | Ressource |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | Ressource |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | Ressource |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | Ressource |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | Ressource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | Datenquelle |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | Datenquelle |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | Datenquelle |

## Inputs | Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | Ermöglicht die Aktivierung oder Deaktivierung der Registrierung | `bool` | `true` | no |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | Liste der IP-Adressen, die auf den Endpunkt EC2 Instance Connect zugreifen dürfen | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | Filtername für das AWS-AMI | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | no |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | Zeit zum automatischen Stoppen der EC2-Instanz | `string` | `"00:00"` | no |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | Verfügbarkeitszonen für die Subnetze | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Instanztyp für die EC2-Instanz | `string` | `"t3a.micro"` | no |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | Liste der zu öffnenden Ports | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | Definition der AWS-Region, in der die Ressourcen bereitgestellt werden. | `string` | n/a | yes |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | Maximalpreis für jeden Instanztyp | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | no |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | Typ der Spot-Anfrage (persistent oder einmalig) | `string` | `"persistente"` | no |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | CIDR-Blöcke für die Subnetze | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | no |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | Index des zu verwendenden Subnetzes [0,1,2] | `number` | `0` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | Größe des EBS-Volumes in GB | `number` | `20` | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | CIDR-Block für das VPC | `string` | `"10.0.0.0/24"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | Die ID der EC2-Instanz |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | Die öffentliche IP-Adresse der EC2-Instanz |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | n/a |
<!-- END_TF_DOCS -->

# Autor

Julien LE SAUX  
Email : contact@jls42.org

# Haftungsausschluss

DURCH DIE VERWENDUNG DIESES SKRIPTS ERKENNEN SIE AN, DASS SIE AUF EIGENES RISIKO HANDELN. DER AUTOR IST NICHT VERANTWORTLICH FÜR DIE ENTSTANDENEN KOSTEN, SCHÄDEN ODER VERLUSTE, DIE SICH AUS DER VERWENDUNG DIESES SKRIPTS ERGEBEN. STELLEN SIE SICHER, DASS SIE DIE MIT AWS VERBUNDENEN KOSTEN VERSTEHEN, BEVOR SIE DIESES SKRIPT VERWENDEN.

# Lizenz

Dieses Projekt ist unter der GPL-Lizenz.

**Dieses Dokument wurde aus der Version fr in die Sprache de mit dem Modell gpt-4o übersetzt. Für weitere Informationen über den Übersetzungsprozess besuchen Sie https://gitlab.com/jls42/ai-powered-markdown-translator.**

