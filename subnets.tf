# Utilisation de for_each pour créer les subnets dans différentes zones de disponibilité
resource "aws_subnet" "subnet" {
  # Création d'une map avec for_each pour itérer sur les zones de disponibilité et les blocs CIDR associés
  for_each = { for i, az in var.availability_zones : az => { cidr = var.subnet_cidrs[i], az = az } }

  vpc_id            = aws_vpc.main.id # ID du VPC dans lequel les subnets seront créés. Le VPC doit être défini ailleurs dans votre code Terraform.
  cidr_block        = each.value.cidr # Utilisation du bloc CIDR spécifié pour chaque subnet dans la variable var.subnet_cidrs.
  availability_zone = each.value.az   # Affectation de chaque subnet à une zone de disponibilité spécifique.

  # Attribution de tags pour chaque subnet, utilisant la clé de l'itération (zone de disponibilité) pour générer un nom unique.
  tags = {
    Name = "subnet-${each.key}" # Le nom du subnet inclut la zone de disponibilité pour faciliter l'identification.
  }
}
