[[_TOC_]]


# 在 EC2 AWS 上全自动部署 Librechat

该项目可以通过两种方式进行部署和使用：
- 可以通过 gitlab.com 界面的管道
- 或者通过本地 Linux 机器

在这两种情况下，使用的核心文件是相同的，即 `export.sh` 文件。
对于基础设施，这是通过 `Terraform`（基础设施即代码）进行的部署。
至于在 EC2 中安装组件，则是通过使用 `bash` 脚本的 `user-data` 完成的。

添加 API 密钥对于部署不是必需的，除了使用 Mistral AI 的情况，需要提供密钥才能正常运行。
在其他情况下，即使没有密钥，也会直接在部署的 Librechat 界面中要求输入。
该项目支持 OpenAI、Mistral AI、Claude 的 API 密钥以及 [Google 搜索](https://docs.librechat.ai/features/plugins/google_search.html) 相关 API 密钥的自动部署。

**Librechat 的默认安装** 使得 80 端口在未激活 443 端口的情况下可访问。此配置对于需要身份验证的应用程序不推荐。
在此 Librechat on EC2 项目中，**默认情况下启用 443 端口并配有自签名证书**。80 端口重定向至 443 端口。使用过程中，浏览器中会显示 HTTPS 警告。此警告是“正常的”，因为证书不是由认可的证书颁发机构签发的。然而，**使用 HTTPS 协议可以提供防止网络密码窃取的安全性**。

项目的 Terraform 代码部署了其正常运行所需的终端。如果您已经拥有这些终端，则可以注释掉相关代码。为此，请注释掉 `endpoints.tf` 文件的内容。

# 架构  
![alt text](/img/architecture.drawio.png)

# 通过本地部署

先决条件：
配置好的 AWS 客户端以便通过 API 执行部署操作（或通过环境变量提供信息）。

编辑 `export.sh` 文件并选择一个用于 `Terraform` 状态的 bucket 名称  

```bash
`BUCKET_STATE="votre-nom-de-projet-state"`  # Nom du bucket S3 pour stocker l'état `Terraform`.
```

如果您想添加密钥：
```bash
export OPENAI_KEY="votre_clef"
export MISTRALAI_KEY="votre_clef"
export ANTHROPIC_KEY="votre_clef"
export GOOGLE_API_KEY="votre_clef"
export GOOGLE_CSE_ID="votre_clef"
```
然后运行 `export.sh` 脚本  

```bash
source export.sh
```
![alt text](/img/export.sh.png)

```bash
terraform_plan
terraform_apply
```

```bash
check_deployment_status
```
这是显示稍加速所得结果的图像：
![alt text](/img/check_deploy_status.gif)

# 通过 gitlab.com 部署

创建一个 Git 项目并将此存储库推送到其中，并添加 CI 变量。

必需变量：
- STATE_BUCKET_NAME
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

可选变量：
- GOOGLE_API_KEY
- GOOGLE_CSE_ID
- MISTRALAI_KEY
- ANTHROPIC_KEY
- OPENAI_KEY

![alt text](/img/variables_ci.png)

然后进入 Build->Pipeline 并启动相关作业（通过“play”图标）：

如果您设置了密钥作业，否则直接执行 Terraform 验证。
![alt text](/img/pipeline.png)


# 文件 export.sh 的文档

此 Bash 脚本是一个命令行界面 (CLI)，用于通过 Terraform 和 EC2 实例的管理来简化 AWS 基础设施的管理和部署。此脚本提供了几种功能来管理 AWS 资源、API 密钥以及连接 EC2 实例。

## 初始配置和全局变量定义

- 激活从此点定义的所有变量的导出。

![alt text](/img/export.sh.gif)

## 定义用于配置 AWS 环境和 Terraform 的变量 - `BUCKET_STATE` : 用于存储 Terraform 状态的 S3 bucket 名称。
- `AWS_DEFAULT_REGION` : 默认使用的 AWS 区域。
- `USERNAME` : SSH 连接的用户名。
- `PUBLIC_KEY_FILE` : 用于认证的 SSH 公钥位置。
- `TF_VAR_region` : 将 AWS 区域设置为 Terraform 的环境变量。

## 检查和安装 awscli

- 检查是否已安装 awscli，如果没有则进行安装。

## 创建和配置 Terraform 的 S3 bucket

- 为存储 Terraform 状态创建一个 S3 bucket，如果尚未创建，并启用 bucket 的版本控制。

## 初始化 Terraform

- 使用必要的配置初始化 Terraform，以使用 S3 bucket 作为后端。

## 功能

1. `terraform_plan()` : 更新模块、验证配置并创建执行计划来规划 Terraform 修改。
2. `terraform_apply()` : 从执行计划中应用 Terraform 修改。
3. `terraform_destroy()` : 销毁所有配置的 Terraform 资源，并尝试删除 AWS SSM Parameter Store 中的部署状态。
4. `ssh_connect()` : 在发送公钥后，通过 SSH 连接到一个 EC2 实例。
5. `ssm_connect()` : 通过 AWS SSM 连接到一个 EC2 实例。
6. `delete_parameter()` : 删除 AWS SSM Parameter Store 中的特定参数。
7. `manage_openai_key()` : 管理 AWS SSM Parameter Store 中的 OpenAI API 密钥（添加和删除）。
8. `manage_mistralai_key()` : 管理 AWS SSM Parameter Store 中的 Mistral AI 密钥（添加和删除）。
9. `manage_anthropic_key()` : 管理 AWS SSM Parameter Store 中的 Anthropic AI 密钥（添加和删除）。
10. `manage_google_api_key()` : 管理 AWS SSM Parameter Store 中的 Google API 密钥（添加和删除）。
11. `manage_google_cse_id()` : 管理 AWS SSM Parameter Store 中的 Google 搜索引擎 ID（添加和删除）。
12. `check_deployment_status()` : 通过检查 AWS SSM Parameter Store 中的特定参数来验证实例的部署状态。
13. `manage_allow_registration()` : 通过更新 AWS SSM Parameter Store 中的参数启用或禁用 LibreChat 的注册。
14. `manage_ssm_parameters()` : 根据指定的操作（列出、查看、删除）管理 AWS SSM Parameter Store 中的参数。
15. `read_spot_request_type()` : 获取 EC2 实例的 Spot 请求类型。
16. `manage_ec2_instance()` : 根据指定的操作（停止、启动、状态）管理 EC2 实例。
17. `display_help()` : 向用户显示可用的命令。

## 导出功能

- 导出功能以便日后使用。

## 显示帮助

- 初始化完成后向用户显示帮助。

## 使用功能

### terraform_plan

该功能可以规划 Terraform 的修改。它更新 Terraform 模块，验证 Terraform 配置，并创建一个 Terraform 执行计划。

要使用 `terraform_plan` 功能，只需执行：
```bash
terraform_plan
```
### terraform_apply

该功能可以从执行计划中应用 Terraform 修改。

要使用 `terraform_apply` 功能，只需执行：
```bash
terraform_apply
```
### terraform_destroy

该功能可以销毁所有配置的 Terraform 资源并尝试删除 AWS SSM Parameter Store 中的部署状态。

要使用 `terraform_destroy` 功能，只需执行：
```bash
terraform_destroy
```
### ssh_connect

该功能可以在发送公钥后，通过 SSH 连接到一个 EC2 实例。

要使用 `ssh_connect` 功能，只需执行：
```bash
ssh_connect
```
### ssm_connect

该功能可以通过 AWS SSM 连接到一个 EC2 实例。

要使用 `ssm_connect` 功能，只需执行：
```bash
ssm_connect
```
![alt text](/img/ssm_connect.png) ### manage_openai_key

该功能用于管理 AWS SSM Parameter Store 中的 OpenAI API 密钥（添加和删除）。

要使用 `manage_openai_key` 功能，运行：
```bash
manage_openai_key put [key]
```
用于添加密钥，和
```bash
manage_openai_key delete
```
用于删除密钥。

### manage_mistralai_key

该功能用于管理 AWS SSM Parameter Store 中的 Mistral AI 密钥（添加和删除）。

要使用 `manage_mistralai_key` 功能，运行：
```bash
manage_mistralai_key put [key]
```
用于添加密钥，和
```bash
manage_mistralai_key delete
```
用于删除密钥。

### manage_anthropic_key

该功能用于管理 AWS SSM Parameter Store 中的 Anthropic AI 密钥（添加和删除）。

要使用 `manage_anthropic_key` 功能，运行：
```bash
manage_anthropic_key put [key]
```
用于添加密钥，和
```bash
manage_anthropic_key delete
```
用于删除密钥。

### manage_google_api_key

该功能用于管理 AWS SSM Parameter Store 中的 Google API 密钥（添加和删除）。

要使用 `manage_google_api_key` 功能，运行：
```bash
manage_google_api_key put [key]
```
用于添加密钥，和
```bash
manage_google_api_key delete
```
用于删除密钥。

### manage_google_cse_id

该功能用于管理 AWS SSM Parameter Store 中的 Google 搜索引擎 ID（添加和删除）。

要使用 `manage_google_cse_id` 功能，运行：
```bash
manage_google_cse_id put [key]
```
用于添加密钥，和
```bash
manage_google_cse_id delete
```
用于删除密钥。

### check_deployment_status

该功能通过检查 AWS SSM Parameter Store 中的特定参数来验证实例的部署状态。

要使用 `check_deployment_status` 功能，只需运行：
```bash
check_deployment_status
```
### manage_allow_registration

该功能通过更新 AWS SSM Parameter Store 中的参数来激活或停用 LibreChat 上的注册。

要使用 `manage_allow_registration` 功能，运行：
```bash
manage_allow_registration on
```
用于激活注册，和
```bash
manage_allow_registration off
```
用于停用注册。

### manage_ssm_parameters

该功能根据指定的操作（list, view, delete）管理 AWS SSM Parameter Store 中的参数。

要使用 `manage_ssm_parameters` 功能，运行：
```bash
manage_ssm_parameters list
```
![alt text](/img/parameters_list.png)
用于列出参数，
```bash
manage_ssm_parameters view
```
用于显示参数值，和
```bash
manage_ssm_parameters delete
```
用于删除所有参数。

### manage_ec2_instance

该功能根据指定的操作（stop, start, status）管理 EC2 实例。

要使用 `manage_ec2_instance` 功能，运行：
```bash
manage_ec2_instance stop
```
用于停止实例，
```bash
manage_ec2_instance start
```
用于启动实例，和
```bash
manage_ec2_instance status
```

用于显示实例状态：

![alt text](/img/status.png)

### display_help

该功能为用户显示可用命令。

要使用 `display_help` 功能，只需运行：
```bash
display_help
```

## 注意事项和最佳实践

1. 确保在使用此脚本之前正确配置了 AWS 凭证。可以使用 `aws configure` 命令或定义环境变量 `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` 和 `AWS_SESSION_TOKEN`（如果需要）来配置凭证。
2. 在使用 `terraform_plan`, `terraform_apply`, 和 `terraform_destroy` 功能之前，确保已正确配置 Terraform 配置文件（例如，`main.tf`）。该文件应包含要创建、更新或删除的 AWS 资源的定义。
3. `manage_openai_key`, `manage_mistralai_key`, `manage_anthropic_key`, `manage_google_api_key` 和 `manage_google_cse_id` 功能将 API 密钥存储在 AWS SSM Parameter Store 中。确保小心管理这些密钥，并遵循安全最佳实践，以避免任何未经授权的访问。
4. `manage_ec2_instance` 功能根据 Spot 请求类型来管理 EC2 实例。在使用此功能之前，请确保理解不同类型 Spot 请求的影响。您可以在 [AWS 文档](https://aws.amazon.com/ec2/spot/) 中了解有关 Spot 实例的更多信息。
5. 建议在使用 Terraform 进行更改后，使用 `check_deployment_status` 功能始终检查实例的部署状态。这将确保实例正确配置并可以使用。
6. 当您使用 `ssh_connect` 功能连接到 EC2 实例时，请确保相应的 SSH 公钥已正确添加到实例中。您可以通过 AWS 控制台、AWS 命令行界面或在您的 Terraform 配置文件中指定来将 SSH 公钥添加到实例。
7. 此脚本设计用于与 AWS 和 Terraform 一起使用。在使用脚本之前，请确保正确安装和配置这些工具。您可以在 [AWS 文档](https://aws.amazon.com/cli/) 中找到 AWS CLI 的安装和配置说明，在 [Terraform 文档](https://www.terraform.io/) 中找到 Terraform 的安装和配置说明。
8. 建议在使用此脚本对您的 AWS 基础设施进行更改之前，始终备份您的数据和配置。这将使您在出现问题时能够轻松恢复数据和配置。
9. 请根据您的特定需求自定义和扩展此脚本。您可以添加新功能、修改现有功能，或者调整脚本以使用其他 AWS 服务或其他基础设施即代码工具。
10. 如果您希望通过 SSM 连接到服务器，则需要部署会话管理插件。Ubuntu 示例：
```bash
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

遵循这些注意事项和最佳实践，您将能够充分利用此脚本，有效地管理您的 AWS 基础设施和 EC2 实例。

## 故障排除与问题解决

1. **AWS 认证错误**：如果在使用此脚本时遇到认证错误，请检查您的 AWS 凭证是否正确配置。您可以使用 `aws configure` 命令或定义环境变量 `AWS_ACCESS_KEY_ID`、`AWS_SECRET_ACCESS_KEY` 和（如有必要）`AWS_SESSION_TOKEN` 来完成此操作。还请确保您有适当的权限访问 Terraform 配置文件中指定的 AWS 资源。
2. **Terraform 配置错误**：如果在使用 `terraform_plan`、`terraform_apply` 或 `terraform_destroy` 功能时遇到错误，请检查您的 Terraform 配置文件是否正确配置。请确保 AWS 资源定义正确，并且资源间的依赖关系得到正确管理。您可以使用 `terraform validate` 命令来验证您的 Terraform 配置文件的有效性。
3. **SSH 连接错误**：如果在使用 `ssh_connect` 功能时遇到错误，请检查相应的 SSH 公钥是否已正确添加到 EC2 实例中。还请确保用户名和实例的 IP 地址正确。您可以使用 AWS 控制台或 AWS 命令行界面来验证这些信息。
4. **AWS SSM 连接错误**：如果在使用 `ssm_connect` 功能时遇到错误，请检查您的 EC2 实例是否正确配置为使用 AWS SSM。 确保实例具有适当的IAM角色，并且安全组的安全参数允许从SSM IP地址传入流量。您可以在[AWS文档](https://aws.amazon.com/ssm/)中了解更多关于配置AWS SSM的信息。
5. **API密钥管理错误**：如果您在使用`manage_openai_key`、`manage_mistralai_key`、`manage_anthropic_key`、`manage_google_api_key`或`manage_google_cse_id`函数时遇到错误，请确认您具有访问AWS SSM Parameter Store的适当权限。还要确保API密钥格式正确，并且您为这些函数提供了正确的参数。
6. **EC2实例管理错误**：如果您在使用`manage_ec2_instance`函数时遇到错误，请确认您具有在AWS账户中管理EC2实例的适当权限。同时确认实例ID正确，并且您为该函数提供了正确的参数。

如果使用此脚本时遇到问题，不妨查看AWS和Terraform的文档，以及在线论坛和资源以获取帮助。如果您需要额外的支持，也可以联系AWS支持。

遵循这些故障排除和问题解决建议，您应该能够解决在使用此脚本管理AWS基础设施和EC2实例时可能遇到的绝大多数问题。

# gitlab-ci.yml文件文档

此GitLab CI/CD文件（gitlab-ci.yml）描述了一个使用Terraform管理AWS基础设施和EC2实例的部署管道，使用了先前文档化的Bash脚本中定义的函数。

## 变量

- `OPENAI_KEY`、`MISTRALAI_KEY`、`ANTHROPIC_KEY`、`GOOGLE_API_KEY`、`GOOGLE_CSE_ID`：要在GitLab CI/CD界面中配置的API密钥。
- `TERRAFORM_VERSION`：用于执行任务的Terraform Docker镜像版本。
- `TF_VAR_region`：默认AWS区域。

## 阶段

- `Pré-requis optionels`：添加或删除API密钥的阶段。
- `Vérifications`：检查Terraform修改的阶段。
- `Déploiements`：应用Terraform修改并验证部署状态的阶段。
- `Management`：管理EC2实例和应用程序参数的阶段。
- `Suppressions`：删除Terraform资源和API密钥的阶段。

## .terraform_template

这是一个用于Terraform任务的模板，定义了Docker镜像、输入和脚本前的命令。

## 任务

1. **Vérification Terraform**：此任务通过执行`terraform_plan`检查Terraform修改，并将计划记录到工件中。
2. **Déploiement Terraform**：此任务通过执行`terraform_apply`应用Terraform修改，并使用`check_deployment_status`验证部署状态。
3. **Suppression Terraform**：此任务通过执行`terraform_destroy`删除Terraform资源。
4. **Librechat Ouvrir les inscriptions**：此任务通过使用`manage_allow_registration on`激活Librechat应用中的注册。
5. **Librechat Fermer les inscriptions**：此任务通过使用`manage_allow_registration off`停用Librechat应用中的注册。
6. **EC2 Démarrage**：此任务通过使用`manage_ec2_instance start`启动EC2实例。
7. **EC2 Stop**：此任务通过使用`manage_ec2_instance stop`停止EC2实例。
8. **EC2 Status**：此任务通过使用`manage_ec2_instance status`显示EC2实例的状态。
9. **Clé(s) - Ajout**：此任务通过使用相应的`manage_*_key put`函数添加API密钥。
10. **密钥 - 删除**：此任务使用相应的`manage_*_key delete`函数删除API密钥。

## 完整基础设施部署时间 + EC2操作系统配置少于6分钟：
![alt text](/img/pipeline_apply_time.png)

## 备注和最佳实践

1. 在运行Pipeline之前，确保在GitLab CI/CD界面中正确配置变量`OPENAI_KEY`、`MISTRALAI_KEY`、`GOOGLE_API_KEY`和`GOOGLE_CSE_ID`。
2. 使用适合您的基础设施的Terraform Docker镜像版本。
3. 当您对该GitLab CI/CD文件进行更改时，别忘了更新相应的Bash脚本。
4. 遵循安全最佳实践来管理API密钥和AWS凭证。

遵循这些备注和最佳实践，您可以使用此GitLab CI/CD文件有效管理您的AWS基础设施和EC2实例，利用Terraform和定义在Bash脚本中的功能。

## 故障排除和问题解决

1. **AWS身份验证错误**：如果您在运行GitLab CI/CD Pipeline时遇到身份验证错误，请检查您的AWS凭证配置是否正确。确保环境变量`AWS_ACCESS_KEY_ID`、`AWS_SECRET_ACCESS_KEY`和`AWS_SESSION_TOKEN`（如果需要）已定义且有效。您可以在GitLab CI/CD界面或Bash脚本中配置它们。
2. **Terraform配置错误**：如果您在执行Terraform任务（`terraform_plan`、`terraform_apply`或`terraform_destroy`）时遇到错误，请检查您的Terraform配置文件是否正确配置。确保AWS资源定义正确，并且资源之间的依赖关系得到正确管理。
3. **API密钥管理错误**：如果在添加或删除API密钥时遇到错误，请检查您是否有适当的权限访问AWS SSM Parameter Store。还要确保API密钥格式正确，并为`manage_*_key`函数提供正确的参数。
4. **EC2实例管理错误**：如果在使用`manage_ec2_instance`函数时遇到错误，请确认您具备在您的AWS账户中管理EC2实例所需的权限。还要确保实例ID正确，并为这些函数提供正确的参数。

如果使用此GitLab CI/CD文件时遇到问题，不要犹豫查阅AWS、Terraform和GitLab CI/CD文档，以及在线论坛和资源以获得帮助。如果需要更多帮助，您还可以联系AWS或GitLab支持。

通过遵循这些故障排除和问题解决建议，您应该能够解决在使用此GitLab CI/CD文件管理您的AWS基础设施和EC2实例时可能遇到的大多数问题，借助Terraform和在Bash脚本中定义的功能。

# LibreChat EC2用户数据脚本

该脚本设计用于在Amazon Elastic Compute Cloud (EC2)实例上运行，自动化配置LibreChat应用的过程。它执行各种任务，如更新操作系统、安装所需软件包、配置Docker并部署LibreChat应用。

## 脚本概览

1. 更新操作系统并安装所需的软件包。
2. 配置Docker和Docker Compose。
3. 克隆并配置LibreChat应用。
4. 配置SSL证书以实现安全通信。
5. 使用Docker Compose启动LibreChat应用。

## 脚本详情 ### 操作系统更新和所需软件包的安装

脚本首先更新操作系统并安装所需的软件包，例如 `ca-certificates`、`curl` 和 `awscli`。这样可以确保系统拥有运行LibreChat应用所需的依赖项。

### Docker和Docker Compose的配置

脚本接着安装Docker和Docker Compose，用于在容器化环境中部署LibreChat应用。它还将当前用户添加到Docker组中，使用户可以在不需要root权限的情况下执行Docker命令。

### LibreChat应用的克隆和配置

脚本从GitHub克隆LibreChat的存储库并配置所需的配置文件。它还通过AWS Systems Manager (SSM) 参数存储获取所需的API密钥和令牌，并相应地更新配置文件。

### 配置SSL证书以实现安全通信

为保障客户端与服务器之间的通信安全，脚本生成一个自行签名的SSL证书并修改Nginx的配置以启用SSL。它还下载了Diffie-Hellman参数来加强SSL加密。

### 使用Docker Compose启动LibreChat应用

最后，脚本使用Docker Compose启动LibreChat应用。这样就启动了容器化的应用，并使其对用户可访问。

## 错误管理和日志记录

脚本包含错误处理和日志记录机制，以便解决在配置过程中可能出现的问题。它将标准输出和错误流重定向到位于 `/var/log/user-data.log` 的日志文件。此外，脚本使用一个捕获函数来捕捉执行过程中发生的错误，并相应地更新AWS SSM参数存储中的部署状态。

## 部署状态的验证

`check_deployment_status`函数在此脚本中未包含，但在提供的信息中提及，用于通过从AWS SSM参数存储中检索特定参数来验证EC2实例的部署状态。这个函数在 `export.sh` 脚本中被调用，提供有关部署进度的更新。

## Terraform集成

此用户数据脚本在Terraform配置文件 `ec2.tf` 中被调用，该文件创建一个EC2实例并应用指定的参数。用户数据脚本通过 `user_data` 参数传递到实例中，确保脚本在实例启动时执行。

## 文件信息

名称: `user_data.sh`   
位置: `${path.module}/scripts/user_data.sh`   
用途: 当EC2实例启动时自动执行，如同在Terraform配置文件 `ec2.tf` 中所指定的。   

# 脚本 check_spot.py  

该Python脚本用于检查和更新Terraform变量文件 (`variables.tf`) 中EC2实例的spot价格。使用此脚本可以简化成本管理，并确保实例使用的spot价格是最新的。

要使用此脚本，首先需确保已安装Python 3和Boto3，即AWS的Python SDK。然后，可以使用 `--update` 选项执行脚本以更新 `variables.tf` 文件中的spot价格。脚本支持不同类型的实例，并自动检索指定实例的可用区（AZ）。

脚本开始时会配置参数解析器并定义AWS参数，例如区域、实例类型和 `variables.tf` 文件路径。然后，它初始化一个EC2客户端以与AWS EC2服务进行交互。 函数 `read_max_spot_prices()` 从 `variables.tf` 文件中读取当前的最大现货价格，并返回一个字典，其中包含实例类型作为键，最大价格作为值。

函数 `get_instance_az()` 使用 EC2 客户端获取由标签名称指定的实例的可用区 (AZ)。如果未找到任何实例，则返回默认的 AZ。

函数 `check_and_update_spot_prices()` 如果有必要，检查并更新 `variables.tf` 文件中的最大现货价格。它调用 `read_max_spot_prices()` 以获取当前价格，然后为每种实例类型和指定的 AZ 获取现货价格历史记录。如果当前价格高于配置的价格，函数将更新配置的价格。

函数 `update_variables_file()` 用新的现货价格更新 `variables.tf` 文件。它输入一个字典，其中包含实例类型作为键，新价格作为值。

最后，主脚本调用 `check_and_update_spot_prices()` 以检查和更新现货价格。如果在运行脚本时指定了 `--update` 参数并且有新的价格，函数 `update_variables_file()` 将被调用以更新 `variables.tf` 文件。

总之，这个 Python 脚本通过自动化检查和更新 Terraform 变量文件中的价格的过程，简化了 EC2 实例的现货价格管理。

# Documentation Terraform-docs  
<!-- BEGIN_TF_DOCS -->
## Requirements

| 名称 | 版本 |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | ~> 2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.36 |

## Providers

| 名称 | 版本 |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.38.0 |

## Modules

无模块。

## Resources | 名称 | 类型 |
|------|------|
| [aws_iam_instance_profile.ec2_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | 资源 |
| [aws_iam_policy.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | 资源 |
| [aws_iam_role.ec2_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | 资源 |
| [aws_iam_role_policy_attachment.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 资源 |
| [aws_iam_role_policy_attachment.ssm_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | 资源 |
| [aws_instance.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | 资源 |
| [aws_internet_gateway.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | 资源 |
| [aws_route_table.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | 资源 |
| [aws_route_table_association.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | 资源 |
| [aws_security_group.inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | 资源 |
| [aws_ssm_parameter.allow_registration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | 资源 |
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | 资源 |
| [aws_vpc.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | 资源 |
| [aws_vpc_endpoint.ec2messages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 资源 |
| [aws_vpc_endpoint.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 资源 |
| [aws_vpc_endpoint.ssmmessages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | 资源 |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | 数据源 |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | 数据源 |
| [aws_iam_policy_document.ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | 数据源 |

## 输入 | 名称 | 描述 | 类型 | 默认值 | 是否必需 |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_registration"></a> [allow\_registration](#input\_allow\_registration) | 允许启用或禁用注册 | `bool` | `true` | 否 |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | 允许访问EC2实例连接端点的IP地址列表 | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | 否 |
| <a name="input_ami_filter_name"></a> [ami\_filter\_name](#input\_ami\_filter\_name) | AWS AMI的过滤器名称 | `string` | `"*ubuntu-jammy-22.04-amd64-server*"` | 否 |
| <a name="input_autostop_time"></a> [autostop\_time](#input\_autostop\_time) | 自动停止EC2实例的时间 | `string` | `"00:00"` | 否 |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | 子网的可用区 | `list(string)` | <pre>[<br>  "eu-west-1a",<br>  "eu-west-1b",<br>  "eu-west-1c"<br>]</pre> | 否 |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | EC2实例的类型 | `string` | `"t3a.micro"` | 否 |
| <a name="input_open_ports"></a> [open\_ports](#input\_open\_ports) | 要打开的端口列表 | `list(number)` | <pre>[<br>  80,<br>  443<br>]</pre> | 否 |
| <a name="input_region"></a> [region](#input\_region) | 定义将部署资源的AWS区域 | `string` | 不适用 | 是 |
| <a name="input_spot_max_price"></a> [spot\_max\_price](#input\_spot\_max\_price) | 每种实例类型的最高价格 | `map(string)` | <pre>{<br>  "t3a.large": "0.0356",<br>  "t3a.medium": "0.0182",<br>  "t3a.micro": "0.0043",<br>  "t3a.small": "0.0092"<br>}</pre> | 否 |
| <a name="input_spot_request_type"></a> [spot\_request\_type](#input\_spot\_request\_type) | Spot请求类型（持久或临时） | `string` | `"持久"` | 否 |
| <a name="input_subnet_cidrs"></a> [subnet\_cidrs](#input\_subnet\_cidrs) | 子网的CIDR块 | `list(string)` | <pre>[<br>  "10.0.0.0/26",<br>  "10.0.0.64/26",<br>  "10.0.0.128/26"<br>]</pre> | 否 |
| <a name="input_subnet_index"></a> [subnet\_index](#input\_subnet\_index) | 要使用的子网索引 [0,1,2] | `number` | `0` | 否 |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | EBS卷的大小（GB） | `number` | `20` | 否 |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | VPC的CIDR块 | `string` | `"10.0.0.0/24"` | 否 |

## Outputs

| 名称 | 描述 |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | EC2实例的ID |
| <a name="output_instance_public_ip"></a> [instance\_public\_ip](#output\_instance\_public\_ip) | EC2实例的公网IP地址 |
| <a name="output_spot_request_type"></a> [spot\_request\_type](#output\_spot\_request\_type) | 不适用 |
<!-- END_TF_DOCS -->

# 作者

Julien LE SAUX  
电子邮件 : contact@jls42.org

# 免责声明

使用此脚本，即表示您承认风险自负。作者对因使用此脚本而产生的费用、损害或损失不承担责任。使用此脚本前，请确保理解与AWS相关的费用。

# 许可证

此项目采用GPL许可证。

**此文档是通过模型 gpt-4o 从版本 fr 翻译到语言 zh。有关翻译过程的更多信息，请访问 https://gitlab.com/jls42/ai-powered-markdown-translator。**

