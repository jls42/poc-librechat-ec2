#region             = "eu-west-1"
vpc_cidr           = "10.0.0.0/24"
subnet_cidrs       = ["10.0.0.0/26", "10.0.0.64/26", "10.0.0.128/26"]
availability_zones = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
#instance_type      = "t3a.medium"
